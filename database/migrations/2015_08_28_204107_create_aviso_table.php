<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aviso', function (Blueprint $table) {
            $table->increments('id_aviso');
            $table->string('titulo');
            $table->mediumText('descripcion');
            $table->date('fecha');
            $table->string('dirigido');
            $table->enum('tipo', array('mica','boletin','yarey','newsletter','generales'));
            $table->string('archivo');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aviso');
    }
}
