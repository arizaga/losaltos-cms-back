<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
            $table->increments('id_horario');
            $table->string('hora_inicio');
            $table->string('lu');
            $table->string('ma');
            $table->string('mi');
            $table->string('ju');
            $table->string('vi');
            $table->string('sa');
            $table->string('grado');
            $table->string('grupo');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('horario');
    }
}
