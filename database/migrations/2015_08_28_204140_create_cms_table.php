<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->increments('id_cms');
            $table->string('user');
            $table->string('pass');
            $table->enum('seccion_horario', array('1', '0'));
            //$table->enum('seccion_tarea', array('1', '0'));
            $table->enum('seccion_aviso', array('1', '0'));
            //$table->enum('seccion_calendario', array('1', '0'));
            $table->enum('seccion_notificacion', array('1', '0'));
            //$table->enum('seccion_periodo', array('1', '0'));
            $table->enum('seccion_curso_padre', array('1', '0'));
            //$table->enum('seccion_maestro', array('1', '0'));
            $table->enum('seccion_foto', array('1', '0'));
            $table->enum('seccion_cms', array('1', '0'));
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms');
    }
}
