<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoPadreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_padre', function (Blueprint $table) {
            $table->increments('id_curso_padre');
            $table->string('nombre');
            $table->string('categoria');
            $table->string('horario');
            $table->date('fecha');
            $table->mediumText('descripcion');
            $table->string('dirigido');
            $table->enum('tipo', array('rinconada', 'curso', 'apa'));
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curso_padre');
    }
}
