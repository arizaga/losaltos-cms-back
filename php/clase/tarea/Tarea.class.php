<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/tarea/TareaConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Tarea extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_tarea = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."tarea/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."tarea/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			
			$sql ="SELECT ta.*, cla.id_clase, cla.materia AS nombre_clase
				FROM tarea AS ta
				LEFT JOIN clase AS cla ON (cla.id_clase = ta.id_clase)
				WHERE ta.id_tarea LIKE '$id_tarea'";
				
			parent::Registro( $id_tarea , $sql);
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case TareaConstantes::$_listado_tarea:
					$sql = "SELECT ta.*, cla.id_clase, cla.materia AS nombre_clase
						FROM tarea AS ta
						LEFT JOIN clase AS cla ON (cla.id_clase = ta.id_clase)";
					break;
					
				case TareaConstantes::$_listado_clase_tarea:
					$sql	= " SELECT *
							  FROM tarea AS t
					     LEFT JOIN clase AS c ON (t.id_clase = c.id_clase) ";
			
					break;
				
				case TareaConstantes::$_listado_tarea_por_alumno:
					$sql	= " SELECT t.*
							FROM tarea AS t
							LEFT JOIN alumno_has_tarea AS aht ON (t.id_tarea = aht.id_tarea)
							LEFT JOIN alumno AS a ON (a.id_alumno = aht.id_alumno)
							WHERE aht.id_alumno = '@@' ";
			
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "tarea";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
