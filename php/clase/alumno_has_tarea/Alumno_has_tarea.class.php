<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/alumno_has_tarea/Alumno_has_tareaConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Alumno_has_tarea extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_alumno_has_tarea = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."alumno_has_tarea/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."alumno_has_tarea/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_alumno_has_tarea );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case Alumno_has_tareaConstantes::$_listado_alumno_has_tarea:
					$sql = "SELECT *
					          FROM alumno_has_tarea";
					break;
					
				case Alumno_has_tareaConstantes::$_listado_alumno_alumno_has_tarea:
					$sql	= " SELECT *
							  FROM alumno_has_tarea AS a
					     LEFT JOIN alumno AS a ON (a.id_alumno = a.id_alumno) ";
			
					break;
									
				case Alumno_has_tareaConstantes::$_listado_tarea_alumno_has_tarea:
					$sql	= " SELECT *
							  FROM alumno_has_tarea AS a
					     LEFT JOIN tarea AS t ON (a.id_tarea = t.id_tarea) ";
			
					break;
									
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "alumno_has_tarea";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
