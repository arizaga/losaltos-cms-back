<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/alumno/AlumnoConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Alumno extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_alumno = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."alumno/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."alumno/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_alumno );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case AlumnoConstantes::$_listado_alumno:
					$sql = "SELECT *
					          FROM alumno";
					break;
					
				case AlumnoConstantes::$_listado_familia_alumno:
					$sql	= " SELECT *
							  FROM alumno AS a
					     LEFT JOIN familia AS f ON (a.id_familia = f.id_familia) ";
			
					break;
				
				case AlumnoConstantes::$_listado_alumno_por_familia:
					$sql	= " SELECT a.*,CONCAT(a.nombre_alumno, ' ', a.apellido_paterno, ' ', a.apellido_materno) AS nombre_completo, f.nombre_familia, f.id_familia
							FROM alumno AS a
							LEFT JOIN familia AS f ON (a.id_familia = f.id_familia)
							WHERE a.id_familia = '@@' ";
			
					break;
				case "listado_alumno_calificacion":
					$sql	= " SELECT c.*,m.materia,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL08' , 'EVAL01') ,
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL11', 'EVAL02'))
						AND per.columna=1
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL08' , 'EVAL01') ,
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL11', 'EVAL02'))
							AND per.columna=1
							LIMIT 1
						), IF(calificacion = 'P I', 'P I' , IF(calificacion = 'BIM I', 'BIM I' , '-')))) AS p1,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL09' , 'EVAL02') ,
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL21', 'EVAL04'))
						AND per.columna=2
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL09' , 'EVAL02') ,
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL21', 'EVAL04'))
							AND per.columna=2
							LIMIT 1
						), IF(calificacion = 'P I', 'P III' , IF(calificacion = 'BIM I', 'BIM III' , '-')))) AS p2,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL10' , 'EVAL03') ,
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL31', 'EVAL06'))
						AND per.columna=3
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL10' , 'EVAL03') ,
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL31', 'EVAL06'))
							AND per.columna=3
							LIMIT 1
						), IF(calificacion = 'P I', 'P III' , IF(calificacion = 'BIM I', 'BIM III' , '-')))) AS p3,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL11' , 'EVAL04') , 
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL41', 'EVAL08'))
						AND per.columna=4
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL11' , 'EVAL04') , 
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL41', 'EVAL08'))
							AND per.columna=4
							LIMIT 1
						), IF(calificacion = 'P I', 'PROM' , IF(calificacion = 'BIM I', 'BIM IV' , '-')))) AS p4,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL12' , 'EVAL05') ,
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL51', 'EVAL10'))
						AND per.columna=5
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL12' , 'EVAL05') ,
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL51', 'EVAL10'))
							AND per.columna=5
							LIMIT 1
						), IF(calificacion = 'P I', 'E.SEM' , IF(calificacion = 'BIM I', 'BIM V' , '-')))) AS p5,
					IF((
						SELECT calificacion
						FROM calificacion AS cal
						LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
						WHERE cal.id_materia = c.id_materia
						AND cal.id_alumno = @@
						AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL14' , 'EVAL07') ,
											  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL60', 'EVAL12'))
						AND per.columna=6
						LIMIT 1
					) = '' , '-' , IFNULL((
							SELECT calificacion
							FROM calificacion AS cal
							LEFT JOIN periodos AS per ON (cal.id_periodo = per.id_periodo)	
							WHERE cal.id_materia = c.id_materia
							AND cal.id_alumno = @@
							AND per.descripcion = IF(per.grupo = 'E' OR per.grupo = 'F', IF(DATE_FORMAT(NOW(), '%m') < '08', 'EVAL14' , 'EVAL07') ,
												  IF(per.grupo = 'C' OR per.grupo = 'D' OR per.grupo = 'H', 'EVAL60', 'EVAL12'))
							AND per.columna=6
							LIMIT 1
						), IF(calificacion = 'P I', 'CALIF' , IF(calificacion = 'BIM I', 'CALIF' , '-')))) AS final, m.renglon
						FROM calificacion AS c
						LEFT JOIN asignacion_materias AS m ON (c.id_materia = m.id_materia)
						WHERE c.id_alumno = @@
						GROUP BY c.id_materia";
		
					break;
						
				case "combo_grupo":

					$sql	= " SELECT 'Todos' AS nombre, '0' AS id
								UNION ALL SELECT grupo AS nombre, grupo AS id 
								FROM alumno
								GROUP BY grupo
								ORDER BY id
								";
					break;

				case "combo_grado":

					$sql	= " SELECT 'Todos' AS nombre, '0' AS id
								UNION ALL SELECT grado AS nombre,grado AS id 
								FROM alumno
								GROUP BY grado
								ORDER BY id
								";
					break;

				case "combo_seccion":

					$sql	= " SELECT nombre AS nombre, id_seccion AS id 
								FROM seccion
								ORDER BY nombre
								";
					break;

			
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "alumno";
		}
		
		//{FUNCION_CLASE}
		
	}
?>
