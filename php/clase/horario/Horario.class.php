<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/horario/HorarioConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Horario extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_horario = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."horario/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."horario/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_horario );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case HorarioConstantes::$_listado_horario:
					$sql = "SELECT *
					          FROM horario
					          ORDER BY grupo,grado,hora_inicio";
					break;
				
				case "horario_alumno":
					$sql = "SELECT h.*,DATE_FORMAT(hora_inicio ,'%H:%i') AS hora
							FROM horario AS h
							WHERE h.grado = (SELECT grado FROM alumno WHERE id_alumno='@@') 
							 AND h.grupo = (SELECT grupo FROM alumno WHERE id_alumno='@@')
							 ORDER BY h.hora_inicio";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "horario";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
