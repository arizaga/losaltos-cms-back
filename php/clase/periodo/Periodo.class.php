<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/periodo/PeriodoConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Periodo extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_periodo = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."periodo/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."periodo/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_periodo );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case PeriodoConstantes::$_listado_periodo:
					$sql = "SELECT *
					          FROM periodo";
					break;
				
				case PeriodoConstantes::$_combo_periodo:
					$sql = "SELECT id_periodo AS id, nombre FROM periodo ORDER BY nombre";
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "periodo";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
