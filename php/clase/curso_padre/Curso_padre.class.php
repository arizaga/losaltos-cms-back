<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/curso_padre/Curso_padreConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Curso_padre extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_curso_padre = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."curso_padre/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."curso_padre/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			$sql = "SELECT *,IF(tipo='rinconada','Rinconada',(IF(tipo='curso','Escuela para padres',''))) AS tipo_nombre
					FROM curso_padre
					WHERE id_curso_padre = '$id_curso_padre'
			";
			parent::Registro( $id_curso_padre ,$sql);
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case Curso_padreConstantes::$_listado_curso_padre:
					$sql = "SELECT *
					          FROM curso_padre";
					break;
				
				case Curso_padreConstantes::$_listado_curso_padre_por_tipo:
					$sql = "SELECT *
					        FROM curso_padre
						WHERE tipo LIKE '@@'
						ORDER BY fecha ASC";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "curso_padre";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
