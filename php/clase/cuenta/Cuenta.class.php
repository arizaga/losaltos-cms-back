<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/cuenta/CuentaConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Cuenta extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_cuenta = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."cuenta/original/";
			$this->directorio_thumbs    = CoreConstantes::$ruta_imagenes ."cuenta/thumb/";
			$this->ancho_thumb          = 60;
			$this->alto_thumb           = 60;
			parent::Registro( $id_cuenta );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case CuentaConstantes::$_listado_cuenta:
					$sql = "SELECT *
					          FROM cuenta";
					break;
					
				case CuentaConstantes::$_listado_alumno_cuenta:
					$sql	= " SELECT *
							  FROM cuenta AS c
					     LEFT JOIN alumno AS a ON (c.id_alumno = a.id_alumno) ";
			
					break;
				
				case CuentaConstantes::$_listado_cuenta_por_alumno:
					$sql	= " SELECT a.*, c.*
							FROM cuenta AS c
							LEFT JOIN alumno AS a ON (c.id_alumno = a.id_alumno)
							WHERE c.id_alumno = '@@' ";
			
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "cuenta";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
