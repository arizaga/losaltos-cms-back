<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/maestro/MaestroConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Maestro extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_maestro = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."maestro/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."maestro/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_maestro );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case MaestroConstantes::$_listado_maestro:
					$sql = "SELECT *
					          FROM maestro";
					break;
				
				case MaestroConstantes::$_combo_maestro:
					$sql = "SELECT id_maestro AS id, nombre FROM maestro ORDER BY nombre";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "maestro";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
