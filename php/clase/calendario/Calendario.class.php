<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/calendario/CalendarioConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Calendario extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_calendario = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."calendario/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."calendario/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			
			$sql ="SELECT ca.*, pe.id_periodo, pe.nombre AS nombre_periodo, tc.id_tipo_calendario, tc.nombre AS nombre_tipo_calendario
				FROM calendario AS ca
				LEFT JOIN periodo AS pe ON (pe.id_periodo = ca.id_periodo)
				LEFT JOIN tipo_calendario AS tc ON (tc.id_tipo_calendario = ca.id_tipo_calendario)
				WHERE ca.id_calendario LIKE '$id_calendario'";
				
			parent::Registro( $id_calendario , $sql);
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case CalendarioConstantes::$_listado_calendario:
					$sql = "SELECT *
					          FROM calendario";
					break;
				
				case CalendarioConstantes::$_listado_periodo_calendario:
					$sql	= " SELECT *
							  FROM calendario AS c
					     LEFT JOIN periodo AS p ON (c.id_periodo = p.id_periodo) ";
			
					break;
									
				case CalendarioConstantes::$_listado_tipo_calendario_calendario:
					$sql	= " SELECT *
							  FROM calendario AS c
					     LEFT JOIN tipo_calendario AS t ON (c.id_tipo_calendario = t.id_tipo_calendario) ";
			
					break;
				
				case CalendarioConstantes::$_listado_calendario_x_periodo:
					$sql	= " SELECT *
							  FROM calendario AS c
					     WHERE id_periodo LIKE '@@'
					     ORDER BY fecha DESC";
			
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "calendario";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
