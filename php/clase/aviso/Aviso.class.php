<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/aviso/AvisoConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Aviso extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_aviso = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."aviso/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."aviso/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_aviso );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case AvisoConstantes::$_listado_aviso:
					$sql = "SELECT *
					          FROM aviso";
					break;
				
				case AvisoConstantes::$_listado_aviso_por_tipo:
					$sql = "SELECT *
					        FROM aviso
						WHERE tipo LIKE '@@'
						ORDER BY fecha 
						LIMIT 20";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "aviso";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
