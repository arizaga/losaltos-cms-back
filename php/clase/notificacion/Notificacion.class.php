<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/notificacion/NotificacionConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Notificacion extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_notificacion = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."notificacion/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."notificacion/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_notificacion );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case NotificacionConstantes::$_listado_notificacion:
					$sql = "SELECT *
					          FROM notificacion 
					    
					          ";
					break;
					
				case NotificacionConstantes::$_listado_alumno_notificacion:
					$sql	= " SELECT *
							  FROM notificacion AS n
					     LEFT JOIN alumno AS a ON (n.id_alumno = a.id_alumno) ";
					break;
				
				case NotificacionConstantes::$_listado_notificacion_por_alumno:
					$sql	= " SELECT n.*
							FROM notificacion AS n
							WHERE (n.grado = (SELECT grado FROM alumno WHERE id_alumno='@@') 
							 AND n.grupo = (SELECT grupo FROM alumno WHERE id_alumno='@@') )
							 OR (n.grado = '0' AND  n.grupo = (SELECT grupo FROM alumno WHERE id_alumno='@@') )
							 OR (n.grado = '0' AND  n.grupo = '0' )";
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "notificacion";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
