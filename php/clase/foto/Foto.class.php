<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/foto/FotoConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Foto extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_foto = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."foto/original/";
			$this->directorio_thumbs	= array(CoreConstantes::$ruta_imagenes ."foto/thumb/",
									CoreConstantes::$ruta_imagenes ."foto/main/");
			$this->ancho_thumb			= array(212,640);
			$this->alto_thumb			= array(212,640);
			
			parent::Registro( $id_foto );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case FotoConstantes::$_listado_foto:
					$sql = "SELECT *
					          FROM foto AS f
					          WHERE f.grupo LIKE '%@b%'
					          AND f.grado LIKE '%@a%'";
					break;
					
				case "listado_foto_alumno":
					$sql = "SELECT f.*
					          FROM foto AS f
					          WHERE f.grupo = (SELECT grupo FROM alumno WHERE id_alumno=@@)
					          AND f.grado = (SELECT grado FROM alumno WHERE id_alumno=@@)";
					break;

				case "listado_foto_grupo_grado":
					$sql = "SELECT f.*
					          FROM foto AS f
					          WHERE f.grupo = @@a
					          AND f.grado = @@b";
					break;

				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "foto";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
