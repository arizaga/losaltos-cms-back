<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/alumno_has_clase/Alumno_has_claseConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Alumno_has_clase extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_alumno_has_clase = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."alumno_has_clase/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."alumno_has_clase/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_alumno_has_clase );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case Alumno_has_claseConstantes::$_listado_alumno_has_clase:
					$sql = "SELECT *
					          FROM alumno_has_clase";
					break;
					
				case Alumno_has_claseConstantes::$_listado_boleta_has_clase_alumno_has_clase:
					$sql	= " SELECT *
							  FROM alumno_has_clase AS a
					     LEFT JOIN boleta_has_clase AS b ON (a.id_boleta_has_clase = b.id_boleta_has_clase) ";
			
					break;
					
				case Alumno_has_claseConstantes::$_listado_alumno_alumno_has_clase:
					$sql	= " SELECT *
							  FROM alumno_has_clase AS a
					     LEFT JOIN alumno AS a ON (a.id_alumno = a.id_alumno) ";
			
					break;
					
				case Alumno_has_claseConstantes::$_listado_clase_alumno_has_clase:
					$sql	= " SELECT *
							  FROM alumno_has_clase AS a
					     LEFT JOIN clase AS c ON (a.id_clase = c.id_clase) ";
			
					break;
					
				case Alumno_has_claseConstantes::$_listado_calificaciones_por_alumno:
					$sql	= " SELECT a.*, c.*, ahc.*
							FROM alumno_has_clase AS ahc
							LEFT JOIN clase AS c ON (ahc.id_clase = c.id_clase)
							LEFT JOIN alumno AS a ON (ahc.id_alumno = a.id_alumno)
							LEFT JOIN periodo AS p ON (p.id_periodo = c.id_periodo)
							LEFT JOIN maestro AS m ON (m.id_maestro = c.id_maestro)
							WHERE ahc.id_alumno = '@a' AND c.id_periodo = '@c' ";
			
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "alumno_has_clase";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
