<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/familia/FamiliaConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Familia extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_familia = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."familia/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."familia/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_familia );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case FamiliaConstantes::$_listado_familia:
					$sql = "SELECT *
					          FROM familia";
					break;
				
				case FamiliaConstantes::$_listado_familia_login:
					$sql = "(
								SELECT fam.id_familia AS id_usuario, fam.usuario, 'familia' AS tipo ,
									fam.nombre_familia AS nombre,
							        (
							        	SELECT id_periodo FROM periodo 
							                WHERE ( CAST(NOW() AS DATE) BETWEEN fecha_inicio AND fecha_fin ) 
							                LIMIT 1 
							        ) AS id_periodo
							        FROM familia AS fam 
							        WHERE fam.usuario ='@a' AND fam.pass = '@b'
							)
							UNION ALL
							(
								SELECT a.id_alumno AS id_usuario, a.id_alumno AS usuario, 'alumno' AS tipo ,
									CONCAT(a.nombre_alumno,' ',a.apellido_paterno,' ',a.apellido_materno) AS nombre,
							        (
							        	SELECT id_periodo FROM periodo 
							                WHERE ( CAST(NOW() AS DATE) BETWEEN fecha_inicio AND fecha_fin ) 
							                LIMIT 1 
							        ) AS id_periodo
							        FROM alumno AS a 
							        LEFT JOIN familia AS f ON(a.id_familia = f.id_familia)
							        WHERE a.id_alumno = '@a' AND f.usuario = '@b'
							)
							ORDER BY tipo DESC
							LIMIT 1 ";
					break;
				case 'listado_cuenta_familia':
					# code...
					$sql='(
						SELECT 
						CONCAT(a.nombre_alumno," ",a.apellido_paterno," ",a.apellido_materno) AS nombre_alumno,
						c.id_alumno, 
						SUM(c.importe) AS importe_total,
						SUM(c.importe_abono) AS pagado_total, 
						SUM(c.importe)-SUM(c.importe_abono) AS restante,
						"0" AS total
						FROM cuenta AS c
						LEFT JOIN alumno as a ON(a.id_alumno = c.id_alumno)
						LEFT JOIN familia as f ON(a.id_familia = f.id_familia)
						WHERE f.id_familia=@@
						GROUP BY c.id_alumno
						) 
						UNION(
						SELECT 
						"TOTAL" AS nombre_alumno,
						0 AS id_alumno,
						SUM(c.importe) AS importe_total,
						SUM(c.importe_abono) AS pagado_total, 
						SUM(c.importe)-SUM(c.importe_abono) AS restante,
						"1" AS total

						FROM cuenta AS c
						LEFT JOIN alumno as a ON(a.id_alumno = c.id_alumno)
						LEFT JOIN familia as f ON(a.id_familia = f.id_familia)
						WHERE f.id_familia=@@

						)';
					break;
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "familia";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
