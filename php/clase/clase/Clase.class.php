<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/clase/ClaseConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Clase extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_clase = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."clase/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."clase/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			
			$sql ="SELECT cla.*, ma.id_maestro, ma.nombre AS nombre_maestro,
				pe.id_periodo, pe.nombre AS nombre_periodo
				FROM clase AS cla
				LEFT JOIN maestro AS ma ON (ma.id_maestro = cla.id_maestro)
				LEFT JOIN periodo AS pe ON (pe.id_periodo = cla.id_periodo)
				WHERE cla.id_clase LIKE '$id_clase'";
			
			parent::Registro( $id_clase , $sql);
			
		}
		
		function getSQL($caso_uso){
			switch ($caso_uso) {
				case ClaseConstantes::$_listado_clase:
					$sql = "SELECT cla.*, ma.id_maestro, ma.nombre AS nombre_maestro,
						pe.id_periodo, pe.nombre AS nombre_periodo
						FROM clase AS cla
						LEFT JOIN maestro AS ma ON (ma.id_maestro = cla.id_maestro)
						LEFT JOIN periodo AS pe ON (pe.id_periodo = cla.id_periodo)";
					break;
					
				case ClaseConstantes::$_listado_maestro_clase:
					$sql	= " SELECT *
							  FROM clase AS c
					     LEFT JOIN maestro AS m ON (c.id_maestro = m.id_maestro) ";
			
					break;
									
				case ClaseConstantes::$_listado_periodo_clase:
					$sql	= " SELECT *
							  FROM clase AS c
					     LEFT JOIN periodo AS p ON (c.id_periodo = p.id_periodo) ";
			
					break;
				
				case ClaseConstantes::$_listado_clases_por_alumno:
					$sql	= " SELECT c.*, m.id_maestro, m.nombre
							FROM clase AS c
							LEFT JOIN alumno AS a ON (a.grupo = c.grupo AND a.grado = c.grado)
							LEFT JOIN periodo AS p ON (p.id_periodo = c.id_periodo)
							LEFT JOIN maestro AS m ON (m.id_maestro = c.id_maestro)
							WHERE a.id_alumno = '@a' AND c.id_periodo = '@b' ";
					break;
				
				case ClaseConstantes::$_listado_clases_por_periodo:
					$sql	= " SELECT c.*, m.id_maestro, m.nombre
							FROM clase AS c
							LEFT JOIN alumno AS a ON (a.grupo = c.grupo AND a.grado = c.grado)
							LEFT JOIN periodo AS p ON (p.id_periodo = c.id_periodo)
							LEFT JOIN maestro AS m ON (m.id_maestro = c.id_maestro)
							WHERE c.id_periodo = '@a' ";
					break;

                case ClaseConstantes::$_listado_clases_por_semana:
                    $sql	= " SELECT t.id_tarea, t.nombre, t.fecha_entrega, c.materia,
                                CASE WEEKDAY(t.fecha_entrega)
                                WHEN 0 THEN 'Lunes'
                                WHEN 1 THEN 'Martes'
                                WHEN 2 THEN 'Miercoles'
                                WHEN 3 THEN 'Jueves'
                                WHEN 4 THEN 'Viernes'
                                ELSE '' END AS dia_semana
                                FROM alumno_has_clase AS ahc
                                LEFT JOIN alumno AS a ON ( ahc.id_alumno = a.id_alumno )
                                LEFT JOIN clase AS c ON (c.id_clase = ahc.id_clase)
                                LEFT JOIN periodo AS p ON (p.id_periodo = c.id_periodo)
                                LEFT JOIN tarea AS t ON (c.id_clase = t.id_clase)
                                LEFT JOIN alumno_has_tarea AS aht ON ( aht.id_tarea = t.id_tarea )
                                WHERE a.id_alumno = '@a' AND (t.fecha_entrega BETWEEN '@b' AND DATE_ADD('@b', INTERVAL 4 DAY) OR t.fecha_entrega IS NULL) ";
                    break;

                case "listado_tareas_semana_actual":
                    $sql	= " SELECT a.id_alumno,c.id_clase, c.materia,
                                (
                                	SELECT IF(count(*)>0,CURDATE() - INTERVAL WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() - INTERVAL WEEKDAY(CURDATE()) DAY
                                        
                                ) AS lunes,
                                (
                                	SELECT IF(count(*)>0,CURDATE() + INTERVAL 1 -WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() + INTERVAL 1 - WEEKDAY(CURDATE()) DAY
                                       
                                ) AS martes,
                                (
                                	SELECT IF(count(*)>0,CURDATE() + INTERVAL 2 -WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() + INTERVAL 2 - WEEKDAY(CURDATE()) DAY
                                       
                                ) AS miercoles,
                                (
                                	SELECT IF(count(*)>0,CURDATE() + INTERVAL 3 -WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() + INTERVAL 3 - WEEKDAY(CURDATE()) DAY
                                       
                                ) AS jueves,
                                (
                                	SELECT IF(count(*)>0,CURDATE() + INTERVAL 4 -WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() + INTERVAL 4 - WEEKDAY(CURDATE()) DAY
                                       
                                ) AS viernes,
                                (
                                	SELECT IF(count(*)>0,CURDATE() + INTERVAL 5 -WEEKDAY(CURDATE()) DAY,'')
                                    FROM tarea AS tar 
                                    WHERE tar.id_clase = c.id_clase
                                    AND fecha_entrega = CURDATE() + INTERVAL 5 - WEEKDAY(CURDATE()) DAY
                                       
                                ) AS sabado
                                FROM clase as c
                                LEFT JOIN alumno AS a ON ( a.grado = c.grado AND a.grupo=c.grupo )
                                WHERE a.id_alumno = '@a' AND c.id_periodo = '@b'";
                    break;

                case "listado_tareas_dia":
                    $sql	= " SELECT c.id_clase, c.materia,t.*,DATE_FORMAT(t.fecha_entrega,'%d/%m/%Y') AS fecha
                                FROM clase as c
                                LEFT JOIN tarea AS t ON (t.id_clase=c.id_clase)
                                WHERE c.id_clase = '@a' AND t.fecha_entrega = '@b' ";
                               
                    break;
				
				case ClaseConstantes::$_combo_clase:
					$sql = "SELECT id_clase AS id, materia AS nombre FROM clase ORDER BY nombre";
					break;
				
				/*
				case ClaseConstantes::$_listado_horario_alumno:
					$sql	= " SELECT c.*, m.id_maestro, m.nombre
							FROM clase AS c
							LEFT JOIN alumno_has_clase AS ahc ON (c.id_clase = ahc.id_clase)
							LEFT JOIN alumno AS a ON (a.id_alumno = ahc.id_alumno)
							LEFT JOIN periodo AS p ON (p.id_periodo = c.id_periodo)
							LEFT JOIN maestro AS m ON (m.id_maestro = c.id_maestro)
							WHERE ahc.id_alumno = '@a' AND c.id_periodo = '@b' ";
					break;
				*/
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "clase";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
