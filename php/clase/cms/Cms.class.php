<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/cms/CmsConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Cms extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_cms = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."cms/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."cms/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_cms );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case CmsConstantes::$_listado_cms:
					$sql = "SELECT *
					          FROM cms";
					break;
				
				case "listado_cms_login":
					$sql = "	SELECT c.*
						        FROM cms AS c
						        WHERE user ='@a' AND pass = '@b'
							";
					break;

				case "check_token":
					$sql = "	SELECT c.*
						        FROM cms AS c
						        WHERE id_cms ='@a' AND token = '@b'
							";
					break;
					
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "cms";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
