<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../clase/tipo_calendario/Tipo_calendarioConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Tipo_calendario extends Registro{
		
		
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_tipo_calendario = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."tipo_calendario/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."tipo_calendario/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_tipo_calendario );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				case Tipo_calendarioConstantes::$_listado_tipo_calendario:
					$sql = "SELECT *
					          FROM tipo_calendario";
					break;
				
				case Tipo_calendarioConstantes::$_combo_tipo_calendario:
					$sql = "SELECT id_tipo_calendario AS id, nombre FROM tipo_calendario ORDER BY nombre";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "tipo_calendario";
		}
		
		//{FUNCION_CLASE}
		
	}

?>
