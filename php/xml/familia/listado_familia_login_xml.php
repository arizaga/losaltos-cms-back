<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$familia  = new Listado(FamiliaConstantes::$_listado_familia_login,"Familia");
	$familia->replaceParametros( Request::getParametros(array('usuario','pass')), array('@a','@b') );
	echo $familia->showListado();

	
?>