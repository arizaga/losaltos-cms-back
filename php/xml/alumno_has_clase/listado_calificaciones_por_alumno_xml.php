<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$alumno_has_clase  = new Listado(Alumno_has_claseConstantes::$_listado_calificaciones_por_alumno,"Alumno_has_clase");
	$alumno_has_clase->replaceParametros( Request::getParametros(array('id_alumno','id_periodo')), array('@a','@c') );
	echo $alumno_has_clase->showListado();

	
?>