<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$cuenta  = new Listado(CuentaConstantes::$_listado_cuenta_por_alumno,"Cuenta");
	$cuenta->replaceParametros( Request::getParametros(array('id_alumno')) , array('@@') );
	echo $cuenta->showListado();

	
?>