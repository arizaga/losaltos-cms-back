<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$familia  = new Listado("listado_cms_login","Cms");
	$familia->replaceParametros( Request::getParametros(array('user','pass')), array('@a','@b') );
	echo $familia->showListado();

	
?>