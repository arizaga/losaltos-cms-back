<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$familia  = new Listado("check_token","Cms");
	$familia->replaceParametros( Request::getParametros(array('id_cms','token')), array('@a','@b') );
	echo $familia->showListado();

	
?>