<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$curso_padre  = new Listado(Curso_padreConstantes::$_listado_curso_padre_por_tipo,"Curso_padre");
	$curso_padre->replaceParametros( Request::getParametros(array('tipo')), array('@@') );
	echo $curso_padre->showListado();

	
?>