<?php
	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/AllRegistro/AllRegistro.class.php");
	require_once("../../framework/Listado/Listado.class.php");

	function __autoload($clase){
		manipulateRequire($clase);
	}
	

	$tabla = Request::getParametro("tabla");
	$tabla_capital = ucfirst( $tabla );

	if( $tabla && Request::getParametro("accion")!='listado' ){
		
		$obj = new AllRegistro($tabla_capital);									
		$obj->showId();
		$obj->makeOperaciones();
	}
	elseif (Request::getParametro("accion")=='listado') {

		$listado = Request::getParametro("listado");

		$obj  = new Listado($listado?$listado:'listado_'.$tabla,$tabla);

		$replace = Request::getParametro("replace");
		$replacing = Request::getParametro("replacing");
		if($replace&&$replacing){
			$replace_array = explode(",",$replace);
			$replacing_array = explode(",",$replacing);
			$obj->replaceParametros( Request::getParametros($replace_array) , $replacing_array );
		}

		echo $obj->showListado();
	}
	else{
		echo '<?xml version="1.0" encoding="iso-8859-1"?>
<RAIZ OK="0" MENSAJE="Error"/>';
	}
	
	
	
	
?>