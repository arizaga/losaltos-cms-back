<?php 

//$link = mysqli_connect("internal-db.s16338.gridserver.com","db16338_arizaga","arizagadd","db16338_losaltos_app") or die("Error " . mysqli_error($link)); 

$link = mysqli_connect("localhost","root","root","losaltos-cms") or die("Error " . mysqli_error($link)); 

function addIOS($device, $family){
	global $link;
	$query = "SELECT dispositivo FROM dispositivos_ios WHERE dispositivo = '$device'" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	if($result->num_rows == 0)
		$query = "INSERT INTO dispositivos_ios (dispositivo, familia) VALUES ('$device', '$family')" or die("Error in the consult.." . mysqli_error($link));
	else
		$query = "UPDATE dispositivos_ios SET familia = '$family' WHERE dispositivo = '$device'" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
}

function addAndroid($device, $family){
	global $link;
	$query = "SELECT dispositivo FROM dispositivos_android WHERE dispositivo = '$device'" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	if($result->num_rows == 0)
		$query = "INSERT INTO dispositivos_android (dispositivo, familia) VALUES ('$device', '$family')" or die("Error in the consult.." . mysqli_error($link));
	else
		$query = "UPDATE dispositivos_android SET familia = '$family' WHERE dispositivo = '$device'" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
}

function getIOSDevices(){
	global $link;
	$query = "SELECT dispositivo FROM dispositivos_ios WHERE 1" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}

function getAndroidDevices(){
	global $link;
	$query = "SELECT dispositivo FROM dispositivos_android WHERE 1" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}
function getAndroidDevicesByGroup($grade, $group){
	global $link;
	$query = "	SELECT d.dispositivo 
				FROM dispositivos_android d, alumno a
				WHERE a.grado = '$grade'
				AND a.grupo = '$group'
				AND a.id_familia = d.familia
				" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}
function getIOSDevicesByGroup($grade, $group){
	global $link;
	$query = "	SELECT d.dispositivo 
				FROM dispositivos_ios d, alumno a
				WHERE a.grado = '$grade'
				AND a.grupo = '$group'
				AND a.id_familia = d.familia
				" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}
function getAndroidDevicesByGrade($grade){
	global $link;
	$query = "	SELECT d.dispositivo 
				FROM dispositivos_android d, alumno a
				WHERE a.grado = '$grade'
				AND a.id_familia = d.familia
				" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}
function getIOSDevicesByGrade($grade){
	global $link;
	$query = "	SELECT d.dispositivo 
				FROM dispositivos_ios d, alumno a
				WHERE a.grado = '$grade'
				AND a.id_familia = d.familia
				" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	$devices = array();
	while($row = mysqli_fetch_array($result)){
		$devices[] = $row['dispositivo'];
	}
	return $devices;
}
function pushAndroid($ids, $title, $message){
	// API access key from Google API's Console
	define( 'API_ACCESS_KEY', 'AIzaSyDaUEwEVN1h1udShNgCCDgiM-cDuVRa7uE' );


	$registrationIds = array();
	$registrationIds = $ids;

	if($message == null)
	{
		$message = "Centro Escolar Los Altos";
	}

	// prep the bundle
	$msg = array
	(
	    'alert' 		=> $message,
		'title'			=> 'Nuevo mensaje',
		'subtitle'		=> $title,
		'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
		'vibrate'		=> 1,
		'sound'			=> 1,
		'notificacionId'=> (GetLastNotificactionId())
	);

	$fields = array
	(
		'registration_ids' 	=> $registrationIds,
		'data'				=> $msg
	);
	 
	$headers = array
	(
		'Authorization: key=' . API_ACCESS_KEY,
		'Content-Type: application/json'
	);

	
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	//curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );

	//echo $result;
	//CheckErrors(json_decode($result)->results, $fields['registration_ids']);
   
}

function CheckErrors($result , $registration_ids)
{
	global $link;

	//var_dump($registration_ids);
	//var_dump($result);

	foreach ($result as $key => $value) 
	{
		$value = (array)$value;

		if($value['message_id'] != NULL)
		{
			if(sizeof($value == 1))
				return;

			if($value['registration_id'] != NULL)
			{
				$newId = $value['registration_id'];
				$oldId = $registration_ids[$key];
				$query = "UPDATE dispositivos_android SET dispositivo = '$newId' WHERE dispositivo = '$oldId'" or die("Error in the consult.." . mysqli_error($link));
				$result = mysqli_query($link, $query);
				//echo '<br>Deprecada  ' . $key;
			}
			else
			{
				//echo '<br>Correcto  ' . $key;
			}
		}
		else
		{
			if($value['error'] == 'NotRegistered')
			{
				$deleteId = $registration_ids[$key];
				$query = "DELETE from dispositivos_android WHERE dispositivo = '$deleteId'" or die("Error in the consult.." . mysqli_error($link));
				$result = mysqli_query($link, $query);
				//echo '<br>Tiene error  ' . $key;
			}
		}
	}
}

function pushIOS($ids, $message){

	$badge = 1;
	$sound = 'default';

	if($message == null)
	{
		$message = "Centro Escolar Los Altos";
	}

	$payload = array();
	$payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound, 'notificacionId' => (GetLastNotificactionId() + 1)); //AGREGAR AQUI 'notificacionId'
	$payload = json_encode($payload);

	$apns_url = NULL;
	$apns_cert = NULL;
	$apns_port = 2195;

	$apns_url = 'gateway.push.apple.com';
	$apns_cert = 'pem/pushCert.pem';

	$stream_context = stream_context_create();
	stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);
	//$apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, 20, STREAM_CLIENT_CONNECT, $stream_context);

	//	You will need to put your device tokens into the $device_tokens array yourself
	$device_tokens = array();
	$device_tokens = $ids;

	foreach($device_tokens as $device_token)
	{
		$apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, 20, STREAM_CLIENT_CONNECT, $stream_context);
		$device_token = substr($device_token, 0, -1);
		$device_token = substr($device_token, 1);
		//echo $device_token;
		$apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_token)) . chr(0) . chr(strlen($payload)) . $payload;
		fwrite($apns, $apns_message);
	}

	//socket_close($apns);
	//fclose($apns);
}

function GetLastNotificactionId()
{
	global $link;
	$query = "	SELECT MAX(id_notificacion) AS id
				FROM notificacion
				" or die("Error in the consult.." . mysqli_error($link));
	$result = mysqli_query($link, $query);
	
	$row =  mysqli_fetch_assoc($result);
	return intval($row['id']);
}

?>
