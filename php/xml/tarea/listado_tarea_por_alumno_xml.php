<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$tarea  = new Listado(TareaConstantes::$_listado_tarea_por_alumno,"Tarea");
	$tarea->replaceParametros( Request::getParametros(array('id_alumno')) , array('@@') );
	echo $tarea->showListado();

	
?>