<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$foto  = new Listado(FotoConstantes::$_listado_foto,"Foto");
	$foto->replaceParametros( Request::getParametros(array('grado','grupo')), array('@a','@b') );
	echo $foto->showListado();

	
?>