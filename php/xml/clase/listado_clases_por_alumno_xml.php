<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$clase  = new Listado(ClaseConstantes::$_listado_clases_por_alumno,"Clase");
	$clase->replaceParametros( Request::getParametros(array('id_alumno','id_periodo')) , array('@a','@b') );
	echo $clase->showListado();

	
?>