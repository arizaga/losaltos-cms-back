<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$clase  = new Listado(ClaseConstantes::$_listado_clases_por_periodo,"Clase");
	$clase->replaceParametros( Request::getParametros(array('id_periodo')) , array('@a') );
	echo $clase->showListado();

	
?>