<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$clase  = new Listado(ClaseConstantes::$_listado_clases_por_semana,"Clase");
	$clase->replaceParametros( Request::getParametros(array('id_alumno','fecha_inicio')) , array('@a','@b') );
	echo $clase->showListado();

	
?>