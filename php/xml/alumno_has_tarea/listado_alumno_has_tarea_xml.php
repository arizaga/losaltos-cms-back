<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$alumno_has_tarea  = new Listado(Alumno_has_tareaConstantes::$_listado_alumno_has_tarea,"Alumno_has_tarea");
	echo $alumno_has_tarea->showListado();

	
?>