<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$tipo_calendario  = new Listado(Tipo_calendarioConstantes::$_listado_tipo_calendario,"Tipo_calendario");
	echo $tipo_calendario->showListado();

	
?>