<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	$periodo  = new Listado(PeriodoConstantes::$_listado_calendario_x_periodo,"Calendario");
	$periodo->replaceParametros( Request::getParametros(array('id_periodo')) , array('@@') );
	echo $periodo->showListado();
	
?>