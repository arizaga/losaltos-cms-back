<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$calendario  = new Listado(CalendarioConstantes::$_listado_calendario,"Calendario");
	echo $calendario->showListado();

	
?>