<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$aviso  = new Listado(AvisoConstantes::$_listado_aviso_por_tipo,"Aviso");
	$aviso->replaceParametros( Request::getParametros(array('tipo')), array('@@') );
	echo $aviso->showListado();

	
?>