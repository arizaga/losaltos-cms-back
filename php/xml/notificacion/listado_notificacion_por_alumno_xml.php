<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$notificacion  = new Listado(NotificacionConstantes::$_listado_notificacion_por_alumno,"Notificacion");
	$notificacion->replaceParametros( Request::getParametros(array('id_alumno')) , array('@@') );
	echo $notificacion->showListado();

	
?>