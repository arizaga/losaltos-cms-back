<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Listado/Listado.class.php");
	
	function __autoload($clase){
		manipulateRequire($clase);
	}
	
	
	$alumno  = new Listado(AlumnoConstantes::$_listado_alumno_por_familia,"Alumno");
	$alumno->replaceParametros( Request::getParametros(array('id_familia')) , array('@@') );
	echo $alumno->showListado();

	
?>