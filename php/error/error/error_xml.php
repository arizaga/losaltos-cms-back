<?php

	require_once("../../framework/ErrorHandler/ErrorHandler.class.php");
	require_once("../../framework/Request/Request.class.php");
	require_once("../../framework/ExcepcionSistema/ExcepcionSistemaConstantes.class.php");

	function __autoload($clase){
		manipulateRequire($clase);
	}

	$xml = new Xml();
	switch (Request::getParametro("tipo_error")) {
		
		case ExcepcionSistemaConstantes::$critical:
			echo $xml->addMensajeError($_REQUEST['error'],true);	
			break;
			
		case ExcepcionSistemaConstantes::$logical:
			echo $xml->addMensajeError($_REQUEST['error'],true);	
			break;

		case ExcepcionSistemaConstantes::$warning:
			echo $xml->addMensajeWarning($_REQUEST['error'],true);	
			break;
			
	
		default:
			echo $xml->addMensajeError($_REQUEST['error'],true);	
			break;
	}
	
?>