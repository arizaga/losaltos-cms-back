<?php

require_once("../../framework/Logger/LoggerConstantes.class.php");
require_once("../../framework/XmlWeb/XmlWebConstantes.class.php");
require_once("../../framework/ErrorConstantes.class.php");
require_once("../../framework/CoreConstantes.class.php");
require_once("../../framework/Redirection/Redirection.class.php");

class XmlWeb{

	private $doc 		= null;
	private $elemento	= null;
	private $archivo	= "";
	private $carpeta	= "";

	public function getArchivo(){
		return $this->archivo;
	}
	
	public function setArchivo($archivo){
		$this->archivo = $archivo;
	}
	
	public function getCarpeta(){
		return $this->carpeta;
	}
	
	public function setCarpeta($carpeta){
		$this->carpeta = $carpeta;
	}
	
	public function __construct(){
		$documento 	= null;
		try{
			$this->doc		= new DOMDocument("1.0","iso-8859-1");
			$this->elemento = null;
			if(!@$this->doc->load(XmlWebConstantes::$nombre_archivo)){
					throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_01,
												ErrorConstantes::$int_error_xml_01,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
			}			
			$this->carpeta = strtolower($_REQUEST[XmlWebConstantes::$VR_NODO_XML]);
			$this->readXML( $this->doc->getElementsByTagName(
								strtoupper($_REQUEST[XmlWebConstantes::$VR_NODO_XML])));
			$this->redirectionArchivo();			

		}catch (ExcepcionSistema   $e){
			$e->muestraError();
		}
	}

	public function redirectionArchivo(){
		try{
			$url = 	CoreConstantes::$ruta_pagina . CoreConstantes::$carpeta_xml .
					$this->carpeta . "/" . $this->archivo;
			$redirecciona = new Redirection($url);
			$resultado = $redirecciona->executeRedireccion();
						
			if(!$resultado){				
			throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_11,
										ErrorConstantes::$int_error_xml_11,
										LoggerConstantes::$LOG_NIVEL_CRITICO);

			}			
		}catch (ExcepcionSistema   $e){
			$e->muestraError();
		}
	}

	public function readXML( $nodos = null ){

		$longitud	= 0;

		try{
			
			$longitud  = $nodos->length;
			if($longitud > 0){
				foreach ($nodos as $nodo) {
					
					if($nodo->tagName == strtoupper($_REQUEST[XmlWebConstantes::$VR_NODO_XML]) ){
						if($nodo->hasChildNodes()){	
							$hijos = $nodo->childNodes;
							$hijo_solicitado = strtoupper($_REQUEST[XmlWebConstantes::$VR_CAMPO_ACCION]);	
							
							foreach ($hijos as $hijo) {																
								if($hijo->tagName == $hijo_solicitado){
									$this->archivo = $hijo->nodeValue;
								}								
							}							
							
						}
						else {
							throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_04,
														ErrorConstantes::$int_error_xml_04,
														LoggerConstantes::$LOG_NIVEL_CRITICO);	
						}
					}
				}
			}

			else {
				throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_02,
							ErrorConstantes::$int_error_xml_02,
							LoggerConstantes::$LOG_NIVEL_CRITICO);
			}

		}catch(ExcepcionSistema $es){
			$es->muestraError();
		}

		
	}


}








?>