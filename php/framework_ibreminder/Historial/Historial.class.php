<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../framework/Historial/HistorialConstantes.class.php");
	require_once("../../framework/AllRegistro/AllRegistroConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Historial extends Registro{
		
		function __construct(&$id_historial = 0){
			parent::Registro( $id_historial);
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				
				case HistorialConstantes::$_listado_historial:
						$sql = "SELECT *
						          FROM historial";
						break;
						
					case HistorialConstantes::$_listado_usuario_historial:
						$sql	= " SELECT *
								  FROM historial AS h
						     LEFT JOIN usuario AS u ON (h.id_usuario = u.id_usuario) ";
				
						break;
						
					case HistorialConstantes::$_busqueda_historial:
						$sql 	= "SELECT *
									 FROM historial AS h
								LEFT JOIN usuario As u ON (h.id_usuario = u.id_usuario)
								    WHERE @@";
						break;
										
					//{CASE_LISTADO}
					
					default:
						$sql = "SELECT 'Error'";
						break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "historial";
		}
		
		function addHistorial($accion = "",$modulo = "",$registro = ""){
			$accion = !$accion ?  Request::getParametro("accion") : $accion;
			try{
				switch ($accion){
					
					case AllRegistroConstantes::$agregar:
						$mensaje = "Agrego al modulo: ".$modulo . " el registro ". $registro;
						break;
						
					case AllRegistroConstantes::$editar:
						$mensaje = "Modific� en el modulo: ".$modulo . " el registro ". $registro;
						break;										
						
					case AllRegistroConstantes::$eliminacion_logica:
						$mensaje = "Desactivo en el modulo: ". $modulo . " el registro ".$registro;
						break;
						
					case HistorialConstantes::$_login_valido:
						$mensaje = "Logueo v�lido del usuario: ". $registro;
						break;										
						
					case HistorialConstantes::$_login_invalido:
						$mensaje = "Logueo inv�lido del usuario: ". $registro;
						break;
						
					default:
						$mensaje= "accion no seleccionada";
						break;
				}
				$values = array();
				$values["mensaje"] 		= $mensaje;
				$values["id_usuario"]	= Request::getParametro("usuario_logueado");
				$values["fecha"]		= date("Y-m-d H:i:s");
				$OK = $this->insertRegistro("",$values);
				if(!$OK){
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_48,
												ErrorConstantes::$int_error_xml_48,
												LoggerConstantes::$LOG_NIVEL_ALMACENAMIENTO);
				}
			}catch (ExcepcionSistema $e){
					$e->muestraError();
			}
			return $OK;
		}
		
		function validateFiltro(){
			$sql = array();
			
			if(($fecha_inicial = Request::getParametro("fecha_inicial"))){
				$sql[] = " fecha >= '$fecha_inicial 00:00:00'";
			}
			if(($fecha_final = Request::getParametro("fecha_final"))){
				$sql[] = " fecha <= '$fecha_final 23:59:59' ";
			}
			if(($usuario = Request::getParametro("usuario"))){
				$sql[] = "u.nombre_usuario LIKE '%$usuario%'";
			}
			$sql = implode(" AND ",$sql);
			
			return $sql;
		}		
		//{FUNCION_CLASE}
		
	}

?>