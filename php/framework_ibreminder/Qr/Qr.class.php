<?
	require('qrlib.php');

	Class Qr{
		
		private $cadena            = "";
		private $tamano            = 4;
		private $instancia         = "";
		private $destino	   = "";
		private $nombre_archivo_qr = "";
		private $ruta_temp	   = '../../framework/Qr/temp/';
	
		
		public function __construct($instancia, $destino = ""){
			$this->instancia = $instancia;
			$this->destino   = $destino;
		}
		
		public function setCadena($cadena_nueva){
			$this->cadena = $cadena_nueva;
		}
		
		public function setTamano($tamano_nuevo){
			$this->tamano = $tamano_nuevo;
		}
		
		public function imprimeImagen(){
			$PNG_TEMP_DIR = "";
			$PNG_WEB_DIR = "";
			//Establece a ubicaci�n de escritura, un lugar de temperatura generada archivos PNG
			$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
			
			//�Prefijo lugar / / html PNG
			$PNG_WEB_DIR = 'temp/';
			
			//Por supuesto necesitamos derechos para crear temp dir
			if(!file_exists($PNG_TEMP_DIR))
				mkdir($PNG_TEMP_DIR);
			
			$errorCorrectionLevel = 'H';
			$matrixPointSize = $this->tamano; /*Un punto equivale a 25px*/
			
			// user data
			$this->nombre_archivo_qr = $PNG_TEMP_DIR.'qr_'. substr(md5($this->cadena) , 0 , 16 ). '.png';
			QRcode::png($this->cadena, $this->nombre_archivo_qr, $errorCorrectionLevel, $matrixPointSize, 1);
			      
			    
			//display generated file
			//echo '<img rel="este_es" src="' . $this->ruta_temp . basename($this->nombre_archivo_qr).'" /><br />';
			$this->nombre_archivo_qr = basename($this->nombre_archivo_qr);
			//echo $this->instancia->directorio_thumbs;
			
			$this->switchOpcion();
			
		}
		
		public function switchOpcion(){
			
			switch (Request::getParametro(AllRegistroConstantes::$accion)) {
				case AllRegistroConstantes::$agregar:
					$this->addEditQr();
					break;
					
				case AllRegistroConstantes::$editar:
					$this->addEditQr();
					break;
			
				case AllRegistroConstantes::$eliminar:
					$this->deleteQr();
					break;
				
				default:
					break;
			}
		}
		
		public function addEditQr(){
			$arreglo_registro = array();
			
			if(Request::getParametro(AllRegistroConstantes::$accion) == AllRegistroConstantes::$editar){
				$arreglo_registro = $this->instancia->getRegistroPorId($this->instancia->id_registro, $this->instancia->getNombreTabla());
				unlink($this->instancia->directorio_thumbs . "/" . $arreglo_registro[$this->destino]);
			}
			
			
			$sql = 'UPDATE '. $this->instancia->getNombreTabla() . " SET $this->destino " . ' = ' . "'" . $this->nombre_archivo_qr . "'" .
				' WHERE id_' . $this->instancia->getNombreTabla() . " = " . $this->instancia->id_registro;
				
			$this->instancia->execute($sql);
			copy($this->ruta_temp . $this->nombre_archivo_qr, $this->instancia->directorio_thumbs . "/" . $this->nombre_archivo_qr) or die("Unable to copy $this->ruta_temp$this->nombre_archivo_qr to $this->instancia->directorio_thumbs.");
			//unlink($this->ruta_temp . $this->nombre_archivo_qr);
			//echo $sql;
		}
		
		public function deleteQr(){
			
		}
		
		
		function datos_imagen(){
			if (isset($_REQUEST[$data])) {
	    
				//it's very important!
				if (trim($_REQUEST[$data]) == '')
				    die('data cannot be empty! <a href="?">back</a>');		            
				// user data
				 nombre_imagen();
				
			} else {    
				    
				//default data
				//echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
				QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);    		        
			}
		}
		
		function guardar_imagen(){
	
		}
		
		function nombre_imagen(){
			$filename = $PNG_TEMP_DIR.'test'.md5($_REQUEST[$data].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
			    QRcode::png($_REQUEST[$data], $filename, $errorCorrectionLevel, $matrixPointSize, 2);  
		}
		
		function size(){
			$matrixPointSize = 4;
			    if (isset($_REQUEST['size']))
				$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
		}
		
		function ecc(){
			$errorCorrectionLevel = 'L';
			    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
				$errorCorrectionLevel = $_REQUEST['level'];  
		}
		
	}


?>