<?php
		
	require_once("../../framework/CoreConstantes.class.php");	
	require_once("../../framework/Mail/Mail.class.php");	

 class Template{

 	private $recurso = "";
 	
 	public function getRecurso(){
 		return $this->recurso;
 	}
 	
 	public function __construct($recurso){
 		$this->recurso = $recurso;
 	}
	
	public function getHtmlTag( $nombre_tag ){		
		$posicion_inicio 	= strpos($this->recurso,$nombre_tag) + strlen($nombre_tag) + 1;
		$posicion_final		= strpos($this->recurso,"/".$nombre_tag) - 1;		
		return substr($this->recurso,$posicion_inicio,$posicion_final-$posicion_inicio);		
	}
		
	public function replaceTag($nombre_tag,$busqueda,$registros){
		$contenido_tag 	= "";
		$tag 			= $this->getHtmlTag($nombre_tag);		
		foreach ($registros as $registro){
			$contenido_tag .= str_replace($busqueda,$registro,$tag);
		}
		return $contenido_tag;
	}
	
	public function replaceInFuente($contenido,$tag){
		$this->recurso = substr_replace(	$this->recurso,$contenido,
											strpos($this->recurso,$tag) -1,
											-(strlen($this->recurso) - (strpos($this->recurso,"/".$tag) + strlen($tag)+ 2)));
	}
	
	public function sendMails(){
		$mail 		= new Mail();
		$registros 	= $mail->getMails();		
		foreach ($registros as $registro) {
			mail(	$registro['mail'],
					"Boletin ".CoreConstantes::$sistema,
					$this->recurso,
					$mail->headers);
		}
		
	}
	
}


?>