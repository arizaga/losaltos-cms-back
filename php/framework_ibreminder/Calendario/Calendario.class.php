
<?php

	require_once( "../../framework/Registro/Registro.class.php");
	require_once( "../../framework/Replace/Replace.class.php");
	require_once( "../../framework/Calendario/CalendarioConstantes.class.php");
	require_once( "../../framework/Fecha/Fecha.class.php");
	require_once( "../../framework/Xml/Xml.class.php");


 	class Calendario{
 		
 		private $fecha 				= "";
 		
 		private $primer_dia 		= "";
 		
 		private $conexion			= null;
 		
 		private $dia_semana 		= "";
 		
 		private $valor_comparativa	= "";
	
 		function __construct($fecha,$valor_comparativa,$primer_dia="0"){
 			$this->fecha 		= $fecha;
 			$this->primer_dia	= $primer_dia;
 			$this->dia_semana	= $primer_dia == 1 ? date("N",strtotime($fecha)) -1 : date("w",strtotime($fecha));
 			$this->conexion 	= new Registro(0,""); 			
 			$this->valor_comparativa	= $valor_comparativa;
 		}
 		
 		public function getUltimoDia(){
 			return $this->conexion->getOne(
 						Replace::replaceDatos(	"@f",
 												$this->fecha,
						 						$this->getSQL(
						 							CalendarioConstantes::$_ultimo_dia_mes)));
 		}
 		
 		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				
				case CalendarioConstantes::$_ultimo_dia_mes:
					$sql = "SELECT LAST_DAY(@f)";
					break;
				
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		} 

		public function	fillMesAnterior(){
			$arreglo = array();
			for ($i=0; $i < $this->dia_semana; $i++ ){
				$arreglo_dia = array();
				$arreglo_dia['dia'] = "0";
				$arreglo[] = $arreglo_dia;
			}
			return $arreglo;
		}
		
 		function buildCalendario($sql = ""){
 			$i = 0;
 			$xml = new Xml();
 			$arreglo_final = $this->fillMesAnterior();
 			$total	 	   = 37 - $this->dia_semana	;		
 			for($i = 0; $i < $total ; $i++){
 				$arreglo_dia = array();
 				$dia 						= Fecha::addDateDay($this->fecha,$i);
 				if(Fecha::getDataFromDate($dia,"m") == Fecha::getDataFromDate($this->fecha,"m")){ 				
	 				$arreglo_dia['dia'] 		=  Fecha::getDataFromDate($dia); 				
	 				$arreglo_dia['disponible']	=  $this->conexion->getOne(Replace::replaceDatos( array("@f","@i"),
	 																							  array($dia,$this->valor_comparativa),
	 																							  $sql)) ? "1":"0";
 				}
 				else {
 					$arreglo_dia['dia']			= "0";
 				}
 				
 				$arreglo_final[] 			= $arreglo_dia;
 			}
 			$xml->getlistado($arreglo_final,"Calendario");
 			return $xml->getXML();
 		}
		
	
	}

?>