<?php
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/Request/Request.class.php");
	require_once("../../framework/Logger/Logger.class.php");
	require_once("../../framework/Redirection/Redirection.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/ExcepcionSistema/ExcepcionSistemaConstantes.class.php");
	
	class ExcepcionSistema extends Exception{
		
		private $severidad	= 0;
		
		private $debug 		= FALSE;
				
		private $xml		= NULL;
		
		public function ExcepcionSistema(	$mensaje = NULL , 
											$codigo = 0, 
											$severidad = 0){												
			$this->severidad = $severidad;
			parent::__construct($mensaje, $codigo);			
		}
		
		
		
		public function muestraError(){	
			$logger = new Logger();
			$this->xml = new Xml();
			switch ($this->severidad){
				case LoggerConstantes::$LOG_NIVEL_CRITICO:
					Request::setParametro("tipo_error",ExcepcionSistemaConstantes::$critical);
					$logger->log($this);
					$this->redirectionArchivo();					
					break;
					
				case LoggerConstantes::$LOG_NIVEL_LOGICA:
					Request::setParametro("tipo_error",ExcepcionSistemaConstantes::$logical);
					$logger->log($this);					
					$this->redirectionArchivo();										
					break;
					
				case LoggerConstantes::$LOG_NIVEL_WARNING:
					Request::setParametro("tipo_error",ExcepcionSistemaConstantes::$warning);
					$this->redirectionArchivo();
					break;	
					
				case LoggerConstantes::$LOG_NIVEL_ALMACENAMIENTO:
					Request::setParametro("tipo_error",ExcepcionSistemaConstantes::$almacenamiento);
					$logger->log($this);					
					break;
			}
			exit(1);
		}
		
		public function redirectionArchivo(){
			if(!CoreConstantes::$json){
				$url = CoreConstantes::$ruta_pagina . CoreConstantes::$pagina_error;
				$redirecciona = new Redirection($url);
				Request::setParametro("error", $this->getMessage());			
				$redirecciona->addParametros();
				$redirecciona->executeRedireccion();
			}
			else
				echo $this->xml->getListadoJsonOrdinario( array(array("Error"=>$this->getMessage())) );
		}
		
		
		
		
		
		
	}




?>