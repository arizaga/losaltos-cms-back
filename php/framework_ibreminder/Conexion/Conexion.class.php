<?php	
	
	require_once("../../adodb/adodb.inc.php");	
	require_once("../../framework/ExcepcionSistema/ExcepcionSistema.class.php");
	require_once("../../framework/ErrorConstantes.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	
	class Conexion{
		
		private 	$ruta_configuracion = "";
				
		private 	$conexion			= null;
		
		private   	$datos_bd			= null;
		
		public  	$mensaje			= "";
		
		static		$instancia			= null; 		
		
		public function getDatosIni(){
			return $this->datos_bd;
		}
		
		private function __clone(){ }
		
		public function getConexion(){
			return $this->conexion;
			
		}
		
		public static function getInstance(){
			if (!(self::$instancia instanceof self)){
				self::$instancia = new self();
			}
			return self::$instancia;
		}
		
		private function __construct($change_ini = false){
			if(!$change_ini){
				$this->ruta_configuracion = CoreConstantes::$to_root  . "configuracion/conexion.ini";
				$this->getDatosBd();
				$this->connectBd();			
			}
		}
				
		public function setConfiracionIni($ruta_ini){
			$this->ruta_configuracion = CoreConstantes::$to_root  . $ruta_ini;
		}
		
		public function makeConnection(){
			$this->getDatosBd();
			$this->connectBd();
		}
		
		
		final private  function connectBd(){
			try {
				//echo 
				if(!$this->conexion = ADONewConnection($this->datos_bd['tipo']."://".$this->datos_bd['usuario'].":".$this->datos_bd['password']."@".$this->datos_bd['host']."/".$this->datos_bd['base_datos']."?clientflags=65536")){					
					throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_06,
												ErrorConstantes::$int_error_xml_06,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
				if(!$this->conexion->Connect(	$this->datos_bd['host'],	
												$this->datos_bd['usuario'],
								  				$this->datos_bd['password'],
								  				$this->datos_bd['base_datos'])){
					$this->mensaje = $this->conexion->_errorMsg;
					throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_05,
												ErrorConstantes::$int_error_xml_05,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
				
			}catch (ExcepcionSistema  $e){
				$e->muestraError();
			}
		}
		
		final private function getDatosBd(){
			try {
				if(!$this->datos_bd = @parse_ini_file($this->ruta_configuracion,TRUE)){
					throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_09,
												ErrorConstantes::$int_error_xml_09,
												LoggerConstantes::$LOG_NIVEL_CRITICO);	
				}
				
			}catch (ExcepcionSistema  $e){
				$e->muestraError();
			}			
		}
	
		
	}
?>