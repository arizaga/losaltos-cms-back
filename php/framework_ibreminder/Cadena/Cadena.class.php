<?php


 class Cadena{
	
	
			
	public static function getCadenaValuesCascada( $registros, $id_cascada){
		
		$arreglo_valores 	= null;
		$registro			= null;
		
		foreach ( $registros as $registro ){
			
			if(is_array($registro)){
				//agregando el id del registro cascada al inicio
				array_unshift($registro,$id_cascada);
				
				foreach ($registro as $campo){
					$arreglo_valores[] = "'".$campo."'";	
				}
				$cadena_values[] = "(".implode(",",$arreglo_valores).")";
				$arreglo_valores = null;					
			}
			else {
				$cadena_values[] = "("."'$id_cascada'".","."'$registro'".")";					
			}
		}
		return implode(",",$cadena_values);
	}
	
	public static function getCadenaAnteriorPosteriorA($cadena,$busqueda,$anterior = TRUE){
		return $anterior ? 
					substr($cadena,0,strpos($cadena,$busqueda)):
					substr($cadena,strlen($cadena) - strpos($cadena,$busqueda) - strlen($busqueda));
	}
	
	
}


?>