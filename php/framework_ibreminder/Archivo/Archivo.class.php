<?php


	class Archivo{
		
		private $nombre;
		
		private $ruta;
				
		private $mensaje_xml;			
		
		private $apuntador_archivo;		
		
		private $carpeta_archivo;
		
		
		function __construct( $ruta = ""){
			$this->nombre 	= basename($ruta);
			$this->carpeta_archivo = dirname($ruta);
			$this->ruta		= $ruta;			
		}
		
		function existArchivo(){
			return is_file( $this->ruta );
		}
		
		function makeArchivo($contenido = ""){
			$OK = file_put_contents($this->ruta,$contenido);
			if($OK){
				$this->mensaje_xml .= "Archivo: {$this->nombre} creado con exito \n";
			}
			else{
				$this->mensaje_xml .= "El archivo: {$this->nombre} no se pudo crear \n";
			}
			return $OK;
		}
		
		function getLinea($longitud = ""){
			if($longitud){
				$linea = fgets($this->apuntador_archivo, $longitud); 
			}
			else {
				$linea = fgets($this->apuntador_archivo);
			}
			return $linea;
		}
		
		function putToArchivo($contenido){
			return file_put_contents($this->ruta,$contenido,FILE_APPEND);
		}
		
		function copyArchivo($ruta_origen = "",$ruta_destino){
			if(!$ruta_origen){
				$ruta_origen = $this->ruta;
			}
			return copy($ruta_origen,$ruta_destino);
		}
		
		function deleteArchivo($ruta_archivo = ""){
			$OK = FALSE;
			
			if(!$ruta_archivo){
				$ruta_archivo = $this->ruta;
			}
						
			if($this->existArchivo()){
			
				if(@unlink($ruta_archivo)){
					$this->mensaje_xml .= "Archivo: $ruta_archivo eliminado \n";
					$OK = TRUE;
				}
				else {
					$this->mensaje_xml .= "NO se pudo eliminar el archivo: $ruta_archivo \n";
				}
			}
			else {
				$this->mensaje_xml .= "NO se pudo eliminar el archivo por que no existe: $ruta_archivo \n";
				$OK = TRUE;
			}
			return $OK;
		}
		
		function openArchivo($modo = "r+"){
			return $this->apuntador_archivo = @fopen($this->ruta,$modo);
		}
		
		
		function getCsv(){
			return fgetcsv($this->apuntador_archivo);
		}
		
		function readArchivo(){			
			return file_get_contents($this->ruta);
		}
		
		function registroToCSV ($registro = NULL){
			
			$cadena ="";
			$cadena .= implode(',',$registro);
			
			return $cadena;
		}
		
		function renameArchivo($nombre_archivo){
			return rename($this->ruta,$this->carpeta_archivo . "/$nombre_archivo");
		}

		

		function writeArchivo($contenido){
			return fwrite($this->apuntador_archivo,$contenido);
		}
		
		function closeArchivo(){
			return fclose($this->apuntador_archivo);
		}	
		
		
	}


?>