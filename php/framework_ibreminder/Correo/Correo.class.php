<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../framework/Correo/CorreoConstantes.class.php");
	require_once("../../framework/CoreConstantes.class.php");

	class Correo extends Registro{
		
		var $_listado_correo = "listado_correo";
		
		public $directorio_imagenes		= "";
		public $directorio_thumbs			= "";
		public $ancho_thumb				= 0;
		public $alto_thumb					= 0;
		//{VAR_LISTADO}
		
		function __construct(&$id_correo = 0){
			$this->directorio_imagenes  = CoreConstantes::$ruta_imagenes ."correo/original/";
			$this->directorio_thumbs	= CoreConstantes::$ruta_imagenes ."correo/thumb/";
			$this->ancho_thumb			= 60;
			$this->alto_thumb			= 60;
			parent::Registro( $id_correo );
			
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				
				case CorreoConstantes::$_listado_correo:
					$sql = "SELECT *
					          FROM correo
					         WHERE estatus = '1'";
					break;
					
				case CorreoConstantes::$_busca_correo:
					$sql = "SELECT *
					          FROM correo
					         WHERE correo = '@@'";
					break;
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getNombreTabla(){
			return "correo";
		}
		
		function getCorreos(){
			return $this->getAll(
						$this->getSQL(
							CorreoConstantes::$_listado_correo));
		}
		
		public function searchCorreo($correo = ""){			
			$registro = $this->getRegistro(
							Replace::replaceDatos(	"@@",
													$correo,
													$this->getSQL(CorreoConstantes::$_busca_correo)));
			return $registro? true : false;
		}
		
	}

?>
