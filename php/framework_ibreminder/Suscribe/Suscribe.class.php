<?php
	require_once("../../framework/Mail/Mail.class.php");
	require_once("../../framework/Request/Request.class.php");
	require_once("../../framework/Archivo/Archivo.class.php");
	require_once("../../framework/Template/Template.class.php");
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/Replace/Replace.class.php");
	require_once("../../framework/Correo/Correo.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/MensajesConstantes.class.php");

	class Suscribe{
		
		private $correo 			= "";
		
		public $objeto_correo		= null;
			
		private $ruta_estrucura_suscribe		= "";
		
		private $ruta_estrucura_desuscribe		= "";
		
		public $ruta_estrucura_confirmacion	= "";
		
		private $ruta_estrucura_confirma_info	= "";
		
		public	$ruta_estrucura_recuerda_contrasena	= "";
		
		private $xml				= null;
		
		public function __construct($correo = ""){
			$this->correo 			= $correo;
			$this->ruta_estrucura_suscribe	    = "../../framework/Suscribe/estructura/estructura_suscribe.php";			
			$this->ruta_estrucura_desuscribe	= "../../framework/Suscribe/estructura/estructura_desuscribe.php";
			$this->ruta_estrucura_confirmacion	= "../../framework/Suscribe/estructura/estructura_confirmacion.php";
			$this->ruta_estrucura_confirma_info = "../../framework/Suscribe/estructura/estructura_confirma_info.php";
			$this->ruta_estrucura_recuerda_contrasena = "../../framework/Suscribe/estructura/estructura_recuerda_contrasena.php";
			$this->objeto_correo	= new Correo();
			$this->xml				= new Xml();
		}
		
		
		public function makeSuscripcion(){
			$resultado 	= "";
			$mensaje 	= "";
			switch (Request::getParametro("suscripcion")) {
				
				case "suscribe":
					$resultado = $this->suscribePersona();
					break;

				case "enable":
					$mensaje = $this->enableDisableCuenta("enable");
					break;
					
				case "disable":
					$mensaje 	= $this->enableDisableCuenta("disable");
					break;
					

				default:
					$resultado = $this->xml->addMensaje("No se selecciono ninguna opción",true);					
					break;
			}
			
			if(Request::getParametro("suscripcion") == "enable" || Request::getParametro("suscripcion") == "disable"){
					$busqueda 		= array(	"{RUTA_IMAGENES}",
												"{NOMBRE_PAGINA}",
												"{MENSAJE}");								
					$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
												CoreConstantes::$sistema,
												$mensaje);
					$archivo		= new Archivo($this->ruta_estrucura_confirmacion);			
					$template		= new Template(Replace::replaceDatos($busqueda,$reemplazo,$archivo->readArchivo()));
					$resultado 		= $template->getRecurso();
				
			}
			
			return $resultado;
		}
		
		
		public function confirmInfo($datos_extra,$ruta_archivo = "",$reply_to= ""){
			
			$campos  = array_keys($datos_extra);
			$valores = array_values($datos_extra);
			$archivo_html = $ruta_archivo ? $ruta_archivo : $this->ruta_estrucura_confirma_info;
			
			
			$busqueda 		= array(	"{RUTA_IMAGENES}",
										"{NOMBRE_PAGINA}",
										"{ROOT}");								
			$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
										CoreConstantes::$sistema,
										CoreConstantes::$root_pagina);
										
			$busqueda_final 	= array_merge($busqueda,$campos);
			$reemplazo_final	= array_merge($reemplazo,$valores);						
			
			$archivo		= new Archivo($archivo_html);			
			$template		= new Template(Replace::replaceDatos($busqueda_final,$reemplazo_final,$archivo->readArchivo()));			
			$mail 			= new Mail(	$this->correo,
										"Confirmación de registro ". CoreConstantes::$sistema,
										$template->getRecurso(),
										CoreConstantes::$correo_boletin,
										$reply_to);										
			//echo $template->getRecurso();
			$mail->sendMail();
			
			
			$this->xml->addMensaje(MensajesConstantes::$msg_02,true);
			
			return $this->xml->getXML();
		}
		
		
		public function suscribePersona(){
			
			$OK = $this->objeto_correo->searchCorreo($this->correo);
			//No existe tel correo dentro de la BD
			if(!$OK ){
				$OK 		= $this->objeto_correo->insertRegistro("",array("correo"=>$this->correo));
				
				if($OK){
					
					$this->objeto_correo->id_registro = md5($this->objeto_correo->getUltimoId() . $this->correo);				
					$busqueda 		= array(	"{RUTA_IMAGENES}",
												"{NOMBRE_PAGINA}",
												"{CLAVE}",
												"{RUTA_SUSCRIBE}");								
					$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
												CoreConstantes::$sistema,
												$this->objeto_correo->id_registro,
												CoreConstantes::$ruta_pagina . CoreConstantes::$ruta_suscribe);
					$archivo		= new Archivo($this->ruta_estrucura_suscribe);
					$template		= new Template(Replace::replaceDatos($busqueda,$reemplazo,$archivo->readArchivo()));
					
					//echo $template->getRecurso();
					$mail 			= new Mail(	$this->correo,
												"Suscripción ". CoreConstantes::$sistema,
												$template->getRecurso(),
												CoreConstantes::$correo_boletin);
					$mail->sendMail();
					
					
					$this->xml->addMensaje(MensajesConstantes::$msg_02,true);					
					
				}
				else {
					$this->xml->addMensaje(MensajesConstantes::$msg_06, 
											true);
				}
			}//Existe el correo dentro de la BD
			else {
					$this->objeto_correo->id_registro = $this->correo;
					$busqueda 		= array(	"{RUTA_IMAGENES}",
												"{NOMBRE_PAGINA}",
												"{CLAVE}");								
					$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
												CoreConstantes::$sistema,
												$this->objeto_correo->id_registro);
					$archivo		= new Archivo($this->ruta_estrucura_desuscribe);			
					$template		= new Template(Replace::replaceDatos($busqueda,$reemplazo,$archivo->readArchivo()));
					$mail 			= new Mail(	$this->correo,
												"Romover suscripción ". CoreConstantes::$sistema,
												$template->getRecurso(),
												CoreConstantes::$sistema . " <".CoreConstantes::$correo_boletin.">");
												
					//echo $template->getRecurso();
					$mail->sendMail();
					
					
					$this->xml->addMensaje(MensajesConstantes::$msg_10,true);
					
					
			}
			return $this->xml->getXML();
		}
		
		public function enableDisableCuenta($opcion){
			
			$mensaje	= "";
			$OK 		= false;	
			
			if($opcion == "enable"){
				$estatus = "1";
				$this->objeto_correo->id_registro	= Request::getParametro("clave");
				$OK = 	$this->objeto_correo->setEstatus("MD5(CONCAT(id_correo,correo))",$estatus);
			}
			else {				
				$this->objeto_correo->id_registro	= Request::getParametro("clave");
				$OK = $this->objeto_correo->deleteRegistro("correo");
			}
			
			if($OK){				
				$mensaje  = "Tu cuenta " ;
				$mensaje .= $opcion == "enable" ? "ha sido activada": "ha sido desactivada";				
			}
			else {
				$mensaje = MensajesConstantes::$msg_08;
			}
			return $mensaje;
		}
	}
?>