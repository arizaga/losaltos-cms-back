<?php
	
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	
	

		function handleErrores($errno, $errstr, $errfile, $errline){
			$cadena_error	= "";
			$xml 			= new Xml();	
			$alerta			= false;
			$respuesta		= "";
		
			switch ($errno) {
				case E_USER_ERROR:
					$cadena_error = "Error: $errstr, provocado en el archivo $errfile" .
									" en la linea $errline";
					if(!CoreConstantes::$json){
						echo $xml->addMensajeError($cadena_error,$alerta);
					}
					else
						echo $xml->getListadoJsonOrdinario( array(array( 'Error' => "$errstr, provocado en el archivo $errfile" . " en la linea $errline" )) );
					break;

				case E_USER_WARNING:
					$cadena_error = "Advertencia: $errstr, provocado en el archivo $errfile" .
									" en la linea $errline";
					if(!CoreConstantes::$json){
						echo $xml->addMensajeError($cadena_error,$alerta);
					}
					else
						echo $xml->getListadoJsonOrdinario( array(array( 'Error' => "$errstr, provocado en el archivo $errfile" . " en la linea $errline" )) );
					break;

				case E_USER_NOTICE:
					$cadena_error = "Notificacion: $errstr, provocado en el archivo $errfile" .
									" en la linea $errline";
					if(!CoreConstantes::$json){
						echo $xml->addMensajeError($cadena_error,$alerta);
					}
					else
						echo $xml->getListadoJsonOrdinario( array(array( 'Error' => "$errstr, provocado en el archivo $errfile" . " en la linea $errline" )) );
					break;

				case E_ERROR:
					$cadena_error = "Error Critico: $errstr, provocado en el archivo $errfile" .
									" en la linea $errline";
					if(!CoreConstantes::$json){
						echo $xml->addMensajeError($cadena_error,$alerta);
					}
					else
						echo $xml->getListadoJsonOrdinario( array(array( 'Error' => "$errstr, provocado en el archivo $errfile" . " en la linea $errline" )) );
					break;
	
				case E_STRICT:
					break;				
					
				default:
					$cadena_error = "Error Desconocido: $errno $errstr, provocado en el archivo $errfile" .
									" en la linea $errline";
					if(!CoreConstantes::$json){
						echo $xml->addMensajeError($cadena_error,$alerta);
					}
					else
						echo $xml->getListadoJsonOrdinario( array(array( 'Error' => "$errstr, provocado en el archivo $errfile" . " en la linea $errline" )) );
					break;
			}
			
			
			return true;
		}


		function manipulateRequire($clase){
			$posicion = strpos($clase,"Constantes");
			if($posicion){
				$clase = substr($clase,0,$posicion);
				require_once	"../../clase/".strtolower($clase)."/{$clase}Constantes.class.php";
			}
			else {
				require_once	"../../clase/".strtolower($clase)."/$clase.class.php";
			}
		}	

		set_error_handler("handleErrores");
	
?>