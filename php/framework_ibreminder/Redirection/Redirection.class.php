<?php




	class Redirection{
	
		private $recurso = null;
	
		public function __construct($direccion){
			$this->recurso = curl_init($direccion);
			$this->setOptionsPOST();
		}
		
		public function setOptionsPOST(){			
			curl_setopt($this->recurso, CURLOPT_POST, 			true);
			curl_setopt($this->recurso, CURLOPT_POSTFIELDS, 	$_REQUEST);
			curl_setopt($this->recurso, CURLOPT_RETURNTRANSFER, true);
			
		}
		
		public function addParametros(){
			curl_setopt($this->recurso, CURLOPT_POSTFIELDS, 	$_REQUEST);		
		}
		
		
		public function executeRedireccion(){
			$exito	= 0;
			$resultado = curl_exec($this->recurso);
			curl_close($this->recurso);
			if($resultado){
				echo $resultado;
				$exito = 1;
			}
			return $exito;
		}
		
	
	}