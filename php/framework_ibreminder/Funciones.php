<?php

            ini_set("memory_limit", "10000M");
	    ini_set('post_max_size','500M');
	    ini_set('max_execution_time','180');

	    function invertir($string){
			$newstring = "";
	    	    for($i=strlen($string);$i>0;$i--)
	    	        $newstring .= substr($string,$i-1,1);
	    	    return $newstring;
	    }

	    function excel_decimal($numero_excel){
	        $long = strlen($numero_excel);
	        $num_decimal = 0;
	        $numero_excel = invertir($numero_excel);
	        for($i=0;$i<$long;$i++)
	            $num_decimal = $num_decimal + (col_numero($numero_excel[$i])*pow(26, $i));
	        return $num_decimal;
	    }

	    function decimal_excel($n){
	        $n--;
			for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
				    $r = chr($n%26 + 0x41) . $r;
			return $r;
	    }

	    function col_numero($columna){
	    	switch($columna){
	        		case "A": $numero=1; break;
	        		case "B": $numero=2; break;
   	    		        case "C": $numero=3; break;
	    		        case "D": $numero=4; break;
	    		        case "E": $numero=5; break;
	    		        case "F": $numero=6; break;
	    		        case "G": $numero=7; break;
	    		        case "H": $numero=8; break;
	    		        case "I": $numero=9; break;
	    		        case "J": $numero=10; break;
	    		        case "K": $numero=11; break;
	    		        case "L": $numero=12; break;
	    		        case "M": $numero=13; break;
	    		        case "N": $numero=14; break;
	    		        case "O": $numero=15; break;
	    		        case "P": $numero=16; break;
	    		        case "Q": $numero=17; break;
	    		        case "R": $numero=18; break;
	    		        case "S": $numero=19; break;
	    		        case "T": $numero=20; break;
	    		        case "U": $numero=21; break;
	    		        case "V": $numero=22; break;
	    		        case "W": $numero=23; break;
	    		        case "X": $numero=24; break;
	    		        case "Y": $numero=25; break;
	    		        case "Z": $numero=26; break;
	    }
			return $numero;
}


?>