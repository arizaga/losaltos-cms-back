<?php
	require_once("../../framework/CoreConstantes.class.php");
	
	/**
	* Descripción: Clase Genérica para realizar el timbrado y cancelación de un CFDI con Facturación Moderna.
	* 
	* Facturación Moderna :  (http://www.facturacionmoderna.com)
	* @author Edgar Durán <edgar.duran@facturacionmoderna.com>
	* @package FacturacionModerna
	* @version 1.2
	* @see http://developers.facturacionmoderna.com
	**/
	
	class Facturacion {
		
		
		public $parametros       = array();/*Info para conectar con el webservice de Facturacion Moderna*/
		public $parametros_extra = array();
		
		public $xml;
		public $pdf;
		public $png;
		public $UUID; /* Nombre del archivo que nos devuelve el webservice, (uuid o folio fiscal) */
		public $numero_certificado;
		
		public $archivo_cer;
		public $archivo_key;
		public $archivo_pem;
		
		
		public $ultimoError;
		public $ultimoCodigoError; 
		
		public $log = 'FacturacionModerna-log.txt';
		public $debug = 1;
		public $url = ''; 
		
		public $cfdi;
		
		public $arreglo_datos     = array();
		public $arreglo_conceptos = array();
		
		public $sello;
		public $cadena_original;
	  
		/**
		* Crea el objeto de conexión con la API de Facturación Moderna, para
		* acceder a los métodos de timbrado y cancelación de CFDI.
		* 
		* @param string $url
		* @param array $opciones
		* @param boolean $debug
		* @return boolean
		*/  
		public function __construct($arreglo_datos, $arreglo_conceptos) {
			$this->arreglo_datos      = $arreglo_datos;
			$this->arreglo_conceptos  = $arreglo_conceptos;
			
			$this->numero_certificado = $arreglo_datos[0]['number_certificate'];
			$this->archivo_cer        = CoreConstantes::$ruta_certificados . $arreglo_datos[0]['certificate'];
			$this->archivo_key        = CoreConstantes::$ruta_certificados . $arreglo_datos[0]['key_file'];
			$this->archivo_pem        = CoreConstantes::$ruta_certificados . $arreglo_datos[0]['pem'];
			$this->pass_key           = $arreglo_datos[0]['pass_key'];
			
			$this->url = CoreConstantes::$url_timbrado;
			
			if(array_key_exists('rfc_emisor',$this->arreglo_datos[0]))
				$index = 'rfc_emisor';
			else
				$index = 'rfc';
			
			//var_dump($this->arreglo_datos[0][$index]);
			$this->parametros = array('emisorRFC' 	=> $this->arreglo_datos[0][$index],
							'UserID' 	=> CoreConstantes::$user_id,
							'UserPass' 	=> CoreConstantes::$user_password);
			
			$this->parametros_extra = array(
								'generarCBB'=>true,
								'generarPDF'=>false,
								'generarTXT'=>false
							);
			
			foreach ($this->parametros as $k => $v) {
				if(isset($v) && in_array($k, array("emisorRFC", 'UserID', 'UserPass'))){
					$this->parametros[$k] = $v;
				}
			}
			
			//var_dump($this->cfdi);
			//exit;
		}
	  
		/**
		* Ejecuta el método SOAP requestTimbrarCFDI() del Servicio Web de
		* Facturacion Moderna, públicado en FacturacionModerna::url
		* http://developers.facturacionmoderna.com/#requestTimbrarCFDI
		*
		* Recibe como parámetro principal $str que contiene la ruta o contenido del
		* archivo a certificar, el archivo debe ser algunos de los soportados por
		* la API. http://developers.facturacionmoderna.com/#layout
		*
		* En caso de una petición exitosa, el método establece los [Valores] a las
		* propiedades de la clase FacturacionModerna
		*
		* En caso de error establece las propiedades ultimoError y
		* ultimoCodigoError, con el mensaje y código de error correspondientes.
		* El listado de mensajes de error se encuentra en: http://developers.facturacionmoderna.com/#errores
		*
		*
		* [Valores]
		*
		* FacturacionModerna::UUID, contiene el UUID del último comprobante certificado.
		*
		* FacturacionModerna::xml, contiene el comprobante certificado en formato XML.
		*
		* FacturacionModerna::txt, contiene en formato de texto simple el contenido del nodo TimbreFiscalDigital del CFDI.
		* El valor se establece siempre y cuando $opciones['generarTXT'] = true
		*
		* FacturacionModerna::pdf, contiene la representación impresa del CFDI en formato PDF.
		* se debe utilizar cuándo se requiera que FacturacionModerna generé un formato PDF genérico para un CFDI,
		* para ser generado $opciones['generarPDF'] = true
		*
		* FacturacionModerna::png, contiene el Código de Barras Bidimensional o CBB
		* (QR-Code) el cuál debe estar presente en la representación impresa del
		* CFDI.
		*
		* Nota: EL CBB se genera siempre y cuando $opciones['generarCBB'] sea igual a
		* true, utilizar está opción dehabilita 'generarPDF'
		* 
		*
		* @param string $str Contenido o Rutá del del comprobante a certificar.
		* @return void
		*/
		
		public function sellar(){
			$this->cfdi = $this->generarXML(CoreConstantes::$rfc_emisor);
			$this->cfdi = $this->sellarXML( $this->cfdi, $this->numero_certificado, $this->archivo_cer, $this->archivo_pem );
			//var_dump($this->cadena_original);
		}
		
		public function timbrar(){ /*NOTA: $str es el CFDI*/
	    
			try {
		    
				//Si $str es la ruta a un archivo leerlo.
				if(file_exists($this->cfdi)){
					$this->cfdi = file_get_contents($this->cfdi);
				}
				//Códificar el comprobante a certificar en Base64, la nota con la que hacen mucho hincapié
				$this->parametros['text2CFDI'] = base64_encode($this->cfdi);                     
				$this->parametros = array_merge($this->parametros, $this->parametros_extra);
			  
				//var_dump((object) $this->parametros);
				$cliente = new SoapClient( $this->url, array('trace' => 1));/*Aquí ya está definida la url del webservice*/
				$respuesta = $cliente->requestTimbrarCFDI((object) $this->parametros);
				
				//Establecer las propiedades con el objeto de respuesta SOAP.
				foreach(array('xml', 'pdf', 'png', 'txt') as $propiedad){
					if(isset($respuesta->$propiedad)){
						$this->$propiedad = base64_decode($respuesta->$propiedad);
					}  
				}
				
				if(isset($respuesta->xml)){
					$xml_cfdi = simplexml_load_string($this->xml);
					$xml_cfdi->registerXPathNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");
					$tfd = $xml_cfdi->xpath('//tfd:TimbreFiscalDigital');        
					$this->UUID = (string) $tfd[0]['UUID'];
				}
				      
				if($this->debug == 1){
					$this->log("SOAP request:\t".$cliente->__getLastRequest());
					$this->log("SOAP response:\t".$cliente->__getLastResponse());
				}
				
				return true; 
			  
			}catch (SoapFault $e){
				
				/*if($this->debug == 1){
				  $this->log("SOAP request:\t".$cliente->__getLastRequest());
				  $this->log("SOAP response:\t".$cliente->__getLastResponse());
				}*/
				$this->ultimoError = $e->faultstring;
				$this->ultimoCodigoError = $e->faultcode;
				
				
					//$this->ultimoError = $e->getMessage();
					//$this->ultimoCodigoError = "Unknown";
				
			return false;
			}
		}
	
		/**
		* Ejecuta el método SOAP requestCancelarCFDI() del Servicio Web de
		* Facturacion Moderna públicado en FacturacionModerna::url
		* http://developers.facturacionmoderna.com/#requestCancelarCFDI
		* 
		* Recibe el UUID de un CFDI para reportar la cancelación del mismo ante los servicios del SAT. 
		*
		* @param string $uuid
		* @return boolean
		*
		*/
	  
		function cancelar(){
			try{
				$cliente = new SoapClient($this->url, array('trace' => 1));      
				$opciones['uuid'] = $this->arreglo_conceptos[0]['folio_fiscal'];
				$opciones = array_merge($opciones, $this->parametros);
				$respuesta = $cliente->requestCancelarCFDI((object) $opciones);
				return true;
			}catch (SoapFault $e){
				$this->ultimoError = $e->faultstring;
				$this->ultimoCodigoError = $e->faultcode;
			}
			return false; 
		}
		
		
		
		
		/**
		* Ejecuta el método SOAP activarCancelacion() del Servicio Web de
		* Facturacion Moderna públicado en FacturacionModerna::url
		* http://developers.facturacionmoderna.com/#activarCancelacion
		* 
		* Recibe los archivos Cer, Key y Contraseña para activar el servicio de cancelación de CFDIS por medio de FM hacia servicios del SAT. 
		*
		* @param string $archCer Contenido o Rutá del archivo Cer del CSD a activar.
		* @param string $archKey Contenido o Rutá del archivo Key del CSD a activar.
		* @param string $passKey Contraseña del archivo Key del CSD a activar.
		* @return boolean 
		*/
		public function activarCancelacion(){
			/*$registros = $registros[0];
			
			$this->numero_certificado = $registros['number_certificate'];
			$this->archivo_cer        = CoreConstantes::$ruta_certificados . $registros['certificate'];
			$this->archivo_key        = CoreConstantes::$ruta_certificados . $registros['key'];
			$this->archivo_pem        = CoreConstantes::$ruta_certificados . $registros['pem'];
			$this->pass_key           = $registros['pass_key'];*/
			
			try {
				//Si $archCer y/o $archKey son rutas de archivos, cargarlos
				if(file_exists( $this->archivo_cer )){
					$archCer = file_get_contents( $this->archivo_cer );
				}
				else{
					if(CoreConstantes::$json === false)
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_02 . ' (Archivo .cer) ' ,
												ErrorConstantes::$int_error_xml_02,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
					else
						echo $xml->getListadoJsonOrdinario( array(array("Error"=>ErrorConstantes::$str_error_xml_02 . ' (Archivo .cer) ')) );
				}
				
				if(file_exists( $this->archivo_key )){
					$archKey = file_get_contents( $this->archivo_key );
				}
				else{
					if(CoreConstantes::$json === false)
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_02 . ' (Archivo .key) ' ,
												ErrorConstantes::$int_error_xml_02,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
					else
						echo $xml->getListadoJsonOrdinario( array(array("Error"=>ErrorConstantes::$str_error_xml_02 . ' (Archivo .key) ')) );
				}
				
				$opciones=array();
				//Códificar los archivos a utilizar en Base64      
				$opciones['archivoKey'] = base64_encode($archKey);
				$opciones['archivoCer'] = base64_encode($archCer);
				$opciones['clave'] = $this->pass_key;
				$opciones = array_merge($opciones, $this->parametros);
			  
				$cliente = new SoapClient($this->url, array('trace' => 1));          
				$respuesta = $cliente->activarCancelacion((object) $opciones);      
				return true;
			}catch (SoapFault $e){      
				$this->ultimoError = $e->getMessage();
				$this->ultimoCodigoError = "Unknown";
			}
			return false; 
		}  
		
		
		
		
		
	      
		/**
		* Registra los mensajes SOAP en el archivo FacturacionModerna::log, si el
		* archivo no existe lo crea.
		*
		* Sólo se ejecuta si FacturacionModerna::debug tiene los valores 1 ó 2
		* @param $str
		* @return void
		*/
		function log($str){
			$f = fopen($this->log, 'a');
			fwrite($f, date('c')."\t".$str."\n\n");
			fclose($f);
		}
		
		
		
		
		function sellarXML($cfdi, $numero_certificado, $archivo_cer, $archivo_pem){
	  
			$private = openssl_pkey_get_private(file_get_contents($archivo_pem));
			$certificado = str_replace(array('\n', '\r'), '', base64_encode(file_get_contents($archivo_cer)));
			
			if($private == false){
				$xml = new Xml();
				echo $xml->getListadoJsonOrdinario( array( array('Error'=>"No se encuentra el archivo pem") ) );
				exit;
			}
			
			if($certificado == false){
				$xml = new Xml();
				echo $xml->getListadoJsonOrdinario( array( array('Error'=>"No se encuentra el certificado") ) );
				exit;
			}
			
			$xdoc = new DomDocument();
			$xdoc->loadXML($cfdi) or die("XML invalido");
		      
			$XSL = new DOMDocument();
			$XSL->load( CoreConstantes::$cadena_original );
			
			$proc = new XSLTProcessor;
			$proc->importStyleSheet($XSL);
			
			$cadena_original = $proc->transformToXML($xdoc);
			$this->cadena_original = $cadena_original;
			openssl_sign($cadena_original, $sig, $private);
			$sello = base64_encode($sig);
			$this->sello = $sello;
		      
			$c = $xdoc->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante')->item(0); 
			$c->setAttribute('sello', $sello);
			$c->setAttribute('certificado', $certificado);
			$c->setAttribute('noCertificado', $numero_certificado);
			return $xdoc->saveXML();
		      
		}
		function generarXML($rfc_emisor){

			$fecha_actual      = substr( date('c'), 0, 19);
			$arreglo_datos     = $this->arreglo_datos;
			$arreglo_conceptos = $this->arreglo_conceptos;
			$tipo_factura 	   = $this->arreglo_conceptos[0]['id_invoice_type'];
			$cad_aux 	   = '';
			
			$retencion_iva     = $this->floor_dec( ( ($arreglo_datos[0]['subtotal'] / 100) * ( $arreglo_datos[0]['value'] / 100 ) ) * (2/3) );
			$retencion_isr     = $this->floor_dec( ( ($arreglo_datos[0]['subtotal'] / 100) * ( 10 ) ) );
			$total_retencion   = $retencion_isr + $retencion_iva;
			
			$cfdi = '<?xml version="1.0" encoding="UTF-8"?>
		      <cfdi:Comprobante xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd" xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="3.2" fecha="'.str_replace(' ', 'T', $arreglo_datos[0]['date']).'" tipoDeComprobante="ingreso" noCertificado="" certificado="" sello="" formaDePago="Pago en una sola exhibición" metodoDePago="'.utf8_encode($arreglo_datos[0]['payment_method']).'" NumCtaPago="No identificado" LugarExpedicion="'.utf8_encode($arreglo_datos[0]['address_emisor']).'" subTotal="'.utf8_encode($arreglo_datos[0]['subtotal']).'" total="'.utf8_encode( $arreglo_datos[0]['subtotal'] + ( ($arreglo_datos[0]['subtotal'] / 100) * $arreglo_datos[0]['value'] ) ).'">
		      <cfdi:Emisor nombre="'.utf8_encode($arreglo_datos[0]['enterprise_emisor']).'" rfc="'.utf8_encode($arreglo_datos[0]['rfc_emisor']).'">
			<cfdi:RegimenFiscal Regimen="'.utf8_encode($arreglo_datos[0]['nombre_regime']).'"/>
		      </cfdi:Emisor>
		      <cfdi:Receptor nombre="'.utf8_encode($arreglo_datos[0]['enterprise_receptor']).'" rfc="'.utf8_encode($arreglo_datos[0]['rfc_receptor']).'"></cfdi:Receptor>
		      <cfdi:Conceptos>';
			
			foreach($arreglo_conceptos AS $fila){
				$cad_aux .= '<cfdi:Concepto cantidad="'.utf8_encode($fila['quantity']).'" unidad="'.utf8_encode($fila['unit']).'" noIdentificacion="'.utf8_encode($fila['id_invoice_item']).'" descripcion="'.utf8_encode($fila['description']).'" valorUnitario="'.utf8_encode($fila['unit_price']).'" importe="'.utf8_encode($fila['amount']).'"></cfdi:Concepto>';
			}
			
		      $cad_aux .= '</cfdi:Conceptos>';
			
			if($tipo_factura == '5' || $tipo_factura == '6'){
				$cad_aux .= 	'<cfdi:Impuestos totalImpuestosRetenidos="'. $total_retencion .'" totalImpuestosTrasladados="'. ( ($arreglo_datos[0]['subtotal'] / 100) * $arreglo_datos[0]['value'] ) .'">
						<cfdi:Retenciones>
							<cfdi:Retencion impuesto="IVA" importe="'. $retencion_iva .'"/>
							<cfdi:Retencion impuesto="ISR" importe="'. $retencion_isr .'"/>
						</cfdi:Retenciones>';
			}
			else{
				$cad_aux .= '<cfdi:Impuestos totalImpuestosTrasladados="'. $this->floor_dec( ($arreglo_datos[0]['subtotal'] / 100) * $arreglo_datos[0]['value'] ) .'">';
			}
			
			$cad_aux .= '<cfdi:Traslados>
			    <cfdi:Traslado impuesto="IVA" tasa="'.$arreglo_datos[0]['value'].'" importe="'. ( ($arreglo_datos[0]['subtotal'] / 100) * $arreglo_datos[0]['value'] ) .'"></cfdi:Traslado>
			  </cfdi:Traslados>
			</cfdi:Impuestos>
			</cfdi:Comprobante>';
			return $cfdi.$cad_aux;
			
		}
		
		/*trunca decimales*/
		function floor_dec($number){
			$precision = 2;
			$separator = '.';
			
			$numberpart=explode($separator,$number);
			$numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
			if($numberpart[0]>=0){
				$numberpart[1]=floor($numberpart[1]);
			}
			$ceil_number= array($numberpart[0],$numberpart[1]);
			return implode($separator,$ceil_number);
		}
		
	    
	}





