<?php

	require_once("../../framework/Archivo/Archivo.class.php");
	require_once("../../framework/CoreConstantes.class.php");

class Logger extends Archivo {
	
	private $ruta_logger = "";	
	
	
	public function Logger(){
		$this->ruta_logger = CoreConstantes::$to_root . "log/log.lsw";		
		parent::__construct($this->ruta_logger);		
		if(!$this->existArchivo()){
			$this->makeArchivo();
		}
	}
	
	public function log($excepcion){
		$cadena_log	= 	date("d/m/Y  -- G:i:s ") 	. 
						CoreConstantes::$sistema 	. 
						$excepcion->getCode()		. "--" .
					  	$excepcion->getMessage() 	. " Detalle ".
					  	$excepcion->getTraceAsString();
		$this->putToArchivo($cadena_log);
	}
	
	
	public function getLineaTrace($trace){
		$linea			= array();
		$linea_final	= "";
		if(is_array($trace)){
			$linea 			= $trace[0];
			$linea_final 	= $linea['file'] . " " . $linea['line'];
		}		
		return $linea_final;		
	}	
	
}



?>