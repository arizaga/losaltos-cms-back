<?php


require_once( "../../framework/CoreConstantes.class.php");
require_once( "../../framework/Xml/Xml.class.php");

class Rss{
	
	private $xml 		= null;
	
	private $objeto		= null;
	
	
	
	public function __construct($objeto){
		$registros = null;
		$clase 			= ucfirst(strtolower($objeto));
		$this->xml 		= new Xml();		
		$this->objeto	= new $clase();
		$this->xml->createRaiz("rss",array(array("version"=>"2.0")));							
		
		$this->xml->setNombreNodo("item");
		if($registros = $this->getRegistros()){
			$this->xml->fullElementos($this->setDetalles(),$registros);
		}
	}
		
	public function setDetalles(){	
		$nodo_canal = $this->xml->createNodo("channel","");	
		$detalles[] = array("elemento"=>"title"			,"contenido"=>CoreConstantes::$sistema);
		$detalles[] = array("elemento"=>"link"			,"contenido"=>CoreConstantes::$root_pagina);
		$detalles[] = array("elemento"=>"language"		,"contenido"=>"es");
		$detalles[] = array("elemento"=>"description"	,"contenido"=>CoreConstantes::$descripcion_rss);
		$detalles[] = array("elemento"=>"generator"		,"contenido"=>"Company Loading SW");
		$detalles[] = array("elemento"=>"lastBuildDate"	,"contenido"=>date("Y-m-d"));
		$detalles[] = array("elemento"=>"category"		,"contenido"=>"Eventos");
				
		foreach ($detalles as $detalle) {			
			$nodo_canal->appendChild($this->xml->createElemento($detalle["elemento"],$detalle["contenido"]));
		}
		$this->xml->addTo($this->xml->getRaiz(),$nodo_canal);
		return $nodo_canal;
	}
	
	public function getRegistros(){
		$this->checkPropiedad(get_class($this->objeto)."Constantes","listado_rss");		
		return $this->objeto->getAll(
					$this->objeto->getSQL("listado_rss"));
	}
	
	public function checkPropiedad($clase_objeto,$propiedad){
		$refleccion 			= null;
		$valor					= "";
		$refleccion_propiedad	= null;
		$regresa				= "";
		try{			
			$refleccion = new ReflectionClass($clase_objeto);
			if($refleccion->hasProperty($propiedad)){
				$refleccion_propiedad = $refleccion->getProperty($propiedad);
				$valor = $refleccion_propiedad->getValue(new $clase_objeto());
				if($valor != ""){
					$regresa = $valor;						
				}
			}		
		}catch (Exception $e){
			$this->instancia->completeTransaccion(false);
			throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_36 . $e->getMessage(),
										ErrorConstantes::$int_error_xml_36,
										LoggerConstantes::$LOG_NIVEL_CRITICO);
		}catch (ExcepcionSistema $es){
			$es->muestraError();
		}
		return $regresa;
	}

		
	public function imprime(){
		return $this->xml->getXML();
	}
	
	
	
	
}
?>