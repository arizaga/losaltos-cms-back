<?php

require_once( "../../framework/Request/RequestConstantes.class.php");

class Request{
	
	public static function getParametro($nombre_parametro){
		$regresa = null;
		
		if(isset($_REQUEST[$nombre_parametro])){
			$regresa = str_replace("'", "", addslashes($_REQUEST[$nombre_parametro]) );
		}
		
		return $regresa;		
	}
	
	public static function getParametros($nombre_parametros){
		$regresa = null;
		
		foreach ($nombre_parametros as $parametro) {
			if(isset($_REQUEST[$parametro])){
				$regresa[] = str_replace("'", "", addslashes($_REQUEST[$parametro]) ) ;
			}
			else {
				$regresa[] = "";
			}
		}		
		return $regresa;		
	}
	
	public static function setParametro($nombre_parametro,$valor){
		$_REQUEST[$nombre_parametro] = $valor;
	}
	
	public static function setParametros($arreglo_parametros){
		foreach ($arreglo_parametros as $parametro) {
			$_REQUEST[$parametro[RequestConstantes::$nombre_parametro]] = $parametro[RequestConstantes::$nombre_valor];
		}
	}
	
	public static function parametrosJqgrid(){
		
		unset($_REQUEST["nd"]);
		unset($_REQUEST["rows"]);
		unset($_REQUEST["page"]);
		unset($_REQUEST["sidx"]);
		unset($_REQUEST["sord"]);
		unset($_REQUEST["searchOper"]);
        
		//var_dump($_REQUEST);
		//exit;
        
		if((self::getParametro("searchField") == "" || self::getParametro("searchField") == NULL) && (self::getParametro("filters") != "" || self::getParametro("filters") != NULL) ){
			$cadenaJson = $_REQUEST["filters"];
			$arregloJson = json_decode($cadenaJson, true);
			$arregloParametros = $arregloJson["rules"];
			//var_dump($arregloParametros);
            
			foreach($arregloParametros as $temp){
				$key = $temp["field"];
				$_REQUEST[$key] = utf8_decode($temp["data"]);
			}
            
		}
		elseif(self::getParametro("searchString")){
			$key = $_REQUEST["searchField"];
			$_REQUEST[$key] = $_REQUEST["searchString"];
		}
        
		unset($_REQUEST["searchField"]);
		unset($_REQUEST["searchString"]);
		unset($_REQUEST["filters"]);
		
		foreach($_REQUEST as $key => $value) {
			$_REQUEST[$key] = utf8_decode($value);
		}

		
	}
	
	
}


?>