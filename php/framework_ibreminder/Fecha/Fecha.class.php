<?php
	

	class Fecha {
		
		public static function diffDate($fecha_inicial,$fecha_final,$diferencia = "d"){
			return date($diferencia,(strtotime($fecha_final) - strtotime($fecha_inicial)));		
		}
		
		public static function addDateDay($fecha,$numero_dias){
			return date("Y-m-d",mktime(	0, 0, 0, 
										date("n",strtotime($fecha)) ,
										date("j",strtotime($fecha))+$numero_dias, 
										date("Y",strtotime($fecha))));
		}
		
		public static function getDataFromDate($fecha,$requiero = "d"){
			return date($requiero,strtotime($fecha));
		}
		
		public static function getFechaFromateada($fecha,$formato = "%A %d %B de %Y"){
			//setlocale(LC_ALL,"es_ES");//para linux
			setlocale(LC_ALL,"esp"); //para windows
			//setlocale(LC_TIME,"esp"); //para windows
			return ucfirst(strftime($formato,strtotime($fecha)));
		}
		
		/**
		 * Agrega segundos a una fecha para retornar la fecha mas 
		 * los segundos sumados...
		 *
		 * @param String $fecha (Timestamp)
		 * @param int $cantidad_segundos (strtotime)
		 */
		public static function addTime2Fecha($fecha,$cantidad_segundos,$formato= "Y-m-d H:i:s"){
			return date($formato,strtotime($fecha) + $cantidad_segundos);
		}
		
		
	}	
	
?>