<?php



	class ErrorConstantes{
		
		public static $int_error_xml_01 = 1;	
		public static $int_error_xml_02 = 2;	
		public static $int_error_xml_03 = 3;	
		public static $int_error_xml_04 = 4;
		public static $int_error_xml_05 = 5;
		public static $int_error_xml_06 = 6;
		public static $int_error_xml_07 = 7;
		public static $int_error_xml_08 = 8;
		public static $int_error_xml_09 = 9;
		public static $int_error_xml_10 = 10;
		public static $int_error_xml_11 = 11;
		public static $int_error_xml_12 = 12;
		public static $int_error_xml_13 = 13;
		public static $int_error_xml_14 = 14;
		public static $int_error_xml_15 = 15;
		public static $int_error_xml_16 = 16;
		public static $int_error_xml_17 = 17;
		public static $int_error_xml_18 = 18;
		public static $int_error_xml_19 = 19;
		public static $int_error_xml_20 = 20;
		public static $int_error_xml_21 = 21;
		public static $int_error_xml_22 = 22;
		public static $int_error_xml_23 = 23;
		public static $int_error_xml_24 = 24;
		public static $int_error_xml_25 = 25;
		public static $int_error_xml_26 = 26;
		public static $int_error_xml_27 = 27;
		public static $int_error_xml_28 = 28;
		public static $int_error_xml_29 = 29;
		public static $int_error_xml_30 = 30;
		public static $int_error_xml_31 = 31;
		public static $int_error_xml_32 = 32;
		public static $int_error_xml_33 = 33;
		public static $int_error_xml_34 = 34;
		public static $int_error_xml_35 = 35;
		public static $int_error_xml_36 = 36;
		public static $int_error_xml_37 = 37;
		public static $int_error_xml_38 = 38;
		public static $int_error_xml_39 = 39;
		public static $int_error_xml_40 = 40;
		public static $int_error_xml_41 = 41;
		public static $int_error_xml_42 = 42;
		public static $int_error_xml_43 = 43;
		public static $int_error_xml_44 = 44;
		public static $int_error_xml_45 = 45;
		public static $int_error_xml_46 = 46;
		public static $int_error_xml_47 = 47;
		public static $int_error_xml_48 = 48;
		
		
		
		public static $str_error_xml_01 = "No se cargo el archivo WEB.XML";	
		public static $str_error_xml_02 = "No se encuentra el archivo solicitado";	
		public static $str_error_xml_03 = "No se cargo el archivo de inicio";	
		public static $str_error_xml_04 = "No esta registrado el archivo solicitado";	
		public static $str_error_xml_05 = "Error al conectarse a la base de datos";	
		public static $str_error_xml_06 = "Fallo el driver de conexi�n";	
		public static $str_error_xml_07 = "Error en el proceso de conexion a la BD";	
		public static $str_error_xml_08 = "Error al obtener los datos del archivo de configuraci�n";	
		public static $str_error_xml_09 = "No se encontro el archivo de configuraci�n";	
		public static $str_error_xml_10 = " debe tener algun valor...";	
		public static $str_error_xml_11 = "No se obutvo respuesta de la p�gina solicitada";	
		public static $str_error_xml_12 = " es una mail incorrecto";	
		public static $str_error_xml_13 = " no es un entero v�lido";	
		public static $str_error_xml_14 = " no es una cadena v�lida";	
		public static $str_error_xml_15 = " la longitud del campo rebaso el l�mite permitido";	
		public static $str_error_xml_16 = " No se pudieron editar los registros solicitados...";	
		public static $str_error_xml_17 = "No existen los registros secundarios...";	
		public static $str_error_xml_18 = " Error en el SQL";	
		public static $str_error_xml_19 = "El registro a eliminar no existe";	
		public static $str_error_xml_20 = "No existe el registro solicitado...";	
		public static $str_error_xml_21 = "Problemas al agregar...";	
		public static $str_error_xml_22 = "Problemas al editar...";	
		public static $str_error_xml_23 = "Problemas al eliminar el registro";
		public static $str_error_xml_24 = " No se selecciono ninguna opc�on para el Listado";
		public static $str_error_xml_25 = " La funcion getSQL no existe en la clase solicitada";
		public static $str_error_xml_26 = "No existen registros para mostrar";
		public static $str_error_xml_27 = "No existen campos de imagen para tratar";
		public static $str_error_xml_28 = "No hay imagenes disponibles para modificar";
		public static $str_error_xml_29 = "No esta definido el directorio ";
		public static $str_error_xml_30 = "Esta vac�a la ruta ";
		public static $str_error_xml_31 = "No existen datos para insertar el registro";
		public static $str_error_xml_32 = "No se ha dado informaci�n para pedir el listado";
		public static $str_error_xml_33 = "No se crearon el mismo numero de thumbs que de im�genes";
		public static $str_error_xml_34 = "Error al tratar de renombrar una imagen que no existe";
		public static $str_error_xml_35 = "La clase de constantes no ha sido definida o no existe";
		public static $str_error_xml_36 = "Error con ";
		public static $str_error_xml_37 = "No existe la libreria GD";
		public static $str_error_xml_38 = "El directorio de las imagenes no existe...";
		public static $str_error_xml_39 = "Las imagenes porporcionadas no estan en un arreglo";
		public static $str_error_xml_40 = "El arreglo de las iamgenes esta vac�o";
		public static $str_error_xml_41 = "No se pudieron crear los thumbs por los permisos del directorio";
		public static $str_error_xml_42 = "No existe el metodo: ";
		public static $str_error_xml_43 = "Problemas con la eliminaci�n l�gica del registro";
		public static $str_error_xml_44 = "El registro a eliminar no existe e.l.";
		public static $str_error_xml_45 = "Error SQL: ";
		public static $str_error_xml_46 = "No esta permitida esta acci�n";
		public static $str_error_xml_47 = "No se eliminaron los multiregistros...";
		public static $str_error_xml_48 = "No se agrego movimiento al historial...";
	}
	
	?>