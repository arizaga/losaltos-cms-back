<?php
require_once('fpdf.php');

	class Pdf extends FPDF{
		
		private $tipo_header = "";
		
		function __construct($orientacion = 'P' ,$unidad = "mm", $formato= "A4"){
			$this->tipo_header = $orientacion;
			parent::FPDF($orientacion,$unidad,$formato);
			//$this->AddPage();
			$this->SetFont('Arial','B',16);						
		}
		
		public function setFuentes($family  = "Arial" ,$style= "",$size = 12){
			$this->SetFont($family,$style,$size);
		}
		
		public function finishPdf($nombre= "",$destino = ""){
			if(!$nombre && !$destino){
				$this->Output();	
			}
			else {
				$this->Output($nombre,$destino);
			}			
		}
		
		function Header(){
			/*
			if($this->tipo_header == "P"){
			    $this->Image('../../framework/imagenes/logo.jpg',85,8,50,11);
			    $this->SetFont('Arial','B',15);
			    $this->Ln(20);				

			}
			else {
			    $this->Image('../../framework/imagenes/logo.jpg',130,8,100,22);
			    $this->SetFont('Arial','B',15);
			    $this->Ln(20);				
			}
			*/
		}	
	
		function Footer(){
		    $this->SetY(-15);
		    $this->SetFont('Arial','I',8);
		    $this->AliasNbPages();
		    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
		}
		
		function putRegistro($registros){
			$x = 15;
			foreach ($registros as $registro) {
				$this->SetX($x+=10);
				$this->Cell(10,"",$registro,0,0,"C");
			}			
		}
		
		function putRecuadros($registros,$y,$cuarto){
			$x = 18;
			//$this->SetY($y+1);
			$this->Cell($x,3,$cuarto,0,1,"C");
			$this->Line(25,$this->GetY()-5,340,$this->GetY()-5);
			foreach ($registros as $registro) {				
				if($registro){
					$this->Rect($x+10,$this->GetY()-4,4,4,"F");
				}
				$x+=10;
			}
			$this->Ln(3);			
		}


		
		
	}
?>