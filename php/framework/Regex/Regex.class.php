<?php
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/ErrorConstantes.class.php");

	class Regex {
		
		public $campo;
		
		public $valor;
		
		public $longitud;
		
		public $nulidad;
		
		function __construct($datos_evaluar = array()){
			
			foreach ($datos_evaluar as $registro) {			
			
				foreach ( $registro as $dato ) {
					
					$opcion 		= $dato->type;
					$this->campo 	= ucfirst($dato->name);
					$this->valor 	= $dato->value;
					$this->longitud = $dato->max_length;
					$this->nulidad  = $dato->not_null;
					
					switch ( $opcion ){
						
						case "varchar":
							$this->validateNulidad();
							//$this->validateCadenaAlfanumerica();
							$this->validateLongitud();
							break;
	
						case "int":
							$this->validateEntero();
							break;
							
						case "date":
							break;
							
						case "double":
							break;
							
						case "decimal":
							$this->validateNulidad();
							$this->validateFlotante();
							break;
							
						case "enuum":
							break;
							
						default:
							break;	
					}					
					
				}
			}
			
		}
		
		function validateNulidad(){
			try {
				if($this->nulidad && !$this->valor){
				throw new ExcepcionSistema(		"El campo " .$this->campo . ErrorConstantes::$str_error_xml_10,
												ErrorConstantes::$int_error_xml_10,
												LoggerConstantes::$LOG_NIVEL_CRITICO);					   		
				}
			}
		   catch (ExcepcionSistema   $e){
		   		$e->muestraError();
		   }
		}
		
		function validateEmail(){
		   try {
				if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/',$this->valor)){
				throw new ExcepcionSistema(		"El campo " .$this->campo . ErrorConstantes::$str_error_xml_12,
												ErrorConstantes::$int_error_xml_12,
												LoggerConstantes::$LOG_NIVEL_CRITICO);					   		
				}
		   }
		   catch (ExcepcionSistema   $e){
		   		$e->muestraError();
		   }
		}
		
		function validateEntero(){
			try {
				if(!preg_match('/^-?[0-9]{1,10}$/',$this->valor) ){
				throw new ExcepcionSistema(		"El campo " .$this->campo .ErrorConstantes::$str_error_xml_13,
												ErrorConstantes::$int_error_xml_13,
												LoggerConstantes::$LOG_NIVEL_CRITICO);					   		

				}
			}
			catch (ExcepcionSistema  $e){
				$e->muestraError();
			}
		}
		
		function validateDinero(){
			try {
				if(!preg_match('/^[0-9]+\.[0-9]{0,2}$/',$this->valor)){
					
				}
			}
			catch (ExcepcionSistema   $e){
				
			}
		}
		
		function validateFlotante(){
			try {
				if(!preg_match('/[0-9]+(\.[0-9])*$/',$this->valor)){
					throw new Exception();
				}
			}
			catch (Exception  $e){
				
			}			

		}
		
		function validateCadenaAlfanumerica(){
			try {
				//if(!preg_match('/^[a-z0-9]+./',$this->valor) && $this->nulidad){
				if(!preg_match('/^[A-Za-z0-9_]+$/',$this->valor) && $this->nulidad){
				throw new ExcepcionSistema(		"El campo " .$this->campo . ErrorConstantes::$str_error_xml_14,
												ErrorConstantes::$int_error_xml_14,
												LoggerConstantes::$LOG_NIVEL_CRITICO);					   						

				}
			}
			catch (ExcepcionSistema   $e){
				$e->muestraError();
			}
			
		}		
		
		function validateLongitud(){
			try {
				if(strlen($this->valor) > $this->longitud){
				throw new ExcepcionSistema(		"El campo " .$this->campo . ErrorConstantes::$str_error_xml_15,
												ErrorConstantes::$int_error_xml_15,
												LoggerConstantes::$LOG_NIVEL_CRITICO);					   						

				}
			}
			catch (ExcepcionSistema   $e){
				$e->muestraError();
			}			
		}
		
		
	}



?>