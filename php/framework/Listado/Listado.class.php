<?php

	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../framework/Listado/ListadoConstantes.class.php");
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/ErrorConstantes.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");


	class Listado{
		
		private $registros	= null;
		
		private $opcion		= "json_normal";
		
		private $instancia	= null;
		
		private $sql		= "";
		
		private $xml		= null;
		
		public function setFunciones($funciones_campos){
			$this->xml->setFuncionesCampos($funciones_campos);
		}
		
		
		public function getRegistros(){
			return $this->registros;
		}
		
		public function getInstancia(){
			return $this->instancia;
		}
		
		public function __construct($directiva_sql ,$instancia,$sql=""){
			try{				
				$this->xml = new Xml();
				$instancia = ucfirst(strtolower($instancia));
				$this->instancia 		= new $instancia();
				Request::parametrosJqgrid();
				//var_dump($directiva_sql);
				
				/*if($directiva_sql != 'listado_user_login'){
					$token   = Request::getParametro('token');
					if($token == null){
						
						if(CoreConstantes::$json)
							echo $this->xml->getListadoJsonOrdinario( array(array("Error"=>'El token es nulo')) );
						else{
							throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_20,
													ErrorConstantes::$int_error_xml_20,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
						}
						
						exit;
					}
					$id_user = Request::getParametro('id_user');
					$stmt = $this->instancia->Execute("SELECT token FROM user WHERE id_user = '$id_user' ");
					$arreglo = $stmt->getRows();
					
					if(!empty($arreglo)){
						if($arreglo[0]['token'] != $token){
							echo $this->xml->getListadoJsonOrdinario( array(array("Error"=>'El token no coincide')) );
							exit;
						}
					}
					
				}*/
				
				if($directiva_sql){
					
					if(method_exists($this->instancia,"getSQL")){
						$this->sql 		= $this->instancia->getSQL($directiva_sql);
					}
					else {
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_25,
													ErrorConstantes::$int_error_xml_25,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
				}
				elseif ($sql){
					$this->sql = $sql;
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_32,
												ErrorConstantes::$int_error_xml_32,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
							
			}catch (ExcepcionSistema  $e){
				$e->muestraError();
			}
		}
		
		public function replaceParametros($reemplazo , $busqueda= "@@"){
			$this->sql = $reemplazo ? Replace::replaceDatos($busqueda,$reemplazo,$this->sql) : "";
		}
		
		public function setOpcion($opcion){
			$this->opcion = $opcion;
		}
		
		public function setCampos($campos){
			$this->xml->setCampos($campos);
		}
		
		public function showMensaje($alerta){
			$this->xml->showAlerta($alerta);
		}
		
		public function getSQL(){
			try{
				switch ($this->opcion){
					case ListadoConstantes::$campo:
						$this->registros = array($this->instancia->getOne($this->sql));
						break;

					case ListadoConstantes::$registros:
						$this->registros = $this->instancia->getAll($this->sql);
						break;
						
					case ListadoConstantes::$registro:
						$this->registros = $this->instancia->getRegistro($this->sql);
						break;
					
					case ListadoConstantes::$sp:
						$this->registros = $this->instancia->getSp($this->sql);
						break;
					
					case ListadoConstantes::$json:
						$this->registros = $this->instancia->getJson($this->sql);
						break;
					
					case ListadoConstantes::$json_normal:
						$this->registros = $this->instancia->getJson($this->sql);
						break;
					
					case ListadoConstantes::$select_html:
						$this->registros = $this->instancia->getJson($this->sql);
						break;
						
					default:
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_24,
													ErrorConstantes::$int_error_xml_24,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
						break;
				}
			}catch (ExcepcionSistema $e){
				$e->muestraError();
			}			
		}
		
		
		public function showListado($arreglo = NULL, $id_select = ""){
			if($this->opcion == "select_html"){
				
				if(is_array($arreglo)){
					array_push($this->registros,$arreglo);
					$this->registros = array_reverse($this->registros);
				}
				
				return $this->showSelectHtml($arreglo, $id_select);
			
			}
			elseif($this->opcion == "json"){
				
				if(is_array($arreglo)){
					array_push($this->registros,$arreglo);
					$this->registros = array_reverse($this->registros);
				}
				
				return $this->showJson($arreglo);
			
			}
			elseif($this->opcion == "json_normal"){

				if(is_array($arreglo)){
					array_push($this->registros,$arreglo);
					$this->registros = array_reverse($this->registros);
				}
				
				return $this->showJsonNormal($arreglo);
			
			}
			else{
				$this->getSQL();
				$this->changeTipoRegistros();
			
				if(is_array($arreglo)){
					array_push($this->registros,$arreglo);
					$this->registros = array_reverse($this->registros);
				}
				
				return $this->xml->getlistado($this->registros,"Listado...");
			}//cierra elseif
			
			
		}
		
		public function showJson($arreglo = NULL){
			$this->getSQL();
			$this->changeTipoRegistros();
			
			return $this->xml->getListadoJson($this->registros);
			
		}
		
		public function showJsonNormal($arreglo = NULL){
			$this->getSQL();
			$this->changeTipoRegistros();
			
			return $this->xml->getListadoJsonOrdinario($this->registros);
			
		}
		
		public function showSelectHtml($arreglo = NULL, $id_select = ""){
			$this->getSQL();
			$this->changeTipoRegistros();
			
			return $this->xml->getSelectHtml($this->registros, $id_select );
			
		}
		
		public function executeSp(){
			$this->getSQL();
			/*$this->changeTipoRegistros();			
			return  $this->xml->getlistado($this->registros,"Listado...");*/
		}
		
		public function showXML(){
			$this->changeTipoRegistros();
			return  $this->xml->getlistado($this->registros,"Listado...");
		}
		
		public function changeTipoRegistros(){
			if($this->opcion == ListadoConstantes::$registro){
				$this->registros = array($this->registros);
			}
		}
		
	}


?>