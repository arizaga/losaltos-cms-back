<?php
	
	class CoreConstantes{
		
		public static $to_root 			= "../../";	
		public static $root_pagina 		= "http://www.arizagadd.com/";			
		public static $ruta_pagina		= "http://www.arizagadd.com/php/";		
		public static $carpeta_xml		= "xml/";
		public static $pagina_error		= "error/error/error_xml.php";
		public static $sistema			= " ";
		public static $ruta_imagenes		= "../../../imagenes/";
		public static $ruta_imagenes_boletin 	= "framework/imagenes/";
		//public static $correo_boletin 	= "contacto@daas.com.mx";
		public static $largo_tupla 		= 130;
		public static $ruta_imagenes_noticia	= "/imagenes/";
		//public static $ruta_noticia		= "/noticias/";
		public static $ruta_suscribe		= "xml/boletin/activar_cuenta_xml.php";
		public static $descripcion_rss		= "Injal";
		public static $correo_contacto		= "contacto@infact.com";
		public static $correo_info		= "info@infact.com";
		public static $menu_movil		= array("Inicio"=>"http://www.mobile.ding.mx/index.php");
		public static $json			= true;
		
		
		/*Facturacion*/
		
		public static $rfc_emisor 			= "ESI920427886";
		public static $numero_certificado 	= "20001000000200000192";
		public static $archivo_cer 			= "../../archivos/certificados/20001000000200000192.cer";
		public static $archivo_key 			= "../../archivos/certificados/20001000000200000192.key";
		public static $archivo_pem 			= "../../archivos/certificados/20001000000200000192.key.pem";
		public static $cadena_original 		= "../../archivos/xslt32/cadenaoriginal_3_2.xslt";
		public static $comprobantes 		= "../../archivos/comprobantes/";
		public static $ruta_pdf 			= "../../archivos/comprobantes/pdf/";
		public static $ruta_png 			= "../../archivos/comprobantes/png/";
		public static $pass_key 			= "12345678a";
		public static $ruta_certificados 	= "../../archivos/certificados/";
		
		/*Para acceso al servidor de Facturacion Moderna S.A. de C.V.*/
		
		public static $url_timbrado 		= "https://t1demo.facturacionmoderna.com/timbrado/wsdl";
		public static $user_id 				= "UsuarioPruebasWS";
		public static $user_password 		= "b9ec2afa3361a59af4b4d102d3f704eabdf097d4";
		
		
		/*
		public static $url_timbrado 		= "https://t1.facturacionmoderna.com/timbrado/wsdl";
		public static $user_id 			= "BERA9006056E0";
		public static $user_password 		= "17de06c4491678b150cabfda4ef743afd7cca43a";
		*/
	}
?>