<?


	require_once("../../framework/utils/Files.class.php");
	require_once("../../framework/ExcepcionSistema/ExcepcionSistema.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/ErrorConstantes.class.php");
	class Thumbs extends Files{
    /**#@+
    * @access public
    */
    /**
    * Originals images dir. Eg: "imagens/"
    * @var string $_directory
    */
    var $_directory;

    /**
    * File list of $directory that will be generate thumbs
    * @var array $_file_list
    */
    var $_file_list = array();

    /**
    * Directory that the thumbs will be saved. Eg: "thumbs/"
    * @var string $_thumbs_directory
    */
    var $_thumbs_directory;

    /**
    * Thumbs name that will be attached to image original name 
    * @var string $_thumbs_name
    */
    var $_thumbs_name;

    /**
    * 0-100 value of thumbs JPEG quality
    * @var integer $_thumbs_quality
    */
    var $_thumbs_quality;

    /**
    * Original image dimensions percent that will be given to the thumbs
    * @var integer $_thumbs_percent
    */
    var $_thumbs_percent;

    /**
    * Unique width to all the thumbs
    * @var integer $_thumbs_width
    */
    var $_thumbs_width;

    /**
    * Unique height to all the thumbs
    * @var integer $_thumbs_height
    */
    var $_thumbs_height;

    /**
    * Grayscale thumbnails?
    * @var boolean $_gray_scale
    */
    var $_gray_scale;
    /**#@-*/

    /**#@+
    * @access private
    */
    /**
    * Server has GIF Write Support?
    * @var boolean $_gif
    */
    var $_gif;    

    /**
    * Images info (getimagesize())
    * @var array $_original_info
    */
    var $_original_info = array();
    /**#@-*/

    var $_new_name_thumbs;
    
    var $instancia		= null;
	
	var $new_width;
	var $new_height;
    
    // {{{ construtor
    /**
    * Sets class vars. Checks if the server has images creation support.
    * If doesn't gave, creates files list. Creates thumbs dir, if inexistent.
    * If you don't give the $file_list parameter, the class will generate thumbs of all images of the dir.
    * If you don't give the $thumbs_directory parameter, you must give $thumbs_name parameter and vice versa.
    * Or give both.
    * You must give $thumbs_percent or $thumbs_width + $thumbs_height, but don't both. If you give both
    * ($thumbs_percent + $thumbs_width&$thumbs_height) the system will use $thumbs_percent
    *
    * @since Jan 19, 2004
    * @version Aug 01, 2004
    * @access public
    */
    

    function Thumbs(	$directory, 
    					$file_list = false, 
    					$thumbs_directory = false, 
    					$thumbs_name = false, 
    					$_gray_scale = false, 
    					$thumbs_quality = 100, 
    					$thumbs_percent = 35, 
    					$thumbs_width = false, 
    					$thumbs_height = false,
    					$instancia		= null)
    {
    	try{
    		$this->instancia = $instancia;
    		// Check if the server has GD library on PHP

    		if(!function_exists("imagecreatetruecolor")) // gd 2.*
    		{
    			if(!function_exists("imagecreate")) // gd 1.*
    			{
    				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_37,
    											ErrorConstantes::$int_error_xml_37,
    											LoggerConstantes::$LOG_NIVEL_CRITICO);
    				//$this->_error("You can't run this script because your PHP hasn't GD library (1.* or 2.*) loaded.", E_USER_ERROR);
    			}
    		}

    		// Checks if the server has gif writing support
    		$this->checks_gif_support();

    		// Checks the dir
    		if(!is_dir($directory))
    		{
    			throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_38,
											ErrorConstantes::$int_error_xml_38,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
    			//$this->_error("Invalid directory: <B>" . $directory . "</B>", E_USER_ERROR);

    		}
    		else
    		{
    			$this->_directory = $directory;
    		}

    		// If gave the file list, but it doesn't and array, raise an error
    		if($file_list && !is_array($file_list))
    		{
    			throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_39,
											ErrorConstantes::$int_error_xml_39,
											LoggerConstantes::$LOG_NIVEL_CRITICO);


    		}
    		// File list doesn't given. So, creates it!
    		elseif(!$file_list)
    		{
    			// Get the list with all images, included GIFs
    			if($this->_gif)
    			{
    				$this->_file_list = $this->get_file_list($this->_directory, "img_creation_gif");
    			}
    			// Get the list with all images, but GIFs
    			else
    			{
    				$this->_file_list = $this->get_file_list($this->_directory);
    			}
    		}
    		else
    		{
    			if(!sizeof($file_list))
    			{
    				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_40,
												ErrorConstantes::$int_error_xml_40,
												LoggerConstantes::$LOG_NIVEL_CRITICO);

    			}
    			$this->_file_list = $file_list;
    		}

    		// Sets other class vars
    		if(!$thumbs_directory && !$thumbs_name)
    		{
    			$this->_error("You must give at once one type that of the thumbanils will be saved (directory or name). The class assumed the directory <B>\"thumbs\"</B>.", E_USER_NOTICE);
    			$thumbs_directory = "thumbs/";
    		}


    		$this->_gray_scale = $_gray_scale;
    		$this->_thumbs_name = $thumbs_name;
    		$this->_thumbs_quality = $thumbs_quality;
    		$this->_thumbs_percent = $thumbs_percent;
    		$this->_thumbs_width = $thumbs_width;
    		$this->_thumbs_height = $thumbs_height;
    		$this->_thumbs_directory = $thumbs_directory;

    		if(!$this->_thumbs_percent && !$this->_thumbs_width)
    		{
    			$this->_error("You must give at once one type that of the thumbanils dimensions (percent or unique sizes). The class assumed 35%", E_USER_NOTICE);
    			$this->_thumbs_percent = 35;
    		}
    	}catch (ExcepcionSistema $e){
    		$this->instancia->completeTransaccion(false);
    		$e->muestraError();
    	}
    }
    // }}}

    // {{{ creates_thumbs()
    /**
    * Generate the Thumbnails
    *
    * @since Jan 19, 2004
    * @version Aug 01, 2004
    * @access public
    */
    function creates_thumbs()
    {
    	try{
    		/**
        * Creates the thumbs dir, if inexistent
        */
    		//echo $this->_thumbs_directory;

    		//$this->_thumbs_directory = $this->_directory . $this->_thumbs_directory;

    		if(!is_dir($this->_thumbs_directory))
    		{
    			if(!@mkdir($this->_thumbs_directory, 0777))
    			{
    				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_41,
												ErrorConstantes::$int_error_xml_42,
												LoggerConstantes::$LOG_NIVEL_CRITICO);

    			}
    			@chmod($this->_thumbs_directory, 0777);
    		}

    		// Loop in file list
    		for($i = 0; $i < count($this->_file_list); ++$i)
    		{
    			// Path of the original image
    			$file = $this->_directory . $this->_file_list[$i];

    			// Checks if the file exists
    			if(!file_exists($file))
    			{
    				$this->_error("Inexistent file: <B>" . $file . "</B>. Thumbnail didn't created.", E_USER_NOTICE);
    				continue;
    			}

    			/**
            * Gets info about the original image. 
            */
    			$this->_original_info = getimagesize($file);

    			// Regex for checking if the file is valid
    			$regex = "/^image\/(pjpeg|jpeg|png";
    			// Add GIF if the server has GIF Write Support
    			if($this->_gif)
    			{
    				$regex .= "|gif";
    			}
    			$regex .= ")$/i";

    			// Checks if the file is a valid image
    			if(!preg_match($regex, $this->_original_info["mime"]) || !sizeof($this->_original_info))
    			{
    				$msg = "They must be JPEG or PNG";
    				if($this->_gif)
    				{
    					$msg .= " or GIF";
    				}
    				$msg .= ".";
    				$this->_error("Invalid image: <B>" . $file . "</B>. " . $msg, E_USER_NOTICE);
    				continue;
    			}

    			// Sets thumb name
    			if($this->_thumbs_name)
    			{
    				/**
                * Original image name - index [1][0] of array $extension
                * Original image extension - index [2][0] of array $extension
                */
    				preg_match_all("/(.*)\.(.*)$/i", $this->_file_list[$i], $extension);

    				// $thumb = directory + original_file_name + thumbs_name + original_file_extension
    				$name = is_array($this->_thumbs_name) ? $this->_thumbs_name[$i] : $this->_thumbs_name;
    				$thumb = $this->_thumbs_directory .  $name . "." . $extension[2][0];

    				//FIX ME PARA SABER SI ES NECESARIO
    				if(is_array($this->_thumbs_name)){
    					$this->_new_name_thumbs[] = $name . "." . $extension[2][0];
    				}
    				else {
    					$this->_new_name_thumbs[]   = $name . "." . $extension[2][0];
    				}

    			}
    			else
    			{
    				$thumb = $this->_thumbs_directory . $this->_file_list[$i];
    			}

    			// Thumbs dimensions. If a percent was given, calculate the values
    			if($this->_thumbs_percent)
    			{

    				$this->_thumbs_width = (integer)($this->_original_info[0] / 100 * $this->_thumbs_percent);
    				$this->_thumbs_height = (integer)($this->_original_info[1] / 100 * $this->_thumbs_percent);
    			}

    			// Creates the thumbnail
    			$this->resize_image($file, $thumb);
    		}    	
        // end for
    	}catch (ExcepcionSistema $e){
    		$this->instancia->completeTransaccion(false);
    		$e->muestraError();
    	}
    }
    // }}}

    // {{{ resize_image()
    /**
    * Creates the thumbnail
    *
    * @param string $image
    * @param string $newimage
    * @since Jan 19, 2004
    * @access private
    */
	function resize_image($image, $newimage)
	{
		
		return $this->resize_image_using_gd($image, $newimage);
	}
	
	function putOnThumb($ancho, $alto){
		
		$medias = array();		
		
		
		$factor_imagen	= $ancho / $alto;
		$factor_thumb	= $this->_thumbs_width / $this->_thumbs_height;
		
		
		if( $factor_imagen > $factor_thumb ){			
			 $porcentaje  = round($this->_thumbs_height * 100 / $alto);			
			 $medidas['nuevo_ancho']  = (integer)($ancho / 100 * $porcentaje);
			 $medidas['nuevo_alto']   = $this->_thumbs_height;			
			
		}
		elseif ( $factor_imagen < $factor_thumb ){
			 $porcentaje = round($this->_thumbs_width  * 100 / $ancho);
			 $medidas['nuevo_alto']  = (integer)($alto / 100 * $porcentaje);
			 $medidas['nuevo_ancho'] = $this->_thumbs_width;					
		}
		else {
			 $porcentaje  = round($this->_thumbs_height * 100 / $alto);
	         $medidas['nuevo_ancho'] = (integer)($ancho / 100 * $porcentaje);
			 $medidas['nuevo_alto']  = (integer)($alto / 100 * $porcentaje);			
		}
		return $medidas;
		
	}
	

    /**
    * Creates the thumbnail
    *
    * @param string $image
    * @param string $newimage
    * @since Jan 19, 2004
    * @version Feb 05, 2004
    * @access private
    */
    
	function resize_image_using_gd($image, $newimage)
	{		
		
	        
		$width  = $this->_original_info[0];
	        $height = $this->_original_info[1];
	        $type   = $this->_original_info[2];

			// Thumb dimensions, calculated on creates_thumbs() or a unique value
			
			//Calcula los tama�os para que se ajusten al thumb			
			$nuevas_medidas = $this->putOnThumb($width,$height);
			
	        $newwidth  = $nuevas_medidas['nuevo_ancho'];
	        $newheight = $nuevas_medidas['nuevo_alto'];

	        $this->new_width = $newwidth;
	        $this->new_height = $newheight;
			
        if($im = $this->read_image_from_file($image, $type))
        {
            // This was a bug.. corrected in Feb 05, 2004
            /*if ($newheight && ($width < $height))
            {
                $newwidth = ($newheight / $height) * $width;
            }
            else
            {
                $newheight = ($newwidth / $width) * $height;
            }*/

            if (function_exists("ImageCreateTrueColor") && !$this->_gray_scale)
            {
                $im2 = ImageCreateTrueColor($newwidth,$newheight);
		    }
            else
            {
                $im2 = ImageCreate($newwidth,$newheight);
            }

            // Check if needs to create the thumbs into grayscale, if yes, do it
            $this->_gray_scale_go($im2);

            if (function_exists("ImageCopyResampled"))
            {
                ImageCopyResampled($im2,$im,0,0,0,0,$newwidth,$newheight,$width,$height);
		    }
            else
            {
                ImageCopyResized($im2,$im,0,0,0,0,$newwidth,$newheight,$width,$height);
            }

            if ($this->write_image_to_file($im2, $newimage, $type))
            {
                return true;
            }
        }

        $this->_error("I couldn't create the thumb: <B>" . $newimage . "</B>", E_USER_NOTICE);
        return false;
	}
    // }}}
    
    // {{{ read_image_from_file()
    /**
    * Creates image handle
    *
    * @param string $filename
    * @param integer $type
    * @since Jan 19, 2004
    * @access private
    */
    function read_image_from_file($filename, $type)
    {
	   $imagetypes = ImageTypes();

        switch ($type)
        {
            case 1 :
                if ($imagetypes & IMG_GIF)
                {
                    return ImageCreateFromGIF($filename);
                }
            case 2 :
                if ($imagetypes & IMG_JPEG)
                {
                    return ImageCreateFromJPEG($filename);
                }
                break;
            case 3 :
                if ($imagetypes & IMG_PNG)
                {
                    return ImageCreateFromPNG($filename);
                }
                break;
            default:
                $this->_error("Invalid type given to read_image_from_file() of file <B>" . $filename . "</B>", E_USER_NOTICE);
                return false;
        }
	}
    // }}}

    // {{{ write_image_to_file()
    /**
    * Writes the new image
    *
    * @param resource $im
    * @param string $filename
    * @param integer $type
    * @since Jan 19, 2004
    * @access private
    */
	function write_image_to_file($im, $filename, $type)
	{
        switch ($type)
        {
            case 1 :
                $result = ImageGif($im, $filename);                
                @chmod($filename, 0777);
                return $result;
            case 2 :
                $result = ImageJpeg($im, $filename, $this->_thumbs_quality);
                @chmod($filename, 0777);
                return $result;
            case 3 :
                $result = ImagePNG($im, $filename);
                @chmod($filename, 0777);
                return $result;
            default:
                $this->_error("Invalid image type: <B>" . $filename . "</B>", E_USER_NOTICE);
                return false;
        }
	}
    // }}}

    // {{{ _gray_scale_go()
    /**
    * Turns the thumbnails into grayscale
    *
    * @param resource $im
    * @since Feb 05, 2004
    * @access private
    */
    function _gray_scale_go(&$im)
    {
        if($this->_gray_scale)
        {
            for ($c = 0; $c < 256; $c++)
            {
                ImageColorAllocate($im, $c,$c,$c); 
            }
        }
    }
    // }}}

    // {{{ checks_gif_support()
    /**
    * Checks if the server has GIF write support
    *
    * @since Aug 01, 2004
    * @access private
    */
    function checks_gif_support()
    {
        $imagetypes = imagetypes();

        if ($imagetypes & IMG_GIF)
        {
            $this->_gif = true;
        }
        else
        {
            $this->_gif = false;
        }
    }
    // }}}
}
?>
