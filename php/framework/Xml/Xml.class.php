<?php	
	
	require_once( "../../framework/ErrorConstantes.class.php");
	require_once( "../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/Reflexion/Reflexion.class.php");
class Xml{

	private $doc		= null;

	private $raiz		= null;

	private $campos		= null;
	
	private $mensaje	= "";
	
	private $atributos_extra	= null;
	
	private $alerta		= false;
	
	private $nombre_nodo = "";
	
	private $funciones_campos = null;
	
	public function setFuncionesCampos($funciones_campos){
		$this->funciones_campos = $funciones_campos;
	}
	
	public function setAtributosExtra($atributos){
		$this->atributos_extra = $atributos;
	}
	
	public function setMensaje($mensaje){
		$this->mensaje = $mensaje;
	}

	public function getCampos(){
		return $this->campos;
	}

	public function setCampos( $campos = array()){
		$this->campos = $campos;
	}
		
	public function setRaiz($nueva_raiz){
		$this->raiz = $nueva_raiz;
	}
	
	
	public function getRaiz(){
		return $this->raiz;
	}
	
	public function setNombreNodo($nombre){
		$this->nombre_nodo = $nombre;
	}
	

	public function __construct(){
		$this->doc	= new DOMDocument("1.0","iso-8859-1");		
		
	}
	
	public function addRoot($OK = 1){
		$nodo = $this->doc->createElement("RAIZ");
		$nodo->setAttribute("OK",$OK);
		$nodo->setAttribute("MENSAJE",utf8_encode($this->mensaje));
		$this->raiz 		= $this->doc->appendChild($nodo);		
	}
	
	//Funcion para probar
	public function createRaiz($elemento,$propiedades){
		$nodo = $this->doc->createElement($elemento);
		if(is_array($propiedades)){
			foreach ($propiedades as $propiedad) {
				$nodo->setAttribute(key($propiedad),$propiedad[key($propiedad)]);
			}
		}
		$this->raiz = $this->doc->appendChild($nodo);	
	}
	
	public function addTo($nodo_padre, $nodo_agregar){
		$nodo_padre->appendChild($nodo_agregar);
	}

	public function createNodo($elemento,$propiedades){
		$nodo = $this->doc->createElement($elemento);
		if(is_array($propiedades)){
			foreach ($propiedades as $propiedad) {
				$nodo->setAttribute(key($propiedad),$propiedad[key($propiedad)]);
			}
		}
		return $nodo;
	}

	public function createElemento($nombre,$contenido = ""){
		return $this->doc->createElement($nombre,$contenido);
	}
	
	public function fullElementos( $nodo_append,$registros ){
		
		foreach ($registros as $registro) {
			$claves = array();
			$item 	= $this->doc->createElement(!$this->nombre_nodo?"NODO":$this->nombre_nodo);

			if ($this->campos == null) {
				$claves = array_keys($registro);
			}
			else {
				$claves	= $this->campos;
			}

			foreach ($claves as $clave) {
				$item->appendChild($this->createElemento(	utf8_encode($clave),
															utf8_encode($registro[$clave])));
			}
			$nodo_append->appendChild($item);
		}
		
	}



	public function fullNodos($nodo_append,$registros){

		foreach ($registros as $registro) {
			$claves 	= array();
			$nodo = $this->doc->createElement("NODO");

			if ($this->campos == null) {
				$claves = array_keys($registro);
			}
			else {
				$claves	= $this->campos;
			}

			foreach ($claves as $clave) {
				if($clave == "adicional"){
					$this->fullNodos($nodo,$registro[$clave]);
				}
				else {
					if($this->funciones_campos){
						$valor = "";
						if(array_key_exists($clave,$this->funciones_campos)){
							if(isset($this->funciones_campos[$clave]['argumentos'])){
								$arreglo_argumentos   = $this->funciones_campos[$clave]['argumentos'];
								$argumentos = is_array($arreglo_argumentos) ? 
												array_unshift($arreglo_argumentos,$registro[$clave]):
												array($registro[$clave],$arreglo_argumentos);
								
							}
							else {
								$argumentos = $registro[$clave];
							}
							
											
							 
							$valor = $this->executeFuncion(	$this->funciones_campos[$clave]['clase'],
															$this->funciones_campos[$clave]['funcion'],
															$argumentos);
						}
						else {
							$valor = $registro[$clave];
						}
						$nodo->setAttribute(utf8_encode($clave),
											utf8_encode($valor));
					}
					else {
						$nodo->setAttribute(utf8_encode($clave),
											utf8_encode($registro[$clave]));

					}
				}
			}
			$nodo_append->appendChild($nodo);
		}
		return $nodo;
	}

	public function executeFuncion($clase,$funcion,$argummentos){
		return Reflexion::hasMethod($clase,$funcion,$argummentos);
	}

	
	public function addAtributos($registro,$nodo_append){
		$claves = array_keys($registro);
		foreach ($claves as $clave) {
			$nodo_append->setAttribute(utf8_encode(strtoupper($clave)),
			utf8_encode($registro[$clave]));

		}
	}

	public function showAlerta($alerta){
		$this->alerta = $alerta;
	}
	
	public function getlistado( $registros = array(),
								$mensaje = ""){
		try{
			$this->setMensaje($mensaje);
			$this->addRoot();
			
			if($registros){
				$this->fullNodos($this->raiz,
								 $registros);
			}
			else {
				if($this->alerta){
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_26,
												ErrorConstantes::$int_error_xml_26,
												LoggerConstantes::$LOG_NIVEL_WARNING);
				}
			}
		}catch (ExcepcionSistema $e){
			$e->muestraError();
		}
		return $this->doc->saveXML();
	}


	public function addMensajeError($mensaje_error, $alerta = false){
		$this->addRoot(0);		
		$this->raiz->setAttribute("MENSAJE",utf8_encode($mensaje_error));
		if($alerta){
			$this->raiz->setAttribute("ALERTA",1);
		}
		if($this->atributos_extra){
			$this->addAtributos($this->atributos_extra,utf8_encode($this->raiz));
		}
		return $this->doc->saveXML();
	}
	
	public function addMensaje($mensaje, $alerta = false){
		$this->addRoot(1);
		$this->raiz->setAttribute("MENSAJE",utf8_encode($mensaje));
		if($alerta){
			$this->raiz->setAttribute("ALERTA",1);
		}
		if($this->atributos_extra){
			$this->addAtributos($this->atributos_extra,$this->raiz);
		}
		return $this->doc->saveXML();
	}
	
	public function getXML(){
		return $this->doc->saveXML();
	}
	
	
	public function addMensajeWarning($mensaje, $alerta = false){
		$this->addRoot(1);
		$this->raiz->setAttribute("MENSAJE",utf8_encode($mensaje));
		if($alerta){
			$this->raiz->setAttribute("ALERTA",1);
		}
		if($this->atributos_extra){
			$this->addAtributos($this->atributos_extra,$this->raiz);
		}
		return $this->doc->saveXML();
	}
	
	public function getListadoJson($registros = array()){
		//var_dump($registros);
		//$registros = json_encode($registros);
		
		if($registros === false){
			$registros = json_encode("Error en sentencia SQL en listado: ".$_SERVER["PHP_SELF"]);
		}
		else{
			
			$respuesta = new stdClass();
			$respuesta->page    = 0;
			$respuesta->total   = 0;
			$respuesta->records = 0;
			$i=0;
			//var_dump($registros);
			while( !empty($registros[$i]) ){
	
				$arreglo_reg = array();
	
				foreach($registros[$i] AS $key=>$valor){
					$arreglo_reg[] = utf8_encode($valor);
				}
	
	
				//$respuesta->rows[$i]['id']=$fila["idCliente"];
				//$respuesta->rows[$i]['cell']=array($fila["idCliente"],utf8_encode($fila["nombre"]),utf8_encode($fila["direccion"]),$fila["telefono"],$fila["email"]);
				$respuesta->rows[$i]['id'] = $arreglo_reg[0];
				$respuesta->rows[$i]['cell'] = $arreglo_reg;
				$i++;

			}
			
			$registros = json_encode($respuesta);
			
		}//cierra else
		
		return $registros;
	
	}
	
	
	public function getListadoJsonOrdinario($registros = array()){
		
		if($registros === false){
			$registros = json_encode("Error en sentencia SQL en listado: ".$_SERVER["PHP_SELF"]);
		}
		else{
			$respuesta = array();
			$i=0;
			
			foreach($registros AS $fila){
				
				foreach($fila AS $key=>$valor){
					$temp[$key] = utf8_encode($valor);
				}
				
				$respuesta["nodos"][] = $temp;
			}
			
			$registros = json_encode($respuesta);
			
		}//cierra else
		
		return $registros;
		
	}
	
	
	public function getSelectHtml($registros = array(), $id_select = ""){
		
		if($registros === false){
			$html = json_encode("Error en sentencia SQL en listado: ".$_SERVER["PHP_SELF"]);
		}
		else{
			if($id_select == "")
				$html = "<select>";
			else
				$html = "<select id='$id_select' name='$id_select'>";
				
			$i = 0;
			//var_dump($registros);
			while(!empty($registros[$i])){
				
				$html .= "<option value='". utf8_encode($registros[$i]["id"]) ."'> ". utf8_encode($registros[$i]["nombre"]) . "</option>";
				
				$i++;
			}
			
			$html .= "</select>";
			
		}
		
		return $html;
	
	}
	


}



?>