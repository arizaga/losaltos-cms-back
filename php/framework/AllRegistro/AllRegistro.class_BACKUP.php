<?php
	require_once("../../framework/AllRegistro/AllRegistroConstantes.class.php");
	require_once("../../framework/Request/Request.class.php");
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/MensajesConstantes.class.php");
	require_once("../../framework/Redirection/Redirection.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/Replace/Replace.class.php");
	require_once("../../framework/Imagen/Imagen.class.php");
	require_once("../../framework/Cadena/Cadena.class.php");
	require_once("../../framework/Historial/Historial.class.php");
	
	class AllRegistro {
		
		private $nombre_objeto 			= "";	
		
		private $accion					= "";
		
		private $instancia				= null;
		
		private $cascada				= false;
		
		private $_registros_cascada  			= "registros_cascada";
		
		private $_delete_registros_secundarios	= "delete_secundarios";
		
		private $showId					= false;
		
		private $campos_imagen			= "";
		
		private $continua_flujo			= false;
		
		private $prohibe_accion			= array();
		
		private $id_registro			= 0;
		
		private $multiregistro			= FALSE;
		
		private $historial			= FALSE;
		
		private $campo_historial		= "";
		
		private $accion_historial		= "";
		
		private $modulo_historial		= "";
		
		private $objeto_historial		= null;
		
		private $opcion				= "xml";
		
		public function setOpcion($var){
			$this->opcion = $var;
		}
		
		public function setMultiRegistro($multiregistro,$registroprincipal){			
			$this->instancia->setMultiRegistro($multiregistro,$registro_principal);
			$this->multiregistro = TRUE;
			
		}
		public function setCamposImagen($campos_imagen){
			$this->campos_imagen = $campos_imagen;
		}
		
		public function updateObjeto(){
			$this->instancia->updateObjeto($this->id_registro);
		}
		
		public function banAction($acciones){
			$this->prohibe_accion	= $acciones;
		}
		
		public function getInstancia(){
			return $this->instancia;
		}
		
		public function continueFlujo(){
			$this->continua_flujo = true;
		}
		
		public function ShowId(){
			$this->showId = true;
		}
		
		public function setValoresCascada($tabla_secundaria, $values_secundarios = ""){
			$this->cascada 				= true;
			$this->instancia->setDatosTablaSecundaria($tabla_secundaria,$values_secundarios);
		}	
		
		public function addCamposTabla($campos){
			$this->instancia->$campos_tabla = $this->instancia->addCamposTabla($campos,$this->instancia->$campos_tabla);
		}
		
		public function deleteCamposTabla($campos){
			$this->instancia->$campos_tabla = $this->instancia->deleteCamposTabla($campos,$this->instancia->$campos_tabla);
		}
		
		public function addCamposTablaSecundaria($campos){
			$this->instancia->campos_secundarios = $this->instancia->addCamposTabla($campos,$this->instancia->campos_secundarios);
		}
		
		public function deleteCamposTablaSecundaria($campos){
			$this->instancia->campos_secundarios = $this->instancia->deleteCamposTabla($campos,$this->instancia->campos_secundarios);
		}

		public function setHistorial(	$modulo_historial = "",
										$campo_historial = "",
										$accion_historial = ""){
			$this->historial 		= TRUE;
			$this->accion_historial = $accion_historial;
			$this->campo_historial	= $campo_historial;
			$this->modulo_historial = $modulo_historial;
			$this->objeto_historial	= new Historial();
		}
		
		public function __construct($objeto){
			$this->nombre_objeto 	= $objeto;		
			$this->accion			= Request::getParametro(AllRegistroConstantes::$accion);
			$this->id_registro		= Request::getParametro("id_".strtolower($this->nombre_objeto));		
			$this->instancia 		= new $this->nombre_objeto($this->id_registro);
			
		}
		
		public function allowAccion(){
			$regreso = false;
			if(in_array($this->accion,$this->prohibe_accion)){
				$regreso = true;
			}
			return $regreso;		
		}
		
		public function getSQL($caso){
			switch ($caso){
				case $this->_registros_cascada:
					$sql = "SELECT @ca
									FROM @ts AS a INNER JOIN @tj AS b
										 ON (a.id_@tj = b.id_@tj)
									WHERE b.id_@ta = @ir";
					break;
					
					
				case $this->_delete_registros_secundarios:
					$sql = "DELETE FROM @ts
							WHERE id_@ta = @id";
					break;
					
					
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
		}
		
		public function makeOperaciones(){
			$url 	= "";
			$OK		 = false;
			try{
				if($this->allowAccion()){
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_46,
												ErrorConstantes::$int_error_xml_46,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}		
				switch ($this->accion){
		
					case AllRegistroConstantes::$ver:
						$OK = $this->seeRegistro();
						break;
		
					case AllRegistroConstantes::$agregar:
						$OK = $this->addRegistro();
						break;
		
		
					case AllRegistroConstantes::$editar:
						$OK = $this->editRegistro();
						break;
		
					case AllRegistroConstantes::$eliminar:
						$OK = $this->deleteRegistro();
						break;
						
					case AllRegistroConstantes::$eliminacion_logica:
						$OK = $this->deleteLogica();
						break;
		
					default:
						$url = CoreConstantes::$ruta_pagina . CoreConstantes::$pagina_error;
						$redirecciona = new Redirection( $url );
						Request::setParametro("error",MensajesConstantes::$msg_03);
						$redirecciona->addParametros();
						$redirecciona->executeRedireccion();
						break;
				}
			}catch (ExcepcionSistema $e){
				$e->muestraError();		
			}
			return $OK;
		}
		
		public function seeRegistro(){
			$tabla_join = "";		
			$sql		= "";
			$busqueda 	= array();
			$sustituto	= array();
			$xml		= new Xml();
			try{
				if($this->instancia->existRegistro()){
					
					if($this->cascada){
						$tabla_join = substr(strrchr( $this->instancia->tabla_secundaria , "_"),1);								
						$busqueda 	= array(	"@ts","@tj","@ta","@ir","@ca");
						$sustituto	= array(	$this->instancia->tabla_secundaria,
												$tabla_join,
												$this->instancia->getNombreTabla(),
												$this->instancia->id_registro,
												implode(",",$this->instancia->campos_secundarios));
												
						$sql = Replace::replaceDatos(	$busqueda,
														$sustituto,
														$this->getSQL($this->_registros_cascada));
						
						$registros_adicionales = $this->instancia->getAll($sql);
						if($registros_adicionales){
							$this->instancia->registro['adicional'] = $registros_adicionales;	
						}
						
					}			
					echo $xml->getlistado( array($this->instancia->registro));
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_20,
												ErrorConstantes::$int_error_xml_20,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
	
				}
			}catch (ExcepcionSistema $e){
				$e->muestraError();		
			}
		}
		
		
		
		public function addRegistro(){
			$xml	= new Xml();
			$OK		= false;
			$this->instancia->startTransaccion();
			try{			
				if($this->cascada){				
					$OK = $this->instancia->insertRegistroCascada();				
				}
				else{
					$OK = $this->instancia->insertRegistro();
				}
				
				$this->id_registro 	=  $this->multiregistro ? 
											$this->instancia->getIdMultiRegistro() :
											$this->instancia->getUltimoId();
				
				if($this->showId && $OK){
					$xml->setAtributosExtra(array("id"=>$this->id_registro));				
				}
				if( $OK ){
					
					if($this->campos_imagen){
						$this->instancia->id_registro = $this->id_registro;
						$imagen = new Imagen($this->campos_imagen,$this->instancia);					
						$imagen->switchOpcion();
					}
					$this->historial ? $this->addHistorial(): "" ;
						
						
					if(!$this->continua_flujo){ 
						echo $xml->addMensaje(		MensajesConstantes::$msg_01,true);
					}
					
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_21,
												ErrorConstantes::$int_error_xml_21,
												LoggerConstantes::$LOG_NIVEL_CRITICO);				
				}
			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false); 
				$e->muestraError();
			}
			$this->instancia->completeTransaccion();
			var_dump($OK);
			var_dump($this->id_registro); /*Este hay que meterlo a un json, validar el retorno segun la opcion en echo xml, continua_flujo*/
			return $OK;
		}
		
		public function editRegistro(){
			$values 	= null;
			$busqueda	= null;
			$sustituto	= null;
			$xml		= new Xml();
			$imagen		= null;
			$OK			= FALSE;
			
			$this->instancia->startTransaccion();
			try{
				
				if($this->instancia->existRegistro() || $this->instancia->existMultiregistro()){
			
					$values = 	$this->instancia->campos_request;
					unset($values["id_{$this->instancia->nombre_tabla}"]);
					
					if($this->campos_imagen){
						$imagen = $this->setObjetoImagen();
					}
					$OK = $this->instancia->setRegistroCampos($values);
	
					if($this->cascada){
	
						if($this->instancia->values_secundarios){
							$busqueda 	= array(	"@ts","@ta","@id");
							$sustituto	= array(	$this->instancia->tabla_secundaria,
													$this->instancia->getNombreTabla(),
													$this->instancia->id_registro);
	
							$sql = Replace::replaceDatos(	$busqueda,
															$sustituto,
															$this->getSQL($this->_delete_registros_secundarios));
	
	
							$ELIMINADO = $this->instancia->execute($sql);
	
							if($ELIMINADO){							
								$this->deleteCamposTablaSecundaria("id_{$this->instancia->tabla_secundaria}");
								$cadena_secundaria 			= implode(",",$this->instancia->campos_secundarios);
	
	
								$sql_tabla_secundaria	 	= $this->instancia->generateInsertCascada(
																Cadena::getCadenaValuesCascada(
																	$this->instancia->values_secundarios,
																	$this->instancia->id_registro),
																	$cadena_secundaria,
																	$this->instancia->tabla_secundaria);
	
	
								$OK = $this->instancia->execute($sql_tabla_secundaria);
							}
							else {
								throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_16,
															ErrorConstantes::$int_error_xml_16,
															LoggerConstantes::$LOG_NIVEL_CRITICO);
							}
						}
						else {
							throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_17,
														ErrorConstantes::$int_error_xml_17,
														LoggerConstantes::$LOG_NIVEL_CRITICO);
	
						}
					}
					if($this->showId && $OK){
						$this->id_registro 	= $this->instancia->id_registro;				
						$xml->setAtributosExtra(array("id"=>$this->id_registro));				
					}
					if($OK){
						if($this->campos_imagen){
							$imagen->switchOpcion();						
						}						
						$this->historial ? $this->addHistorial() : "";						 
						if(!$this->continua_flujo){ 
								echo $xml->addMensaje(MensajesConstantes::$msg_05,true);
						}
					}
					else {
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_22,
													ErrorConstantes::$int_error_xml_22,
													LoggerConstantes::$LOG_NIVEL_CRITICO);					
					}
				}
	
			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false); 
				$e->muestraError();
			}
			$this->instancia->completeTransaccion(); 
			return $OK;
		}
	
		public function deleteRegistro(){
			$xml	=	new Xml();		
			$this->instancia->startTransaccion();
			$imagen  = null;
			try{
				if($this->instancia->existRegistro() || $this->instancia->existMultiregistro()){
					
					if($this->campos_imagen){
						$imagen = $this->setObjetoImagen();
					}
					$OK = $this->instancia->deleteRegistro();
		
					if($this->cascada){
						$busqueda 	= array("@ts","@ta","@id");
						$reemplazo 	= array( $this->instancia->tabla_secundaria,
											$this->instancia->nombre_tabla,
											Request::getParametro($this->instancia->llave_primaria));
						$sql = Replace::replaceDatos(	$busqueda,
														$reemplazo,
														$this->getSQL($this->_delete_registros_secundarios));
						$OK = $this->instancia->execute($sql);
					}
		
					if($OK){
						if($this->campos_imagen){
							$imagen->switchOpcion();	
						}
						if(!$this->continua_flujo){					
							echo $xml->addMensaje(MensajesConstantes::$msg_07,true);
						}
					}
					else {
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_23,
													ErrorConstantes::$int_error_xml_23,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
				}		
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_19,
												ErrorConstantes::$int_error_xml_19,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false); 
				$e->muestraError();
			}
			$this->instancia->completeTransaccion(); 
			return $OK;
		}
	
		public function setObjetoImagen(){
			$imagen = null;
			$copias_originales = null;
			if(is_array($this->campos_imagen)){
				foreach ($this->campos_imagen as $campo) {
					$copias_originales[] =  $this->instancia->get($campo);
				}
	
			}
			else {
				$copias_originales[] = $this->instancia->get($this->campos_imagen);
			}
			$imagen = new Imagen($this->campos_imagen,$this->instancia);
			$imagen->setImagenesOriginales($copias_originales);
			return $imagen;
	
		}
		
		
		
		public function deleteLogica(){
			$xml	=	new Xml();		
			$this->instancia->startTransaccion();
			try{
				if($this->instancia->existRegistro()){
					
					$OK = $this->instancia->setEstatus(Request::getParametro("estatus"));
					
					//Fix me pendiente por checar
					/*
					if($this->cascada){
						
					}*/
		
					if($OK){
						
						$this->historial ? $this->addHistorial() : "";	
						
						if(!$this->continua_flujo){					
							echo $xml->addMensaje(MensajesConstantes::$msg_07,true);
						}
					}
					else {
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_43,
													ErrorConstantes::$int_error_xml_43,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_44,
												ErrorConstantes::$int_error_xml_44,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false); 
				$e->muestraError();
			}
			
			$this->instancia->completeTransaccion(); 
			return $OK;		
		}
		
		public function addHistorial(){
			$accion =  !$this->accion_historial?$this->accion:$this->accion_historial;
			$this->updateObjeto();
			$this->objeto_historial->addHistorial(	$accion,
													$this->nombre_objeto,
													$this->instancia->get($this->campo_historial));
		}
	}
?>