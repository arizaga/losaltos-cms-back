<?php

	require_once("../../framework/Mail/Mail.class.php");
	require_once("../../framework/Replace/Replace.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/Archivo/Archivo.class.php");
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/MensajesConstantes.class.php");


	class Recomienda extends Mail {
	
		public $ruta_estructura = "";
		
		public $nombre_de		= "";
		
		public $nombre_para		= "";
		
		public function __construct($nombre_para,$correo_para,$nombre_de,$correo_de,$mensaje){
			
			$to 	= $nombre_para . " <$correo_para>";
			$from	= $nombre_de   . " <$correo_de>";
			$asunto = "�Visita esta p�gina!";
			
			$this->nombre_de		= $nombre_de;
			$this->nombre_para		= $nombre_para;
			$this->ruta_estructura = "../../framework/Recomienda/estructura/estructura_recomienda.php";			
			parent::__construct($to,$asunto,$this->setContenido(htmlentities($mensaje)),$from);			
		}
		
		public function setContenido($mensaje){
			$busqueda 		= array(	"{RUTA_IMAGENES}",
										"{NOMBRE_PAGINA}",
										"{RUTA_PAGINA}",
										"{MENSAJE}",
										"{DE}",
										"{PARA}");								
			$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
										CoreConstantes::$sistema,
										CoreConstantes::$root_pagina,
										$mensaje,
										$this->nombre_de,
										$this->nombre_para);

			$archivo = new Archivo($this->ruta_estructura);
			return  Replace::replaceDatos(		$busqueda,
											$reemplazo,
											$archivo->readArchivo());
		}
		
		public function sendCorreo(){
			$xml = new Xml();
			if($this->sendMail()){
				$xml->addMensaje(MensajesConstantes::$msg_11,true);
			}
			else {
				$xml->addMensajeError(MensajesConstantes::$msg_12,true);
			}
			return $xml->getXML();
		}

	}