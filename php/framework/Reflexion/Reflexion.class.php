<?php
		
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/ErrorConstantes.class.php");
	require_once("../../framework/ExcepcionSistema/ExcepcionSistema.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");	

class Reflexion{
	
	public static function checkPropiedad($clase_objeto,$propiedad){
		$refleccion 			= null;
		$valor					= "";
		$refleccion_propiedad	= null;
		$regresa				= "";
		try{
			
		$refleccion = new ReflectionClass($clase_objeto);
		
			if($refleccion->hasProperty($propiedad)){
				$refleccion_propiedad = $refleccion->getProperty($propiedad);
				$valor = $refleccion_propiedad->getValue(new $clase_objeto());
				if($valor != ""){
					$regresa = $valor;						
				}
			}
		
		}catch (Exception $e){
			$this->instancia->completeTransaccion(false);
			throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_36 . $e->getMessage(),
										ErrorConstantes::$int_error_xml_36,
										LoggerConstantes::$LOG_NIVEL_CRITICO);
		}
		return $regresa;
	}
	
	public static function hasMethod($clase_objeto,$metodo,$argumentos = ""){
		$reflexion 				= null;
		$valor					= "";
		$reflexion_metodo		= null;
		$regresa				= "";
		try{
			
		$reflexion = new ReflectionClass($clase_objeto);
			if($reflexion->hasMethod($metodo)){
				$reflexion_metodo = $reflexion->getMethod($metodo);				
				if(is_array($argumentos)){
					$valor = $reflexion_metodo->invokeArgs(new $clase_objeto(),$argumentos);
				}
				else {
					$valor = $reflexion_metodo->invoke(new $clase_objeto(),$argumentos);	
				}
				
				if($valor != ""){
					$regresa = $valor;						
				}
			}
			else {
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_42 . $metodo,
											ErrorConstantes::$int_error_xml_42,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
		}catch (ExcepcionSistema  $e){
			$e->muestraError();
		}
		return $regresa;		
	}
		
	public static function booleanHasMethod($clase_objeto,$metodo){
		$reflexion 				= null;		
		$regresa				= false;
		try{
			
		$reflexion = new ReflectionClass($clase_objeto);
		
			if($reflexion->hasMethod($metodo)){
				$regresa = true;
			}
			else {
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_42 . $metodo,
											ErrorConstantes::$int_error_xml_42,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}		
		}catch (ExcepcionSistema  $e){
			$e->muestraError();
		}
		return $regresa;		
	}
	
}


?>