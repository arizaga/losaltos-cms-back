<?php
	
	require_once("../../framework/Replace/Replace.class.php");
	require_once("../../framework/Archivo/Archivo.class.php");
	require_once("../../framework/CoreConstantes.class.php");
	require_once("../../framework/Reflexion/Reflexion.class.php");
	require_once("../../framework/Template/Template.class.php");
	require_once("../../framework/Mail/Mail.class.php");
	require_once("../../framework/Xml/Xml.class.php");
	require_once("../../framework/Correo/Correo.class.php");
	

 /**
  * Esta clase depende directamente de la clase Correo ubicada en las
  * clases del proyecto, la cual controla una tabla de la BD
  *
  */
 class Boletin{

 	private $noticia = null;
 	
 	private $mail	= null;
 	
 	private $ruta_estructura = "";
 	
 	private $html			= "";
 	
 	public function getHtml(){
 		return $this->html;
 	}
 	
 		
	public function __construct( $clase_noticias, $estructura = ""){
		$this->noticia 			= $clase_noticias;						
		$this->ruta_estructura	= $estructura ? $estructura :"../../framework/Boletin/estructura/estructura.php";		
	}
	
	
	public function constructEstructura($tag){
		try{
	
			if(Reflexion::booleanHasMethod($this->noticia,"getCamposBoletin")) {
				$campos = $this->noticia->getCamposBoletin();
			}
			else {
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_42 . "getCamposBoletin",
											ErrorConstantes::$int_error_xml_42,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
					
			if(Reflexion::booleanHasMethod($this->noticia,"getRegistrosBoletin")){
				$registros = $this->noticia->getRegistrosBoletin();
			}
			else {
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_42 . "getRegistrosBoletin",
											ErrorConstantes::$int_error_xml_42,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}						
			$busqueda 		= array(	"{RUTA_IMAGENES}",
										"{NOMBRE_PAGINA}",					
										"{LARGO_TUPLA}");								
			$reemplazo		= array(	CoreConstantes::$ruta_pagina .CoreConstantes::$ruta_imagenes_boletin,
										CoreConstantes::$sistema,
										sizeof($registros) * CoreConstantes::$largo_tupla);
			$archivo		= new Archivo($this->ruta_estructura);				
			$template 		= new Template(Replace::replaceDatos($busqueda,$reemplazo,$archivo->readArchivo()));

			
			$template->replaceInFuente(	$template->replaceTag(	$tag,
																$campos,
																$registros),
										$tag);
			$this->html = $template->getRecurso();
		}catch (ExcepcionSistema  $e){
			$e->muestraError();
		}
	}
	
	
	public function sendMails(){
		$contador	= 0;
		$correo 	= new Correo();
		$registros 	= Reflexion::hasMethod($correo,"getCorreos");
		
		foreach ($registros as $registro) {
			$mail = new Mail(	$registro['correo'],
								"Boletin ". CoreConstantes::$sistema,
								$this->html);
			$OK = $mail->sendMail();
			$contador += $OK? 1: 0;
		}
		
		$xml = new Xml();
		return $xml->addMensaje("Correos enviados: ".$contador . "/" .sizeof($registros),true);
	}
	
}
?>