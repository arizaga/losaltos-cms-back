<?php
	require_once( "../../framework/Registro/Registro.class.php");
	require_once( "../../framework/Sincroniza/SincronizaConstantes.class.php");
	require_once( "../../framework/Replace/Replace.class.php");
	require_once( "../../framework/Matriz/Matriz.class.php");
	require_once( "../../clase/Reservacion/Reservacion.class.php");
		
	class Sincroniza { 
		
		
		public $conexion_maestro = "";
		
		public $conexion_esclavo = "";
		
		public $maestro_ini		 = "";
		
		public $esclavo_ini		 = "";		
		
		public $tabla_maestro	 = "";
		
		public $tabla_esclavo	 = "";
		
		public $id_maestro		 = "";
		
		public $id_esclavo		 = "";
		
		public $base_datos_maestro	= "";
		
		public $base_datos_esclavo	= "";
		
		public $diferencia_segundos	= "";
		
		
		function __construct(){			
			$this->conexion_maestro = new Registro(0,"",true);			
			$this->maestro_ini		= "configuracion/sincroniza_local.ini";
			
			$this->conexion_esclavo = new Registro(0,"",true);
			$this->esclavo_ini		= "configuracion/sincroniza_server.ini";						
		}
		
		public function setMaestroIni($ruta_ini){
			$this->maestro_ini = $ruta_ini;
		}
		
		public function setEsclavoIni($ruta_ini){
			$this->esclavo_ini = $ruta_ini;
		}				
		
		function makeConexiones(){
			/* @var $this->conexion_maestro Registro */
			$this->conexion_maestro->setConfiracionIni($this->maestro_ini);
			$this->conexion_esclavo->setConfiracionIni($this->esclavo_ini);			
			
			$this->conexion_maestro->makeConnection();
			$this->conexion_esclavo->makeConnection();		
			
			$datos_maestro = $this->conexion_maestro->getDatosIni();
			$this->base_datos_maestro = $datos_maestro["base_datos"];
			
			$datos_esclavo = $this->conexion_esclavo->getDatosIni();
			$this->base_datos_esclavo = $datos_esclavo["base_datos"];
		}		
		
		function setDatosMaestro($tabla,$campo_id){
			$this->tabla_maestro	= $tabla;
			$this->id_maestro		= $campo_id;
		}
		
		function setDatosEsclavo($tabla,$campo_id){
			$this->tabla_esclavo	= $tabla;
			$this->id_esclavo		= $campo_id;
		}
		
		function getSQL($caso_uso){
			
			switch ($caso_uso) {
				
				case SincronizaConstantes::$ids_bases:
					$sql = "SELECT @i,alias,actualiza
							  FROM @d.@t";					
					break;
								
				case SincronizaConstantes::$registros_insertar:
					$sql = "SELECT @c
							  FROM @d.@t
							 WHERE @i IN ( @l )";					
					break;
				
				case SincronizaConstantes::$registros_completos:
					$sql = "SELECT @c
							  FROM @d.@t";					
					break;
					
					
					
				//{CASE_LISTADO}
				
				default:
					$sql = "SELECT 'Error'";
					break;
			}
			return $sql;
			
		}
		
		function getFullTablaEsclavo(){
			$busqueda 	= array("@c","@d","@t");
			$reemplazo	= array($this->getColumnasEsclavo($this->tabla_esclavo),
								$this->base_datos_esclavo,
								$this->tabla_esclavo );
			return $this->conexion_esclavo->getAll(
					Replace::replaceDatos(	$busqueda,
											$reemplazo,
											$this->getSQL(
												SincronizaConstantes::$registros_completos)));
		}
		
		function getRegistrosMaestro(){			
			$busqueda 	= array("@i","@t","@d");
			$reemplazo 	= array($this->id_maestro,
								$this->tabla_maestro,
								$this->base_datos_maestro);
			return $this->conexion_maestro->getAll(
							Replace::replaceDatos(  $busqueda,
													$reemplazo,
													$this->getSQL(
														SincronizaConstantes::$ids_bases)));
		}		
		
		function getColumnasMaestro($tabla){			
			$columnas = $this->conexion_maestro->getCamposTabla($tabla);				
			unset($columnas[strtoupper($this->id_maestro)]);			
			return implode(",",$columnas);
		}		
		
		function getColumnasEsclavo($tabla){			
			$columnas = $this->conexion_esclavo->getCamposTabla($tabla);				
			unset($columnas[strtoupper($this->id_esclavo)]);			
			return implode(",",$columnas);
		}		
		
		function getRegistrosAInsertar($registros){
			$busqueda 	= array("@c","@d","@t","@i","@l");
			$reemplazo 	= array(	$this->getColumnasMaestro($this->tabla_maestro),
									$this->base_datos_maestro,
									$this->tabla_maestro,
									$this->id_maestro,
									$registros);
			return $this->conexion_maestro->getAll(
						Replace::replaceDatos(
							$busqueda,
							$reemplazo,
							$this->getSQL(
								SincronizaConstantes::$registros_insertar)));						
		}
				
		function getRegistrosEsclavo(){			
			$busqueda 	= array("@i","@t","@d");
			$reemplazo 	= array($this->id_esclavo,
								$this->tabla_esclavo,
								$this->base_datos_esclavo);
			return $this->conexion_esclavo->getAll(
							Replace::replaceDatos(  $busqueda,
													$reemplazo,
													$this->getSQL(
														SincronizaConstantes::$ids_bases)));
		}		
	
		function sincronizaEsclavo($chequeo_especial = false){
			$registros_maestros = $this->getRegistrosMaestro();
			$registros_problema = array();
			
			if($registros_maestros){
				
				$master_id 		= array();
				$master_fechas	= array();
				
				$master_id 		= Matriz::matriz2ArrayIndice($registros_maestros,$this->id_maestro,"alias");
				$master_fechas	= Matriz::matriz2ArrayIndice($registros_maestros,"alias","actualiza");
				
				$registros_esclavos = $this->getRegistrosEsclavo();
				
				$slave_id		= array();
				$slave_fechas 	= array();
				
				if($registros_esclavos){
					$slave_id		= Matriz::matriz2ArrayIndice($registros_esclavos,$this->id_esclavo,"alias");
					$slave_fechas	= Matriz::matriz2ArrayIndice($registros_esclavos,"alias","actualiza");
				}
				
				$this->getDiferenciaTiempoServer();
				
				$in 	= array_diff($master_id, $slave_id);
				$sin 	= implode(",",array_keys($in));
				//Obtiene registros nuevos que no estan en el server			
				if($sin){
					
					$registros_insertar = $this->getRegistrosAInsertar($sin);
					foreach ($registros_insertar as $registro_server){
						$registro_server['actualiza'] = Fecha::addTime2Fecha($registro_server['actualiza'],$this->diferencia_segundos);
						
						//Validar si el registro no choca con otro arriba
						if($chequeo_especial){							
							if(!$this->chequeoEspecial($registro_server)){
								$registro_final[]		 = $registro_server;
							}
							else {
								$registros_problema[] 	 = $registro_server;
							}
						}
						else {
							$registro_final[]			  = $registro_server;	
						}
					}
					$sql = $this->conexion_esclavo->generateInsertCascada(
									$this->conexion_esclavo->getCadenaValuesCascada($registro_final),								
									$this->getColumnasMaestro($this->tabla_maestro),
									$this->base_datos_esclavo.".".$this->tabla_esclavo);				
					//Fix me
					//Comentado para hacer pruebas				
					$this->conexion_esclavo->execute($sql);				
				}
				
				$in 	= array_intersect($master_id,$slave_id);
				$sin 	= trim(implode(",",array_keys($in)),",");			
				
				//Obtiene registros que estan tanto en el server como en la bd local
				//para comparar sus fechas de actualizacion
				if($sin){
					
					$registros_iguales 	= $this->getRegistrosAInsertar($sin);
					//Fix me comentado para pruebas									
					
					foreach ($registros_iguales as $registro) {
						
						$timestamp_igualado = Fecha::addTime2Fecha($registro['actualiza'],$this->diferencia_segundos);
						
						if($timestamp_igualado > $slave_fechas[$registro["alias"]]){
							$registro['actualiza'] = $timestamp_igualado;
							$sql = $this->conexion_esclavo->updatePersonalizaWhere(
										$this->base_datos_esclavo.".".$this->tabla_esclavo,
										$this->conexion_esclavo->getCadenaUpdate($this->tabla_esclavo,$registro),
										"alias = '{$registro['alias']}'");											
								
							$this->conexion_esclavo->execute($sql);	
						}
					}
				}
		}	
		//Limpia la tabla de abajo para bajar la nueva información
			
		$OK = $this->clearTablaMaestro($this->base_datos_maestro.".".$this->tabla_maestro);		
		
			if( $OK ){
					$registros_regreso = $this->getFullTablaEsclavo();
					
					if($registros_regreso){
						$this->getDiferenciaTiempoServer(true);
						foreach ($registros_regreso as $regreso){
							$timestamp_igualado = Fecha::addTime2Fecha($regreso['actualiza'],$this->diferencia_segundos);
							$regreso["actualiza"] = $timestamp_igualado;
							$regreso_final[] = $regreso;
						}
																								
						$sql = $this->conexion_maestro->generateInsertCascada(
										$this->conexion_maestro->getCadenaValuesCascada($regreso_final),								
										$this->getColumnasMaestro($this->tabla_maestro),
										$this->base_datos_maestro.".".$this->tabla_maestro);
						
						$this->conexion_maestro->execute($sql);
					}
			}
			
			//Fix me, en caso de tener problemas con los registros
			if($registros_problema){
				
			}
		}
		
		function clearTablaMaestro($tabla){
			$sql = "TRUNCATE TABLE $tabla";
			return $this->conexion_maestro->execute($sql); 
		}
		
		
		function getDiferenciaTiempoServer($bajada = false){
			$sql = "SELECT NOW() As tiempo";
			//TERMINAR esto
				$hora_local 	= $this->conexion_maestro->getOne($sql);
				$hora_externa	= $this->conexion_esclavo->getOne($sql);		

			if($bajada){
				$this->diferencia_segundos = (strtotime($hora_local) - strtotime($hora_externa));
			}
			else {
				$this->diferencia_segundos = (strtotime($hora_externa) - strtotime($hora_local));			
			}
		}
		
		function chequeoEspecial($registro){
			$reservacion = new Reservacion("",true);
			$reservacion->setConfiracionIni($this->esclavo_ini);
			$reservacion->makeConnection();			
			return $reservacion->haveReservacion( $registro['id_habitacion'],
												  $registro['fecha_inicial'],
												  $registro['fecha_final']);
			
		}
	
	
	
	
	}	
?>