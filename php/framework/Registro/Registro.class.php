<?php	
require_once("../../framework/Matriz/Matriz.class.php");
require_once("../../framework/Regex/Regex.class.php");
require_once("../../framework/Logger/LoggerConstantes.class.php");
require_once("../../framework/ErrorConstantes.class.php");
require_once("../../framework/Replace/Replace.class.php");
require_once("../../framework/Conexion/Conexion.class.php");
require_once("../../framework/ExcepcionSistema/ExcepcionSistema.class.php");
require_once("../../framework/Registro/RegistroConstantes.class.php");

class Registro{


	/**
		 * Variable donde se alamcenan el registro instanciado
		 *
		 * @var array
		 */
	public 	$registro;

	/**
		 * Id del registro que inicializo la tabla
		 *
		 * @var int
		 */
	public 	$id_registro;

	/**
		 * Nombre de la tablap del objeto
		 *
		 * @var string
		 */
	public	$nombre_tabla;

	/**
		 * Campo por el cual se ordena un listado
		 *
		 * @var string
		 */
	public 	$campo_orden;

	/**
		 * Orden que tiene un listado ASC o DESC 
		 *
		 * @var string
		 */
	public	$orden;


	/**
		 * Arreglo de campos de la tabla del objeto inicializado...
		 * tambien puede ser unacadena separada por comas
		 * @var mixed
		 */
	public 	$campos_tabla;


	/**
		 * Metadatos de los campos de la tabla: name,type,max_length
		 *
		 * @var array
		 */
	public	$tipo_datos_campos;

	/**
		 * Mensajes de Error para visualizar con el xml
		 *
		 * @var string
		 */
	public	$mensaje_error;

	/**
		 * Valor que permite que evalue el regex los values en un sql de insert o update
		 * 
		 *
		 * @var boolean
		 */
	public $regex = TRUE;


	/**
		 * Campos de la tabla secundaria para su inserci�n
		 *
		 * @var array
		 */
	public $campos_secundarios;


	/**
		 * Objetos con propiedades de los campos secundarios
		 *
		 * @var array(objects)
		 */
	public $tipo_datos_campos_secundarios;


	public $campos_request		= null;

	public $info_registro		= "info_registro";

	public $tabla_secundaria	= "";

	public $values_secundarios	= null;

	public $llave_primaria		= "";

	private $conexion		= "";

	private $multiregistro		= null;

	private $id_multiregistro	= null;


	//MODIFICADA
	/*
	*	Constructor el cual nos regresa la conexion
	*	para utilizarla en la clase registro
	*/
	public function Registro($id_registro = 0,$sql_especial="",$change = false){
		$conexion 		= Conexion::getInstance();
		$this->conexion	= $conexion->getConexion();
		$this->getInfoRegistro($id_registro,$sql_especial);

		//$this->regex = FALSE;
	}

	/**
		 * Setea el valor del nombre de la tabla
		 *
		 * @param string $tabla
		 */
	public function setTablaPrincipal($tabla){
		$this->nombre_tabla = $tabla;
	}

	public function setMultiRegistro($multiregistro,$id_multiregistro){
		$this->multiregistro 	= $multiregistro;
		$this->id_multiregistro	= $id_multiregistro;
	}

	public function existMultiregistro(){
		return $this->multiregistro ? TRUE:FALSE;
	}

	/**
		 * Setea los campos principales de la tabla
		 * @param array $campos
		 */
	public function setCamposTabla($campos){
		$this->campos_tabla = $campos;
	}

	public function showDebug(){
		$this->conexion->debug = true;
	}
	
	public function getIdMultiRegistro(){
		return $this->id_multiregistro;
	}

	public function addCamposTabla($campos, $arreglo_campos = null){

		if(is_array($campos)){
			foreach ($campos as $campo) {
				$arreglo_campos[] = $campo;
			}
		}
		else {
			$arreglo_campos[] = $campos;
		}
		return $arreglo_campos;
	}

	public function deleteCamposTabla($campos,$arreglo_campos = null){
		if(is_array($campos)){
			foreach ($campos as $campo) {
				unset($arreglo_campos[strtoupper($campos)]);
			}
		}
		else {
			unset($arreglo_campos[strtoupper($campos)]);
		}
		return $arreglo_campos;
	}

	public function setIdRegistro($id_registro){
		$this->id_registro = $id_registro;
	}

	public function setDatosTablaSecundaria( $tabla, $values ){
		$this->values_secundarios				= $values;
		$this->campos_secundarios 				= $this->getCamposTabla($tabla);
		$this->tipo_datos_campos_secundarios 	= $this->getMetaDatos($tabla);
		$this->tabla_secundaria					= $tabla;
	}

	public function instanceObjeto($id_registro,$sql_especial= ""){
		$this->getInfoRegistro($id_registro,$sql_especial);
	}

	public function getSQLRegistro($caso){
		$sql  = "";
		switch ($caso) {
			case $this->info_registro:
				$sql = 	"SELECT *
						   	   FROM @nt
						      WHERE id_@nt = @id";
				break;

			case RegistroConstantes::$registros_multiregistro:
				$sql = 	"SELECT GROUP_CONCAT( DISTINCT CAST( @cs AS CHAR )) AS multilistado
							  FROM @t
							 WHERE @cp = @id
						  GROUP BY @cp";
				break;

			default:
				$sql = "SELECT 'Error'";
				break;
		}
		return $sql;
	}

	public function updateObjeto($id_registro,$sql_especial = ""){
		$this->getInfoRegistro($id_registro,$sql_especial);
	}



	//MODIFICADA
	/**
		 * Funcion que obtiene informacion de un registro 
		 * para inicializar el objeto
		 * @param int $id_registro
		 * @param int $sql_especial(opcional)
		 */

	public function getInfoRegistro( 	$id_registro = 0,
	$sql_especial = ""){

		$busqueda = array();

		if( !$sql_especial && $id_registro ){

			$busqueda 	= array("@nt","@id");
			$reemplazo	= array($this->getNombreTabla(),
			$id_registro);

			$sql =		Replace::replaceDatos(	$busqueda,
			$reemplazo,
			$this->getSQLRegistro($this->info_registro));

		}
		else {
			$sql = $sql_especial;
		}
		if($sql && $id_registro){
			$this->registro = $this->getRegistro($sql);
		}
		if($this->registro){
			$this->id_registro 			= $id_registro;
		}
		else {
			$this->registro = "";
		}

		$this->nombre_tabla 		= $this->getNombreTabla();
		$this->campos_tabla 		= $this->getCamposTabla();
		$this->tipo_datos_campos 	= $this->getMetaDatos();
		$this->getPrimaryKey();
		$this->campos_request		= Matriz::getValoresDesdeArray($this->campos_tabla);
	}


	//MODIFICADA

	/**
		 * Funcion de interface que no define el comportamiento de 
		 * las clases que extiende de esta pero si lo marca
		 * @return string
		 */
	public function getNombreTabla(){
		return "";
	}



	//MODIFICADA
	/**
		 * Funci�n con la cual obtenemos cualquier campo del
		 * registro solicitado
		 * @param string $campo
		 * @return string
		 */
	public function get($campo = "" ){
		if($this->registro){
			$campo_registro = array_key_exists($campo,$this->registro)?
			$this->registro[$campo]:
			"";
		}
		else {
			$campo_registro = "";
		}
		return $campo_registro;
	}

	//MODIFICADA
	/**
		 * Funci�n que comprueba si existe o no un registro
		 * solo necesita haberse inicializado el objeto con getInfoRegistro
		 * @return boolean
		 */
	public function existRegistro(){
		if($this->registro){
			$existencia = TRUE;
		}
		else{
			$existencia = FALSE;
		}
		return $existencia;
	}



	/**
		 * Inserta un registro: 
		 * en caso de no recibir $campos los sustiruye con los campos de la tabla,
		 * en caso de no recibir $values los sustiruye con $_REQUEST, 
		 * en caso de no recibir $tabla los sustiruye con la tabla de la clase hijo, 
		 * 
		 *
		 * @param mixed $campos
		 * @param mixed $values
		 * @param string $tabla
		 * @return boolean
		 */
	public function insertRegistro( $lista_campos ="", $values = "", $tabla=""){
		$matriz_regex 	= null;
		$sql_campos		= null;
		$sql_values		= null;
		$cadena_values	= "";
		try{
			if(!$lista_campos){
				$lista_campos = $this->campos_tabla;
			}
			else {
				$lista_campos = is_array($lista_campos) ? $lista_campos : array($lista_campos);
			}

			if(!$values){
				$values = $_REQUEST;
			}
			else {
				$values 	= is_array($values) ? $values : array($values);
			}

			if(!$tabla){
				$tabla = $this->nombre_tabla;
			}
			//Si hay multiregistro
			if($this->multiregistro != null){
				$multiregistro = is_array($this->multiregistro) ? 
								 	$this->multiregistro :
								 	explode(",",$this->multiregistro);
								 	
				$sql = $this->generateInsertCascada(
									$this->getCadenaValuesCascada($multiregistro,
									$this->id_multiregistro),
									implode(",",$lista_campos),
									$tabla);
			}
			else{
				foreach ($lista_campos as $campo) {

					if(array_key_exists($campo,$values)){
						$sql_campos[] 	= $campo;
						$sql_values[] 	= $values[$campo];
						if(array_key_exists(strtoupper($campo),$this->tipo_datos_campos)){
							$this->tipo_datos_campos[strtoupper($campo)]->value = $values[$campo];
							$matriz_regex[$campo] = $this->tipo_datos_campos[strtoupper($campo)];
						}
					}
				}
				if($sql_values == null){
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_31,
					ErrorConstantes::$int_error_xml_31,
					LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
				if($this->regex){
					$regex = new Regex(array($matriz_regex));
				}
				$cadena_values = $this->getCadenaValues($sql_values);
				$sql = $this->generateInsert(	$cadena_values,
				implode(",",$sql_campos),
				$tabla);
			}



		}catch (ExcepcionSistema $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		//var_dump($sql);

		return  $this->execute($sql);
	}



	/**
		 * Obtiene toda la informacion de los campos de la tabla marcada
		 *
		 * @param string $tabla
		 * @return array(objects)
		 */
	public function getMetaDatos( $tabla = "" ){
		if( !$tabla ){
			$tabla = $this->nombre_tabla;
		}
		return $this->conexion->MetaColumns($tabla);
	}

	public function getPrimaryKey(){
		if($this->tipo_datos_campos){
			foreach ($this->tipo_datos_campos as $campo) {
				if($campo->primary_key){
					$this->llave_primaria = $campo->name;
				}
			}
		}
	}

	function insertRegistroCascada(	)	{
		$id_principal 			= "";
		$registro_regex			= null;
		$matriz_regex			= null;
		$registro_secundario	= null;

		$this->conexion->StartTrans();
		$OK = $this->insertRegistro();

		if( $OK ){
			$id_principal = $this->getUltimoId();

			foreach ( $this->values_secundarios as $registro_secundario ) {

				foreach ($this->campos_secundarios as $campo){
					if( array_key_exists($campo,$registro_secundario) ){
						if(array_key_exists(strtoupper($campo),$this->tipo_datos_campos_secundarios)){
							$this->tipo_datos_campos_secundarios[strtoupper($campo)]->value = $registro_secundario[$campo];
							$registro_regex[] = $this->tipo_datos_campos_secundarios[strtoupper($campo)];
						}
					}

				}
				$matriz_regex[] = $registro_regex;
			}

			if( $this->regex ){
				$regex = new Regex($matriz_regex);
			}

			$sql_secundario = $this->generateInsertCascada(
			$this->getCadenaValuesCascada($this->values_secundarios,$id_principal),
			implode(",",$this->campos_secundarios),
			$this->tabla_secundaria);
			$this->execute($sql_secundario);
		}
		return $this->conexion->CompleteTrans();
	}


	public function getUltimoId($campo = ""){
		if(!$campo){
			$campo = "id_$this->nombre_tabla";
		}

		$sql = "SELECT MAX($campo) AS id
					  FROM {$this->nombre_tabla}";			
		return $this->getOne($sql);
	}


	/**
		 * Esta funcion recibe los parametros de el nombre de la tabla,
		 *	los valores a modificar ordenados de acuerdo al orden en la tabla
		 *	y el id a modificar.
		 * @param string $sql
		 * @param string $tabla
		 * @param array $values
		 * @param string $id
		 * @return boolean
		 */
	function updateRegistro($tabla,$values,$id){
		if(!$sql){
			$sql = $this->generateUpdate($this->getCadenaUpdate($tabla,$values),$tabla,$id);
		}
		//var_dump($sql);
		return  $this->execute($sql);
	}
	/*
	*	Funcion que nos permite eliminar un registro
	*
	*/
	function deleteRegistro( $campo_where = "" , $valor= ""){

		if($this->existMultiregistro()){
			$campo = $campo_principal = "id_" . Cadena::getCadenaAnteriorPosteriorA($this->getNombreTabla(),"_to_");
			$valor = $this->id_multiregistro;
		}
		else{
			$campo 	= $campo_where  ? $campo_where  : "id_$this->nombre_tabla";
			$valor	= $valor		? $valor		: $this->id_registro;
		}

		$sql = "DELETE FROM $this->nombre_tabla
					WHERE $campo = '$valor'";

		return  $this->execute($sql);
	}
	/*
	* Recibe un arreglo de arreglos para los campos que se desean, setear
	* EL array del campo tiene que tener la siguiente estructura:
	* $campos = array(0=>array("status_recibe",0),1=>array("campo",valor));
	*/

	function setRegistroCampos( $values ){
		$sql 	= null;
		$campos	= "";
		try{
			if($this->multiregistro != null){
				$campo_principal = "id_" . Cadena::getCadenaAnteriorPosteriorA($this->getNombreTabla(),"_to_");
				$OK = $this->deleteRegistro($campo_principal,$this->id_multiregistro);
				if(!$OK){
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_47 ,
					ErrorConstantes::$int_error_xml_47,
					LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
				$multiregistro = is_array($this->multiregistro) ? 
								 	$this->multiregistro :
								 	explode(",",$this->multiregistro);

				
				$sql = $this->generateInsertCascada(
												$this->getCadenaValuesCascada($multiregistro,
												$this->id_multiregistro),
												implode(",",$this->campos_tabla),
												$this->getNombreTabla());
			}
			else{
				foreach ($this->campos_tabla as $campo){
					if(array_key_exists($campo,$values)){
						$sql[] = $campo." = '".$values[$campo]."'";
					}
				}
				if($sql){
					$campos = implode(",",$sql);
					$sql = "UPDATE {$this->nombre_tabla}
								   SET $campos 
								 WHERE id_{$this->nombre_tabla} = {$this->id_registro}";
				}
				else{
					$sql = $this->getSQLRegistro("");
				}
			}
			$OK = $this->execute($sql);
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		return $OK;
	}


	/*
	*	Envia un correo a la direccion indicada.
	*	El para acepta elformato de mails para:xxx@sdfsdf.com,xxx@xxx.com
	*/
	function sendMail( $para, $asunto, $mensaje, $cabeceras = NULL){

		if($cabeceras){
			$mensaje = mail($para,$asunto,$mensaje,$cabeceras);
		}
		else {
			$mensaje = mail($para,$asunto,$mensaje);
		}
		return $mensaje;
	}

	function getAssoc($sql){
		return $this->conexion->GetAssoc($sql);

	}
	/*
	*	Genera una cadena sql de insert
	*
	*/
	function generateInsert($values,$campos,$tabla){
		$sql = "INSERT INTO $tabla
					($campos) values($values)";
		return $sql;
	}


	/**
		 * Genera un SQL con varios values para poder ser
		 * ejecutado con 
		 */
	function generateInsertCascada($values,$campos,$tabla){
		return $sql = "INSERT INTO $tabla ($campos) VALUES $values";

	}

	function generateUpdate($campos_organizados,$tabla,$id){
		$sql = "UPDATE $tabla SET $campos_organizados
					WHERE id_$tabla = $id";
		return $sql;
	}


	function updatePersonalizaWhere($tabla,$values,$where){
		$sql = "UPDATE $tabla SET $values
					WHERE $where";
		return $sql;
	}


	/*
	*	Esta funcion genera la cadena necesaria para realizar el seteo SET
	*   en un update en una consulta: "campo = 'valor'"
	*	Los valores a modificar, deben estar ordenados de acuerdo al orden
	*	que tienen en la tabla que se le indica

	*/
	function getCadenaUpdate($tabla,$valores_modificar){
		$arreglo_campos = $this->getCamposTabla($tabla);
		$iteracion = 0;
		$sql="";
		foreach ($arreglo_campos as $campo){

			if($campo != "id_$tabla"){
				$sql[] = "$campo = '$valores_modificar[$campo]'";
			}
			$iteracion ++;
		}
		$sql_final = implode(",",$sql);

		return $sql_final;
	}

	function getCadenaCampos($campos){
		return implode(",",$campos);;
	}

	function getCadenaValues($values){

		foreach ($values as $valor){
			if($valor){
				$arreglo_valores[] = "'".$valor."'";
			}
                        elseif($valor == '0'){
                                $arreglo_valores[] = "'0'";
                        }
			else{
				$arreglo_valores[] = "NULL";
			}

		}
		$arreglo_final = implode(",",$arreglo_valores);
		return $arreglo_final;
	}

	/**
		 * Funcion que genera los registros que se le manden en forma de 
		 * (camp1,camp2,...) (camp1,camp2) ....
		 * @param array $registros
		 * @return string
		 */
	function getCadenaValuesCascada( $registros, $id_cascada = ""){
		$arreglo_valores 	= null;
		$registro		= null;

		foreach ( $registros as $registro ){

			if(is_array($registro)){
				//agregando el id del registro cascada al inicio
				$id_cascada ? array_unshift($registro,$id_cascada): "";

				foreach ($registro as $campo){
					$arreglo_valores[] = "'".$campo."'";
				}
				$cadena_values[] = "(".implode(",",$arreglo_valores).")";
				$arreglo_valores = null;
			}
			else {

				$cadena_values[] = $id_cascada ? "("."'$id_cascada'".","."'$registro'".")" : "($registro)";
			}
		}
		//var_dump($cadena_values);
		return implode(",",$cadena_values);
	}

	function getRegistroPorId($id,$tabla){
		$sql = "SELECT *
			          FROM $tabla
			         WHERE id_".$tabla."= $id";

		return $this->conexion->GetRow($sql);
	}
	//MODIFICADA
	function execute($sql){
		try{
			//$this->startTransaccion();
			$registros = $this->conexion->Execute($sql);
			if($registros === false){
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_45 . $this->conexion->ErrorMsg(),
				ErrorConstantes::$int_error_xml_45,
				LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		return $registros;//$this->completeTransaccion();
	}

	//MODIFICADA
	function getRegistro($sql){
		try{
			//$this->startTransaccion();
			$this->fetchMode();
			$registros = $this->conexion->GetRow($sql);
			if($registros === false){
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_45 . $this->conexion->ErrorMsg(),
				ErrorConstantes::$int_error_xml_45,
				LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		return $registros;
	}
        
        function getJson($sql){
		try{
			$this->fetchMode();
                        
                        $registros = $this->conexion->GetAll($sql);
                        //$registros = $registros->getRows();
                        
                        return $registros;
			
                        
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		//return $registros;
	}
        
        
	function getSp($sql){
		try{
			$this->fetchMode();
                        
                        $registros = $this->conexion->Execute($sql);
                        //var_dump($registros);
			if($registros === false){
                                
                            $xml_root = new SimpleXMLElement('<?xml version="1.0" encoding="iso-8859-1"?><RAIZ OK="0" ALERTA="1" MENSAJE="Error al ejecutar Store Procedure..."></RAIZ>');
                            
			}
                        else{
                            $xml_root = new SimpleXMLElement('<?xml version="1.0" encoding="iso-8859-1"?><RAIZ OK="0" ALERTA="1" MENSAJE="Ejecucion correcta..."></RAIZ>');
                        }
                        echo $xml_root->asXML();
                        
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		//return $registros;
	}
        
        
	//MODIFICADA
	function getAll( $sql ){
		try{
			//$this->startTransaccion();
			$this->fetchMode();
			$registros = $this->conexion->GetAll($sql);
			if($registros === false){
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_45 . $this->conexion->ErrorMsg(),
				ErrorConstantes::$int_error_xml_45,
				LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
		}catch (ExcepcionSistema  $e){
			$this->completeTransaccion(false);
			$e->muestraError();
		}
		return $registros;
	}

	//MODIFICADA
	function fetchMode(){
		return $this->conexion->SetFetchMode(ADODB_FETCH_ASSOC);
	}


	//MODIFICADA
	/*
	*	Esta funcion recibe  el nombre de la tabla
	*	y un parametro llamado cadena para indicar si
	*	queremos los campos como cadena o en un array
	*/
	public function getCamposTabla( $tabla = "", $cadena = FALSE){

		if(!$tabla){
			$tabla = $this->nombre_tabla;
		}
		$campos = $this->conexion->MetaColumnNames($tabla);
		if($cadena){
			$regresa_campos = implode(",",$campos);
		}
		else{
			$regresa_campos = $campos;
		}
		return $regresa_campos;
	}

	function prepareSQL($sql){
		return $this->conexion->Prepare($sql);
	}


	function showFormatoFecha($fecha = NULL,$formato = NULL, $caracter = NULL){
		setlocale(LC_ALL,"spanish");
		switch ($formato){
			case "fecha_caracter":
				$fecha_formateada = strftime("%d$caracter",strtotime($fecha)).
				ucfirst(strftime("%B$caracter",strtotime($fecha))).
				strftime("%Y",strtotime($fecha));
				break;

			case "fecha_hora_modificar":
				$fecha_formateada = strftime("%d/%m/%Y - %H:%M Hrs.", strtotime($fecha));
				break;

			case "fecha_corta":
				break;

			case "fecha_larga":
				$fecha_formateada = strftime("%A %d de %B del %Y",strtotime($fecha));
				break;
			case "fecha_larga_hora":
				$fecha_formateada = ucfirst(strftime("%A ", strtotime($fecha))).
				strftime("%d de ",      strtotime($fecha)).
				ucfirst(strftime("%B ",strtotime($fecha))).
				strftime("del %Y  / %H:%M Hrs.",strtotime($fecha));
				break;

			case "fecha_mexicana":
				$fecha_formateada = strftime("%d/%m/%Y",strtotime($fecha));
				break;
			case "fecha_mexicana_hora":
				$fecha_formateada = strftime("%d/%m/%Y - %H:%M Hrs.",strtotime($fecha));
				break;
			case "hora_corta":
				$fecha_formateada = date("H:i \H\\r\s.",strtotime($fecha));
				break;
			case "dia_semana":
				$fecha_formateada = strftime("%A",strtotime($fecha));
				break;
			default:
				$fecha_formateada = $fecha;
				break;
		}
		return $fecha_formateada;
	}




	function getCamposFromSQL($sql){

		$select = "SELECT";
		$from	= "FROM";

		$pos_inicial 	= stripos( 	$sql,
		$select);

		$pos_final		= stripos( 	$sql,
		$from);

		$pos_final = $pos_final ? $pos_final : strlen($sql);

		$campos = substr(	$sql,
		($pos_inicial + strlen($select)) ,
		($pos_final - ($pos_inicial + strlen($select))));
		if(strrpos($campos,"*")){
			$campos = FALSE;
		}
		else {
			$campos = explode( 	",",
			$campos);
		}


		if($campos){
			foreach ($campos as $campo){

				if(stripos($campo, " as ") !== FALSE){

					$campos_listado[] =  trim(substr($campo,stripos($campo, " as ") + strlen(" as "))) ;
				}
				else {
					$campos_listado[] = trim($campo);
				}
			}


		}
		else {
			//si encontro un *
			foreach ($this->campos_tabla as $campo) {
				$campos_listado[] =  $campo ;
			}

		}
		return implode(",",$campos_listado);
	}




	function getOne($sql){
		$campo = $this->getCamposFromSQL($sql);
		$registro = $this->getRegistro($sql);
		return $registro ? $registro[$campo] : "";

	}


	function searchInMatriz($valor,$matriz){
		for ( $i = 0; $i < count($matriz); $i++ ){
			$valor_encontrado = strstr($elemento,$valor);
			$valor_encontrado ? $i=count($matriz) : "";

		}
		return $valor_encontrado;
	}


	public function startTransaccion(){
		$this->conexion->StartTrans();
	}

	public function completeTransaccion($accion = true){
		return $this->conexion->CompleteTrans($accion);
	}

	public function setEstatus($valor_estatus,$campo_id = ""){
		$field_id = $campo_id? $campo_id : $this->llave_primaria;
		$sql = "UPDATE {$this->nombre_tabla}
					   SET estatus = '$valor_estatus' 
					 WHERE {$field_id} = '{$this->id_registro}'";				//var_dump($sql);
		return $this->execute($sql);
	}


}


?>