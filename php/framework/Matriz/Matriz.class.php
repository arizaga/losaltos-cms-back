<?php


class Matriz{
	
	public static function addToArray( $arreglo , $valor = ""){			
		
		if(is_array($arreglo)){
			if(is_array($valor)){
				$arreglo_final = array_merge($arreglo,$valor);				
			}
			else {
				$arreglo_final = array_merge($arreglo ,array($valor));
			}
		}
		else {
			$arreglo_final = $arreglo;
		}
		
		return $arreglo_final;
	}
	
	public static function constructMultiregistro(	$registro_principal 		= "",		
													$registros_multiplicadores 	= array()){
		$nueva_matriz 	= array();
		$nuevo_registro = array();
		if(is_array($registros_multiplicadores)){									
			foreach ($registros_multiplicadores as $registro){
				$nuevo_registro[] = $registro_principal ."," .$registro;
			}
			$nueva_matriz[] = $nuevo_registro;
		}
		return $nueva_matriz;
	}
	
	public static function deleteToArray($arreglo,$valor= ""){
		if(is_array($arreglo)){
			if(is_array($valor)){
				foreach ($valor as $elemento) {
					if(in_array($elemento,$arreglo)){
						unset($arreglo[$elemento]);
					}
				}
				$arreglo_final = $arreglo;
			}
			else {
				if(in_array($valor,$arreglo)){
					unset($arreglo[$valor]);
				}
				$arreglo_final = $arreglo;
			}
		}
		else {
			$arreglo_final = $arreglo;
		}
	}
	
		//MODIFICADA
		/**
		 * Funcion que regresa ciertos valores en un arreglo
		 * rescatados desde otro arreglo ya sea $_POST, $_GET o
		 * $_REQUEST por default esta el $_REQUEST
		 * @param mixed $valores
		 * @param array $arreglo
		 * @return mixed
		 */
		public static function getValoresDesdeArray( 	$valores="",
		$arreglo = "" ){
			$arreglo_pedido  	= null;
			$arreglo = $arreglo == ""?
			$_REQUEST
			:$arreglo;

			if(is_array($valores)){

				foreach ($valores as $valor) {
					if(array_key_exists( $valor, $arreglo )){
						$arreglo_pedido[$valor] = $arreglo[$valor];
					}
				}
			}
			else {
				if($valores){
					$arreglo_pedido = array_key_exists($valores,$arreglo)?
												$arreglo[$valores]:
												"";	
				}
				else {
					$arreglo_pedido = "";
				}
				
			}
			return $arreglo_pedido;
		}
		
		function reduceArray($matriz,$campos){
			if(is_array($campos)){
				foreach ($matriz as $valor) {
					foreach ($campos as $campo){
						$matriz_campos[$campo] = $valor[$campo];
					}
					$nueva_matriz[] = $matriz_campos;
				}
			}
			else {
				foreach ($matriz as $valor) {
					$nueva_matriz[] = $valor[$campos];
				}
			}
			return $nueva_matriz;
		}
		
		public function buildArray($matriz,$campos){
			$matriz_final  = array();
			$matriz_actual = array();
			$arreglo_actual = array();
			foreach ($matriz as $elemento) {
				$matriz_final[] = array_combine($campos,$elemento);
			}
			return $matriz_final;
		}

		public function field2Array($campo){
			return explode(",",$campo);
		}
		
		public function array2Matriz($arreglo){
			$matriz_final = array();
			foreach ($arreglo as $elemento) {
				$matriz_final[]  = array($elemento);
			}
			return $matriz_final;
		}
		
		public function matriz2ArrayIndice($matriz,$indice,$indice_valor){
			$matriz_final = array();
			foreach ($matriz as $elemento) {
				if(array_key_exists($indice,$elemento)){
					if(array_key_exists($indice_valor,$elemento)){
						$matriz_final[$elemento[$indice]] = $elemento[$indice_valor];	
					}					
				}			
			}
			return $matriz_final;
		}
		
		
		/**
		 * Enter description here...
		 *
		 * @param unknown_type $arreglo
		 * @param unknown_type $llave
		 * @return unknown
		 */
		public function isLlave($arreglo,$llave){
			$regresa = false;
			if(array_key_exists($llave,$arreglo)){
				$regresa = true;
			}
			return $regresa;
		}
		
		
		/**
		 * Crea una matriz en base al numero de valores arrojados por los
		 * distintos arrays que lleguen
		 * @param mixed $array Recibe un numero infinito de arrays
		 * @return Matriz matriz en la forma array(array(),array(),array())
		 */
		public function buildMatrizFromArrays(){
			
			$matriz_regreso 	= array();
			$arreglo_temporal	= array();
			$numero_parametros 	= func_num_args();
			$listado_argumentos = func_get_args();
			$numero_campos		= count(func_get_arg(0));
						
			for($i =0; $i < $numero_campos ; $i++ ){
				for($j = 0; $j < $numero_parametros; $j++ ){
					 $arreglo_temporal[] = $listado_argumentos[$j][$i];  
				}
				$matriz_regreso[] = $arreglo_temporal;
				$arreglo_temporal = array();
			}
			return $matriz_regreso;
		}
		

}

?>