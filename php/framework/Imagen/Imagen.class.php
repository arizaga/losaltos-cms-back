<?php

	require_once("../../framework/Request/Request.class.php");
	require_once("../../framework/AllRegistro/AllRegistroConstantes.class.php");
	require_once("../../framework/Replace/Replace.class.php");
	require_once("../../framework/Registro/Registro.class.php");
	require_once("../../framework/ExcepcionSistema/ExcepcionSistema.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/Matriz/Matriz.class.php");
	require_once("../../framework/utils/Thumbs.class.php");
	require_once("../../framework/ErrorConstantes.class.php");
	require_once("../../framework/Logger/LoggerConstantes.class.php");
	require_once("../../framework/Imagen/ImagenConstantes.class.php");
	require_once("../../framework/Archivo/Archivo.class.php");
	
	


	class Imagen {
		
		private $campos_imagen 			= null;		
		private $instancia				= null;
		private $nombre_thumbs			= null;
		private $_listado_imagenes		= "listado_imagenes";
		private $Thumb					= null;
		private $campo_actual			= "";
		private $imagenes_originales	= null;
		private $imagenes_modificar		= null;
		private $imagenes_viejas		= null;		
		private $directorio_imagenes 	= "";
		private $imagenes_eliminar		= null;
		private $encajar				= FALSE;
		
		public function setEncajar($valor){
			$this->encajar = $valor;	
		}

		public function __construct($campos_imagen,$instancia){
			try{
				if(is_array($campos_imagen)){
					if(count($campos_imagen)<0){
						throw new ExcepcionSistema(	ErrorConstantes::$int_error_xml_27,
													ErrorConstantes::$int_error_xml_27,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
				}
				else {
					$campos_imagen = array($campos_imagen);
				}
				$this->campos_imagen	= $campos_imagen;
				$this->instancia 		= $instancia;
			}catch (ExcepcionSistema  $e){
				$this->instancia->completeTransaccion(false);
				$e->muestraError();
			}
		}
		
		public function setImagenesOriginales($imagenes_originales){
			$this->imagenes_originales = $imagenes_originales;
		}
		
		public function switchOpcion(){
			
			switch (Request::getParametro(AllRegistroConstantes::$accion)) {
				case AllRegistroConstantes::$agregar:
					$this->addImagen();
					break;
					
				case AllRegistroConstantes::$editar:
					$this->editImagen();
					break;
			
				case AllRegistroConstantes::$eliminar:
					$this->deleteImg();
					break;
				
				default:
					break;
			}
		}
		
		public function deleteImg(){
			for($i=0 ; $i < count($this->campos_imagen);$i++){
				$this->imagenes_eliminar = Matriz::field2Array($this->imagenes_originales[$i]);
				$this->deleteImagenes();
			}			
		}
		
		public function editImagen(){
			
			for($i=0 ; $i < count($this->campos_imagen);$i++){
				$this->campo_actual 		= $this->campos_imagen[$i];
				$originales					= Matriz::field2Array($this->imagenes_originales[$i]);
				$actuales					= Matriz::field2Array($this->getRegistroImagenes($this->campos_imagen[$i]));
				$this->imagenes_eliminar	= array_values(array_diff($originales,$actuales));
				$this->imagenes_modificar	= array_values(array_diff($actuales,$originales));
				$this->imagenes_viejas		= array_values(array_intersect($actuales,$originales));
				
				if(Request::getParametro($this->campos_imagen[$i]) != ""){
					if( $this->imagenes_modificar ){
						$this->changeNames($this->imagenes_modificar);
					}
					if( $this->imagenes_eliminar ){
						$this->deleteImagenes();
					}
				}
			}
		}
		
		public function addImagen(){
			foreach ($this->campos_imagen as $campo) {
				$this->campo_actual 		= $campo;
				$this->imagenes_modificar 	= Matriz::field2Array($this->getRegistroImagenes($campo));
				if(Request::getParametro($campo) != ""){
					$this->changeNames($this->imagenes_modificar);
				}
			}
		}
		
		public function getSQL($caso){
			
			$sql 	= "";			
			switch ($caso) {
				case $this->_listado_imagenes:
					$sql =  "SELECT @c
							   FROM @t
							  WHERE id_@t = @i";
					break;
			
				default:
					$sql = 	"SELECT 'Error'";
					break;
			}
			return $sql;
		}
		
		public function getRegistroImagenes($campo){
			
			$busqueda 	= array("@c","@t","@i");
			$id 		= Request::getParametro(AllRegistroConstantes::$agregar) != ""?
								$this->instancia->getUltimoId():
								$this->instancia->id_registro;
								
			$reemplazo	= array(	$campo,
									$this->instancia->getNombretabla(),
									$id);
			
			//Aqui se detecto el error y se corrigio pero esto genero un problema en otro lugar
			/*						
			if(Request::getParametro(AllRegistroConstantes::$agregar)){
				$reemplazo[] = $this->instancia->getUltimoId();
			}
			elseif (Request::getParametro(AllRegistroConstantes::$editar)){
				$reemplazo[] = $this->instancia->id_registro;
			}
			*/
			$sql = Replace::replaceDatos($busqueda,$reemplazo,$this->getSQL($this->_listado_imagenes));
			return $this->instancia->getOne($sql);
		}
		
		public function getImagenes(){
			$imagenes = "";
			foreach ($this->campos_imagen as $imagen) {
				if(isset($_REQUEST[$imagen])){
					$imagenes = Request::getParametro($imagen);
					if($imagenes != null){
						$arreglo_imagenes = explode(",",$imagenes);
					}
					
				}
			}
		}
		
		function changeNames($imagenes){
			$id 		= "";
			$nombres	= array();

			try{
				$id = $this->instancia->id_registro;

				if($imagenes){
					
					foreach ($imagenes as $imagen){
						$nombres[]  			= "{$id}-".count($nombres) . date("G_i_s_"). substr(microtime(),2,3).
												  "_img_{$this->instancia->getNombreTabla()}";
					}

					$this->nombre_thumbs = $nombres;
					//AQUI ESTA EL CHISTE DE LAS DIFERENTES TAMANOS PARA LA DIFERENTE CAMPO

									
					$array_info 		 = $this->getInfoThumbs();					
					$ancho 				 = $array_info["ancho"];
					$alto				 = $array_info["alto"];
					$thumbs				 = $array_info["thumbs"];
					
					
					
					if( (sizeof($ancho) != sizeof($alto)) && (sizeof($ancho)!= sizeof($thumbs)) && (sizeof($alto) != sizeof($thumbs)) ){
						throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_44,
													ErrorConstantes::$int_error_xml_44,
													LoggerConstantes::$LOG_NIVEL_LOGICA);													
					}
					
					for($i = 0; $i < sizeof($thumbs); $i++){
						$this->createThumbs( $imagenes,$ancho[$i],$alto[$i],$thumbs[$i] );						
					}
					
					$this->actualizaImagenes();
					
					///////////ESTA LINEA ES PARA OBTENER EL ANCHO DE LA IMAGEN Y GUARDARLO EN LA BD PARA PONCHO MTZ
					//$this->instancia->setAnchoImagen($ancho);
					$this->renameImagenes();

				}
				else {
					throw new ExcepcionSistema(	ErrorConstantes::$str_error_xml_28,
												ErrorConstantes::$int_error_xml_28,
												LoggerConstantes::$LOG_NIVEL_LOGICA);
				}

			}catch (ExcepcionSistema  $e){				
				$e->muestraError();
			}

		}
		
		public function getValuePropertyThumb($propiedad){
			if(($valor = $this->checkPropiedad($this->instancia,$propiedad))){
					if(is_array($valor)){
						if(array_key_exists($this->campo_actual,$valor)){
							$propiedad  = is_array($valor[$this->campo_actual]) ? 
												$valor[$this->campo_actual]:
												array($valor[$this->campo_actual]);
						}
						else {
							$propiedad = $valor;	
						}
						
					}
					else {
						$propiedad = array($valor);
					}					
				}
				else {
					$propiedad = array($this->checkPropiedad("ImagenConstantes",$propiedad));
				}
				return $propiedad;
		}
		
		public function getInfoThumbs(){
				$regreso = array();
				
				$regreso["ancho"] 	= $this->getValuePropertyThumb("ancho_thumb");
				$regreso["alto"]	= $this->getValuePropertyThumb("alto_thumb");
				$regreso["thumbs"]	= $this->getValuePropertyThumb("directorio_thumbs");
							
				return $regreso;
		}
		
		function createThumbs( $arreglo_imagenes = array() ,$ancho_thumb =0,$alto_thumb = 0, $directorio_thumbs = ""){
			$thumb 					= null;
			$directorio_imagenes 	= "";						
			$propiedad_imagen		= null;
			$propiedad_thumb		= null;
			try{
				
				$directorio_imagenes = $this->checkStrictPropiedad($this->instancia,"directorio_imagenes");
				
				$thumb = new Thumbs(	$directorio_imagenes,
										$arreglo_imagenes,
										$directorio_thumbs,
										$this->nombre_thumbs,
										FALSE,
										100,
										0,
										$ancho_thumb,
										$alto_thumb,
										$this->instancia);
										
										
				if($this->encajar){
					/*file_put_contents("b.txt","dt-".$directorio_thumbs,FILE_APPEND);*/
					$thumb->setEcajar(TRUE);
				}
				$thumb->creates_thumbs();
				$this->Thumb = $thumb;
			}catch (ExcepcionSistema  $e){
				$this->instancia->completeTransaccion(false);
				$e->muestraError();
			}catch (Exception $e){
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_36 . $e->getMessage(),
											ErrorConstantes::$int_error_xml_36,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}

		}
		
		public function checkStrictPropiedad($clase_objeto,$propiedad){
			$refleccion 			= null;
			$valor					= "";
			$refleccion_propiedad	= null;
			$regresa				= "";
			try{
				
			$refleccion = new ReflectionClass($clase_objeto);
				if($refleccion->hasProperty($propiedad)){
					$refleccion_propiedad = $refleccion->getProperty($propiedad);
					$valor = $refleccion_propiedad->getValue(new $clase_objeto());
					if($valor != ""){
						$regresa = $valor;						
					}
					else {
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_30 . $propiedad,
													ErrorConstantes::$int_error_xml_30,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_29 . $propiedad,
												ErrorConstantes::$int_error_xml_29,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}
			
			}catch (Exception $e){
				$this->instancia->completeTransaccion(false);
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_36 . $e->getMessage(),
											ErrorConstantes::$int_error_xml_36,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}catch (ExcepcionSistema  $e){
				$this->instancia->completeTransaccion(false);
				$e->muestraError();
			}
			return $regresa;			
		}

		public function checkPropiedad($clase_objeto,$propiedad){
			$refleccion 			= null;
			$valor					= "";
			$refleccion_propiedad	= null;
			$regresa				= "";
			try{
				
			$refleccion = new ReflectionClass($clase_objeto);
				if($refleccion->hasProperty($propiedad)){
					$refleccion_propiedad = $refleccion->getProperty($propiedad);
					$valor = $refleccion_propiedad->getValue(new $clase_objeto());
					if($valor != ""){
						$regresa = $valor;						
					}
				}
			
			}catch (Exception $e){
				$this->instancia->completeTransaccion(false);
				throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_36 . $e->getMessage(),
											ErrorConstantes::$int_error_xml_36,
											LoggerConstantes::$LOG_NIVEL_CRITICO);
			}
			return $regresa;
		}
		
		public function actualizaImagenes(){
			$imagenes_finales = null;
			try{

				if(count($this->imagenes_modificar) == count($this->Thumb->_new_name_thumbs)){


					if(Request::getParametro(AllRegistroConstantes::$accion) == AllRegistroConstantes::$agregar){
						$values = array(	$this->campo_actual=>
											implode(",",$this->Thumb->_new_name_thumbs) );
						
						$values["width"] = $this->Thumb->new_width;
						
					}
					elseif (Request::getParametro(AllRegistroConstantes::$accion) == AllRegistroConstantes::$editar){
						if($this->imagenes_viejas){
							$imagenes_finales = array_merge(	$this->imagenes_viejas,
																$this->Thumb->_new_name_thumbs);
						}
						else {
							$imagenes_finales = $this->Thumb->_new_name_thumbs; 
						}
						$values = array( $this->campo_actual=>
										 implode(",",$imagenes_finales));
					}
					$this->instancia->setRegistroCampos($values);
				}
				else {
					throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_33,
												ErrorConstantes::$int_error_xml_33,
												LoggerConstantes::$LOG_NIVEL_CRITICO);
				}


			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false);
				$e->muestraError();
			}
		}

		function renameImagenes(){
			try{
				for($i =0; $i < count($this->imagenes_modificar); $i++){
					$imagen = new Archivo(	$this->instancia->directorio_imagenes . 
											$this->imagenes_modificar[$i]);
					if(!$imagen->existArchivo()){
						throw new ExcepcionSistema( ErrorConstantes::$str_error_xml_33,
													ErrorConstantes::$int_error_xml_33,
													LoggerConstantes::$LOG_NIVEL_CRITICO);
					}
					$imagen->renameArchivo($this->Thumb->_new_name_thumbs[$i]);
				}
			}catch (ExcepcionSistema $e){
				$this->instancia->completeTransaccion(false);
				$e->muestraError();
			}

		}
		//FIX ME Checar que pasa cuando elimina una imagen y tiene un thumb en varias carpetas
		public function deleteImagenes(){			
				foreach ($this->imagenes_eliminar as $imagen) {
					$original 	= new Archivo($this->instancia->directorio_imagenes . $imagen);
					//$thumb		= new Archivo($this->instancia->directorio_thumbs . $imagen);
					$original->deleteArchivo();
					//$thumb->deleteArchivo();
				}					
		}
	}


?>