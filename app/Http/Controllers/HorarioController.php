<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Horario;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $horario = Horario::all();
        if(!is_null($horario)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"horario found successfully",
                        "horario"=>$horario
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"horario is empty, please try again", 
                        "horario"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $horario = new Horario;
            $horario->hora_inicio = $input['hora_inicio'];
            $horario->lu = $input['lu']; 
            $horario->ma = $input['ma'];
            $horario->mi = $input['mi'];
            $horario->ju = $input['ju'];
            $horario->vi = $input['vi'];
            $horario->sa = $input['sa'];
            $horario->grupo = $input['grupo'];
            $horario->grado = $input['grado'];

            $horario->save();
            return response()->json(array("success"=>true, "message"=>"Horario created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $horario = Horario::find($id);

        if(!is_null($horario)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"horario found successfully",
                        "horario"=>array("horario"=>$horario)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"horario is not found, please try again", 
                        "horario"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
