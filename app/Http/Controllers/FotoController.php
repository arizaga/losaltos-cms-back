<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Foto;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class FotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $foto = Foto::all();
        if(!is_null($foto)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"foto found successfully",
                        "foto"=>$foto
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"foto is empty, please try again", 
                        "foto"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $foto = new Foto;
            $foto->titulo = $input['titulo'];
            $foto->descripcion = $input['descripcion']; 
            $foto->foto = $input['foto'];
            $foto->grupo = $input['grupo'];
            $foto->grado = $input['grado'];
           
            $foto->save();
            return response()->json(array("success"=>true, "message"=>"Foto created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $foto = Foto::find($id);

        if(!is_null($foto)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"foto found successfully",
                        "foto"=>array("foto"=>$foto)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"foto is not found, please try again", 
                        "foto"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
