<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Curso_padre;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class Curso_padreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $curso_padre = Curso_padre::all();
        if(!is_null($curso_padre)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"curso_padre found successfully",
                        "curso_padre"=>$curso_padre
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"curso_padre is empty, please try again", 
                        "curso_padre"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $curso_padre = new Curso_padre;
            $curso_padre->nombre = $input['nombre'];
            $curso_padre->categoria = $input['categoria'];
            $curso_padre->horario = $input['horario'];
            $curso_padre->fecha = $input['fecha'];
            $curso_padre->descripcion = $input['descripcion'];           
            $curso_padre->dirigido = $input['dirigido'];
            $curso_padre->tipo = $input['tipo'];
            
            $curso_padre->save();
            return response()->json(array("success"=>true, "message"=>"Curso padre user created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $curso_padre = Curso_padre::find($id);

        if(!is_null($curso_padre)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"curso_padre found successfully",
                        "curso_padre"=>array("curso_padre"=>$curso_padre)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"curso_padre is not found, please try again", 
                        "curso_padre"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
