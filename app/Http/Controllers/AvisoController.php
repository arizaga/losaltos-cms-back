<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Aviso;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class AvisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $aviso = Aviso::all();
        if(!is_null($aviso)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"aviso found successfully",
                        "aviso"=>$aviso
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"aviso is empty, please try again", 
                        "aviso"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $aviso = new Aviso;
            $aviso->titulo = $input['titulo'];
            $aviso->descripcion = $input['descripcion'];
            $aviso->fecha = $input['fecha'];
            $aviso->dirigido = $input['dirigido'];
            $aviso->tipo = $input['tipo'];
            $aviso->archivo = $input['archivo'];

            $aviso->save();
            return response()->json(array("success"=>true, "message"=>"aviso created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $aviso = Aviso::find($id);

        if(!is_null($aviso)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"aviso found successfully",
                        "aviso"=>array("aviso"=>$aviso)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"aviso is not found, please try again", 
                        "aviso"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
