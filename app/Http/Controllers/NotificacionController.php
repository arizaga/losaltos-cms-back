<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notificacion;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class NotificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $notificacion = Notificacion::all();
        if(!is_null($notificacion)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"notificacion found successfully",
                        "notificacion"=>$notificacion
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"notificacion is empty, please try again", 
                        "notificacion"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $notificacion = new Notificacion;
            $notificacion->nombre = $input['nombre'];
            $notificacion->fecha = $input['fecha']; 
            $notificacion->modulo = $input['modulo'];
            $notificacion->id_alumno = $input['id_alumno'];
            $foto->descripcion = $input['descripcion']; 
            $notificacion->grupo = $input['grupo'];
            $notificacion->grado = $input['grado'];

            $notificacion->save();
            return response()->json(array("success"=>true, "message"=>"Notificacion created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $notificacion = Notificacion::find($id);

        if(!is_null($notificacion)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"notificacion found successfully",
                        "notificacion"=>array("notificacion"=>$notificacion)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"notificacion is not found, please try again", 
                        "notificacion"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
