<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cms;//call the model
use Illuminate\Http\Response;
//use Illuminate\Http\Request; commented and add the line above to can use Request::json()
use Request;
use DB; //Enable use db class
use App\Quotation; //Enable use db class
use Auth;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cms = Cms::all();
        if(!is_null($cms)){
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"cms found successfully",
                        "cms"=>$cms
                    )
                );
        }else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"cms is empty, please try again", 
                        "cms"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Request::json()->all();
        
        if(!is_null($input)){
            $cms = new cms;
            $cms->user = $input['user'];
            $cms->pass = $input['pass'];
            $cms->seccion_horario = $input['seccion_horario'];
            //$cms->seccion_tarea = $input['seccion_tarea'];
            $cms->seccion_aviso = $input['seccion_aviso'];
            //$cms->seccion_calendario = $input['seccion_calendario'];
            $cms->seccion_notificacion = $input['seccion_notificacion'];           
            //$cms->seccion_periodo = $input['seccion_periodo'];
            $cms->seccion_curso_padre = $input['seccion_curso_padre'];
            //$cms->seccion_maestro = $input['seccion_maestro'];
            $cms->seccion_foto = $input['seccion_foto'];
            $cms->seccion_cms = $input['seccion_cms'];
            
            $cms->save();
            return response()->json(array("success"=>true, "message"=>"cms user created successfully"));
        }
        else{
            return response()->json(array("success"=>false, "message"=>"empty data, please try again"));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cms = Cms::find($id);

        if(!is_null($cms)){    
            return 
                response()->json(
                    array(
                        "success"=>true, 
                        "message"=>"cms found successfully",
                        "cms"=>array("cms"=>$cms)
                    )
                );
        }
        else{
            return 
                response()->json(
                    array(
                        "success"=>false, 
                        "message"=>"cms is not found, please try again", 
                        "cms"=>null
                    )   
                );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
