<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//AvisoController
Route::get('aviso/index', 'AvisoController@index');
Route::post('aviso/create', 'AvisoController@create');
Route::post('aviso/update', 'AvisoController@update');
Route::post('aviso/delete', 'AvisoController@destroy');
Route::get('aviso/show/{id}', 'AvisoController@show');

//CmsController
Route::get('cms/index', 'CmsController@index');
Route::post('cms/create', 'CmsController@create');
Route::post('cms/update', 'CmsController@update');
Route::post('cms/delete', 'CmsController@destroy');
Route::get('cms/show/{id}', 'CmsController@show');

//Curso_padreController
Route::get('curso_padre/index', 'Curso_padreController@index');
Route::post('curso_padre/create', 'Curso_padreController@create');
Route::post('curso_padre/update', 'Curso_padreController@update');
Route::post('curso_padre/delete', 'Curso_padreController@destroy');
Route::get('curso_padre/show/{id}', 'Curso_padreController@show');

//FotoController
Route::get('foto/index', 'FotoController@index');
Route::post('foto/create', 'FotoController@create');
Route::post('foto/update', 'FotoController@update');
Route::post('foto/delete', 'FotoController@destroy');
Route::get('foto/show/{id}', 'FotoController@show');

//HorarioController
Route::get('horario/index', 'HorarioController@index');
Route::post('horario/create', 'HorarioController@create');
Route::post('horario/update', 'HorarioController@update');
Route::post('horario/delete', 'HorarioController@destroy');
Route::get('horario/show/{id}', 'HorarioController@show');

//NotificacionController
Route::get('notificacion/index', 'NotificacionController@index');
Route::post('notificacion/create', 'NotificacionController@create');
Route::post('notificacion/update', 'NotificacionController@update');
Route::post('notificacion/delete', 'NotificacionController@destroy');
Route::get('notificacion/show/{id}', 'NotificacionController@show');



/*
Route::get('pet/getPet/{id}', 'PetController@show');
Route::post('pet/UploadImage', 'PetController@UploadImage');
*/
