<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso_padre extends Model {

    protected $table = 'curso_padre';
    protected $primaryKey = 'id_curso_padre';

}
