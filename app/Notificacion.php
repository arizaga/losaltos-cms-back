<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model {

    protected $table = 'notificacion';
    protected $primaryKey = 'id_notificacion';

}
